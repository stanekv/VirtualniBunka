/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1627387826_1_
#define _BUR_1627387826_1_

#include <bur/plctypes.h>

/* Datatypes and datatypes of function blocks */
typedef struct gRobotCtrlInterfaceType
{	plcbit Enable;
	plcbit Error;
	unsigned short ErrorCode;
	plcbit ErrorReset;
	struct RBTemplateRobotCtrlInputCmdType ControllerInput;
	struct RBTemplateRobotCtrlStatusType ControllerStatus;
	struct RBTemplateRobotCtrlInputRobType RobotInput1;
	struct RBTemplateRobotCtrlInputRobType RobotInput2;
	struct RBTemplateRobotCtrlInputRobType RobotInput3;
	struct RBTemplateRobotCtrlInputRobType RobotInput4;
	struct RBTemplateRobotCtrlRobInfoType RobotStatus1;
	struct RBTemplateRobotCtrlRobInfoType RobotStatus2;
	struct RBTemplateRobotCtrlRobInfoType RobotStatus3;
	struct RBTemplateRobotCtrlRobInfoType RobotStatus4;
	unsigned char RobotIndex;
	struct RBTemplateRobotMotionDataType MotionData;
} gRobotCtrlInterfaceType;

typedef struct gRobotTeachingVisuInstrListType
{	unsigned short Index[30];
	plcstring Type[30][13];
	plcstring PredefinedSpeed[30][11];
	plcstring PredefinedZone[30][11];
	plcstring Name[30][33];
	plcstring TableRows[201];
} gRobotTeachingVisuInstrListType;

typedef struct gRobotTeachingInterfaceType
{	plcbit Enable;
	plcbit Error;
	unsigned short ErrorCode;
	plcbit ErrorReset;
	plcbit Active;
	struct RBTemplateTeachingCmdType Command;
	struct RBTemplateTeachingParInstrType Instruction;
	unsigned short InstrCount;
	unsigned short Index;
	plcbit KeepTargetPosition;
	unsigned char RobotIndex;
	unsigned char RobotIndexMax;
	struct RBTemplateRobotMotionDataType MotionData;
	plcstring ProcedureName[37];
	struct gRobotTeachingVisuInstrListType InstructionList;
} gRobotTeachingInterfaceType;






__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1627387826_1_ */

