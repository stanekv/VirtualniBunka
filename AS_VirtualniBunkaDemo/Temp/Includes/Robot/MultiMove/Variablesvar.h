/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1627388070_2_
#define _BUR_1627388070_2_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL plcbit StartAll;
_BUR_LOCAL plcbit LoadAll;
_BUR_LOCAL plcstring DstPathSim[37];
_BUR_LOCAL plcstring DstPath[37];
_BUR_LOCAL plcstring SrcPathSim[37];
_BUR_LOCAL plcstring SrcPath[37];
_BUR_LOCAL plcstring ControllerIpAdrSim[16];
_BUR_LOCAL plcstring ControllerIpAdr[16];
_BUR_LOCAL struct RBTemplateRobotCtrl RBTemplateRobotCtrl_0;
_BUR_LOCAL plcbit QuatCheck;
_BUR_LOCAL struct RBTemplateABBOrientType Quat;
_BUR_LOCAL float Rx;
_BUR_LOCAL float Ry;
_BUR_LOCAL float Rz;
_BUR_LOCAL struct RBTemplateABBOrientType Rot;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Robot/MultiMove/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplate.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1627388070_2_ */

