/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1627388070_4_
#define _BUR_1627388070_4_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_BUR_LOCAL struct RBTemplateABBMoveInstrListType MoveInstructionList_0[20];
_BUR_LOCAL plcstring PathSim[37];
_BUR_LOCAL plcstring Path[37];
_BUR_LOCAL plcstring ControllerIpAdrSim[16];
_BUR_LOCAL plcstring ControllerIpAdr[16];
_BUR_LOCAL struct RBTemplateTeaching RBTemplateTeaching_0;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Robot/RobotTeach/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplate.fun\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1627388070_4_ */

