#ifndef _DEFAULT_757602046
#define _DEFAULT_757602046
#include <bur/plctypes.h>
#include <bur/plc.h>

#ifdef __cplusplus 
extern "C" 
{
#endif
	#include <operator.h>
	#include <runtime.h>
	#include <standard.h>
	#include <AsBrMath.h>
	#include <AsBrStr.h>
	#include <MTDiag.h>
	#include <astime.h>
	#include <brsystem.h>
	#include <DataObj.h>
	#include <FileIO.h>
	#include <sys_lib.h>
	#include <AsTCP.h>
	#include <AsHttp.h>
	#include <AsICMP.h>
	#include <AsUDP.h>
	#include <AsGuard.h>
	#include <ArEventLog.h>
	#include <AsZip.h>
	#include <AsIODiag.h>
	#include <AsUSB.h>
	#include <MTHash.h>
	#include <CSCom.h>
	#include <RsComm.h>
	#include <FDLink.h>
	#include <RBTemplate.h>
#ifdef __cplusplus
};
#endif

#include <globalTYP.h>
#include <globalVAR.h>
#endif
