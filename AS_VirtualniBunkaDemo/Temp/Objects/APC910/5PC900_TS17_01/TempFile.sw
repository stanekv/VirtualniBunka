﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.6.6.68 SP?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="MultiMove" Source="Robot.MultiMove.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="RobotTeach" Source="Robot.RobotTeach.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2">
    <Task Name="Program" Source="Program.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Cycle" Source="Cycle.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#3" />
  <TaskClass Name="Cyclic#4" />
  <TaskClass Name="Cyclic#5" />
  <TaskClass Name="Cyclic#6" />
  <TaskClass Name="Cyclic#7" />
  <TaskClass Name="Cyclic#8" />
  <DataObjects>
    <DataObject Name="McAcpSys" Source="" Memory="UserROM" Language="Binary" />
    <DataObject Name="assl1" Source="" Memory="UserROM" Language="Binary" />
  </DataObjects>
  <Binaries>
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="mvLoader" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="TCLang" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="TC" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="mCoWebSc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ViSettings" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ViBlobApp" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Config" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Config_1" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="McAcpDrv" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="McAcpSim" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="OpcUaMap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arsvcreg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcgclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ArFlatPrv" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ArCoal" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ItmListRec" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Backup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AlarmX" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="FileAlarmX" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="UserXLog" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="RecAlarmX" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="SetGroup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="FileManag" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Diag" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AuditTrail" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="RecGroup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Recipe" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="AlarmXHist" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="asnxdb1" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="HWAlarmX" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="DgAlarmX" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="HWGroup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="UserX" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="WebX" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="DgGroup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="OeeStat" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Setup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="FileGroup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="SetAlarmX" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="gDBCore" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Settings" Source="" Memory="UserROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="astime" Source="Libraries.astime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="DataObj" Source="Libraries.DataObj.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="FileIO" Source="Libraries.FileIO.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsTCP" Source="Libraries.AsTCP.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsHttp" Source="Libraries.AsHttp.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsICMP" Source="Libraries.AsICMP.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsGuard" Source="Libraries.AsGuard.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="ArEventLog" Source="Libraries.ArEventLog.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsBrMath" Source="Libraries.AsBrMath.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="Libraries.AsBrStr.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsZip" Source="Libraries.AsZip.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsIODiag" Source="Libraries.AsIODiag.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsUSB" Source="Libraries.AsUSB.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsUDP" Source="Libraries.AsUDP.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="CSCom" Source="Libraries.CSCom.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="FDLink" Source="Libraries.FDLink.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTDiag" Source="Libraries.MTDiag.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTHash" Source="Libraries.MTHash.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="RBTemplate" Source="Libraries.RBTemplate.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="RsComm" Source="Libraries.RsComm.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="arssl" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>