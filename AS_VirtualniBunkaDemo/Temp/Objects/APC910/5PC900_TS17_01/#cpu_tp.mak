SHELL := cmd.exe

export AS_SYSTEM_PATH := C:/BrAutomation/AS/System
export AS_BIN_PATH := C:/BrAutomation/AS46/bin-en
export AS_INSTALL_PATH := C:/BrAutomation/AS46
export AS_PATH := C:/BrAutomation/AS46
export AS_VC_PATH := C:/BrAutomation/AS46/AS/VC
export AS_GNU_INST_PATH := C:/BrAutomation/AS46/AS/GnuInst/V6.3.0
export AS_STATIC_ARCHIVES_PATH := C:/projects/RoboTemplate/ROBOTemplate1/Temp/Archives/APC910/5PC900_TS17_01
export AS_CPU_PATH := C:/projects/RoboTemplate/ROBOTemplate1/Temp/Objects/APC910/5PC900_TS17_01
export AS_CPU_PATH_2 := C:/projects/RoboTemplate/ROBOTemplate1/Temp/Objects/APC910/5PC900_TS17_01
export AS_TEMP_PATH := C:/projects/RoboTemplate/ROBOTemplate1/Temp
export AS_BINARIES_PATH := C:/projects/RoboTemplate/ROBOTemplate1/Binaries
export AS_PROJECT_CPU_PATH := C:/projects/RoboTemplate/ROBOTemplate1/Physical/APC910/5PC900_TS17_01
export AS_PROJECT_CONFIG_PATH := C:/projects/RoboTemplate/ROBOTemplate1/Physical/APC910
export AS_PROJECT_PATH := C:/projects/RoboTemplate/ROBOTemplate1
export AS_PROJECT_NAME := ROBOTemplate1
export AS_PLC := 5PC900_TS17_01
export AS_TEMP_PLC := 5PC900_TS17_01
export AS_USER_NAME := stanek
export AS_CONFIGURATION := APC910
export AS_COMPANY_NAME := HP\ Inc.
export AS_VERSION := 4.6.6.68\ SP


default: \



