define([
    'brease/enum/Enum'
], function (
    Enum
) {

    'use strict';   
    
    /**
     * @class widgets.brease.Table.Config
     * @extends core.javascript.Object
     * @override widgets.brease.Table
     */
   
    /**
     * @cfg {UInteger} headerSize=0
     * @iatStudioExposed
     * @iatCategory Appearance
     * Height of rows.  
     */
   
    /**
     * @cfg {Size} rowHeight=30
     * @iatStudioExposed
     * @iatCategory Appearance
     * @groupRefId CellSize
     * @groupOrder 1
     * Height of rows.  
     */

    /**
     * @cfg {Size} columnWidth=100
     * @iatStudioExposed
     * @iatCategory Appearance
     * @groupRefId CellSize
     * @groupOrder 2
     * Width of columns.  
     */

    /**
     * @cfg {brease.enum.Direction} dataOrientation='vertical'
     * @iatStudioExposed
     * @iatCategory Behavior
     * A dataset is either placed horizontally or vertically.  
     */

    /**
     * @cfg {Integer} offsetRow=0
     * @iatStudioExposed
     * @iatCategory Behavior
     * @bindable
     * @not_projectable
     * Index of first viewed row entry.  
     */

    /**
     * @cfg {Integer} offsetColumn=0
     * @iatStudioExposed
     * @iatCategory Behavior
     * @bindable
     * @not_projectable
     * Index of first viewed column entry.  
     */

    /**
     * @cfg {Integer} selectedRow=0
     * @iatStudioExposed
     * @iatCategory Data
     * @bindable
     * Index of selected row.  
     */

    /**
     * @cfg {Integer} selectedColumn=0
     * @iatStudioExposed
     * @iatCategory Data
     * @bindable
     * Index of selected column.  
     */

    /**
     * @cfg {Boolean} showScrollbars=true
     * @iatStudioExposed
     * @iatCategory Behavior
     * Show/Hide scrollbars  
     */

    /**
     * @cfg {Boolean} showHeader=true
     * @iatStudioExposed
     * @iatCategory Appearance
     * Show/Hide header  
     */

    /**
     * @cfg {Boolean} ellipsis=false
     * @iatStudioExposed
     * @iatCategory Behavior 
     * If true, overflow of text is symbolized with an ellipsis. This option has no effect, if wordWrap = true.
     */

    /**
     * @cfg {Boolean} useTableStyling=true
     * @iatStudioExposed
     * @iatCategory Appearance 
     * If false, styling can be set on the individual columns on their repsective TableItem/TableItemImageList.
     */

    /**
     * @cfg {Boolean} showSortingButton=false
     * @iatStudioExposed
     * @iatCategory Behavior
     * If true,  by clicking on a columns header will be availabe. The table will only sort on the 
     * currently selected header. The header can be sorted in descending order (one click), ascending order 
     * (one more click) or returned to normal state (one more click); given that you started in normal state.
     */

    /**
     * @cfg {Boolean} multiLine=false
     * @iatStudioExposed
     * @iatCategory Behavior
     * If true, more than one line is possible. Text will wrap when necessary (wordWrap=true) or at line breaks (\n). 
     * If false, text will never wrap to the next line. The text continues on the same line.  
     */

    /**
     * @cfg {Boolean} wordWrap=false
     * @iatStudioExposed
     * @iatCategory Behavior
     * If true, text will wrap when necessary. 
     */

    /**
     * @cfg {Boolean} selection=true
     * @iatStudioExposed
     * @iatCategory Behavior
     * If true, the user can select data entries. 
     */

    /**
     * @cfg {Integer} maxHeight=0
     * @iatStudioExposed
     * @iatCategory Appearance
     * Maximum height the Table can grow, when not set to zero
     */

    /**
     * @cfg {String} tableConfiguration=''
     * @iatStudioExposed
     * @iatCategory Data
     * @bindable
     * @not_projectable
     * Configuration for the visibility and/or disabling of table rows and columns.
     * Use it like: 
     * Visible:
     *    "{
     *    'specRows': [
     *        {'index':0,'visible': true},
     *        {'index':2,'visible': false},
     *        {'index':4,'visible': true}
     *                ],
     *    'specColumns':  [
     *       {'index':5, 'visible': true},
     *       {'index':8, 'visible': false},
     *       {'index':10,'visible': true}
     *       ]
     *   }"
     *
     * Disable:
     *    "{
     *    'specRows': [
     *        {'index':0,'disable': true},
     *        {'index':2,'disable': false},
     *        {'index':4,'disable': true}
     *                ],
     *    'specColumns':  [
     *       {'index':5, 'disable': true},
     *       {'index':8, 'disable': false},
     *       {'index':10,'disable': true}
     *      ]
     *   }"
     *  Both: 
     *    "{
     *   'specRows': [
     *        {'index':0,'visible': true, 'disable': true},
     *        {'index':2,'visible': true,'disable': false},
     *        {'index':4,'visible': false, 'disable': true}
     *       ],
     *    'specColumns':  [
     *       {'index':5, 'visible': true, 'disable': true},
     *       {'index':8, 'visible': true, 'disable': false},
     *       {'index':10,'visible': false, 'disable': true}
     *       ]
     *    }"
     */

    /**
     * @cfg {String} filterConfiguration=''
     * @iatStudioExposed
     * @iatCategory Data
     * @bindable
     * Configuration for filtering entires in the table
     */
    
    /**
     * @cfg {WidgetReference} scrollLinkYRefId=''
     * @iatStudioExposed
     * @iatCategory Behavior
     * @groupRefId ScrollSynchronization
     * Reference to a second Table widget that should be synchronized for vertical scrolling
     */
    
    /**
     * @cfg {WidgetReference} scrollLinkXRefId=''
     * @iatStudioExposed
     * @iatCategory Behavior
     * @groupRefId ScrollSynchronization
     * Reference to a second Table widget that should be synchronized for horizontal scrolling
     */
       
    return {
        editorFirst: true,
        width: 400,
        height: 300,
        maxHeight: 0,
        headerSize: 0,
        rowHeight: 30,
        columnWidth: 100,
        dataOrientation: Enum.Direction.vertical,
        order: '',
        offsetRow: 0,
        offsetColumn: 0,
        selectedRow: 0,
        selectedColumn: 0,
        ellipsis: false,
        multiLine: false,
        wordWrap: false,
        selection: true,
        tableItemIds: [],
        tableData: [{ 'empty': true }],
        itemVisibility: [],
        itemRowHeights: [],
        itemColumnWidths: [],
        itemStyling: [],
        itemConfigs: [],
        itemEnableStates: [],
        hiddenColumns: [],
        headerTexts: [],
        useTableStyling: true,
        showSortingButton: false,
        visibilityConfiguration: undefined,
        rendererOptions: {
            errorMode: 'throw',
            selectableItem: 'row',
            scroller: {
                scrollY: true,
                scrollX: true,
                bounce: false,
                momentum: true,
                scrollDuration: 100,
                pagingItemTolerance: 0.2,
                scrollbars: 'custom'
            },
            selectionCallbackFn: '_selectionCallback',
            scrollCallbackFn: '_scrollCallback',
            drawCallbackFn: '_drawCallback',
            rendererReadyCallbackFn: '_rendererReadyCallback',
            tableReadyCallbackFn: '_tableReadyCallback',
            headerClickCallbackFn: '_headerClickCallback'
        },
        editor: {
            first: true,
            added: 0,
            editorItemOrder: []
        },
        showScrollbars: true,
        showHeader: true,
        tableItemLengths: [],
        tableItemTypes: [],
        scrollLinkYRefId: '',
        scrollLinkXRefId: '',
        tableConfiguration: '',
        filterConfiguration: '',
        filter: [],
        cellVisibility: [],
        cellDisability: []
    };
});
