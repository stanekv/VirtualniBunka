define(['brease/events/BreaseEvent', 'brease/core/Utils'],
    function (BreaseEvent, Utils) {

        'use strict';
        function KeyCollector() {
            var self = this;
            self.value = '';
            self.layer = 1;
            self.capslock = false;
            self.shift = false;
            self.special = false;
            self.init = function (eventDispatcher, widget) {
                self.widget = widget;
                self.eventDispatcher = eventDispatcher;
                self.clickEventName = _getEventConfig(brease.config.virtualKeyboards);
                self.widget.el.on('keyup', '.ValueOutput', self.onKeyUp);
                self.widget.el.on('keydown', '.ValueOutput', self.onKeyDown);
                self.widget.el.on(BreaseEvent.MOUSE_DOWN, '.ValueButton, .ActionButton, .ActionImage', self.onButtonMouseDown);
                self.widget.el.on(self.clickEventName, '.ValueButton', self.onValueButtonClick);
                self.widget.el.on(self.clickEventName, '.ActionButton', self.onActionButtonClick);
                self.widget.el.on(self.clickEventName, '.ActionImage', self.onActionButtonClick);
                self.eventDispatcher.addEventListener('Keyboard.Hide', _reset);
                self.valueButtons = self.widget.el.find('.ValueButton');
            };

            self.onButtonMouseDown = function () {
                if (self.activeButton) {
                    Utils.removeClass(self.activeButton, 'active');
                }
                self.activeButton = this;
                Utils.addClass(self.activeButton, 'active');
                brease.docEl.on(BreaseEvent.MOUSE_UP, self.onButtonMouseUp);
            };

            self.onButtonMouseUp = function () {
                brease.docEl.off(BreaseEvent.MOUSE_UP, self.onButtonMouseUp);
                Utils.removeClass(self.activeButton, 'active');
                self.activeButton = undefined;
                self.eventDispatcher.dispatchEvent({
                    type: 'Input.Focus'
                });
            };

            self.onValueButtonClick = function (e) {
                var value = _getValueByLayer(this, self.layer);
                self.eventDispatcher.dispatchEvent({
                    type: 'Collector.Input',
                    detail: {
                        'value': value
                    }
                });
                if (self.shift && !self.capslock) {
                    self.widget.el.find('[data-action="shift"]').removeClass('selected');
                    self.shift = false;
                    self.layer = 1;
                    _switchLayer();
                }
            };

            self.onActionButtonClick = function (e) {
                var action = this.getAttribute('data-action');
                switch (action) {

                    case 'delete':
                        self.eventDispatcher.dispatchEvent({ type: 'Collector.Delete' });
                        break;
                    case 'enter':
                        self.eventDispatcher.dispatchEvent({ type: 'Collector.Submit' });
                        break;
                    case 'left':
                        self.eventDispatcher.dispatchEvent({ type: 'Collector.CursorLeft' });
                        break;
                    case 'right':
                        self.eventDispatcher.dispatchEvent({ type: 'Collector.CursorRight' });
                        break;
                    case 'shift':
                        _onShift();
                        _switchLayer();
                        break;
                    case 'close':
                        self.eventDispatcher.dispatchEvent({ type: 'Collector.Close' });
                        break;
                    case 'special':
                        _onSpecial();
                        _switchLayer();
                        break;
                    case 'clear':
                        self.eventDispatcher.dispatchEvent({ type: 'Collector.Clear' });
                        break;
                    default:
                        console.iatWarn('keyboard action "' + action + '" not supported!');
                        break;
                }
            };

            self.onKeyUp = function (e) {
                if (e.which === 13) {
                    self.eventDispatcher.dispatchEvent({ type: 'Collector.Submit' });
                } else {
                    self.eventDispatcher.dispatchEvent({
                        type: 'Collector.Change',
                        detail: {
                            'value': this.value
                        }
                    });
                }
            };

            self.onKeyDown = function (e) {
                if (self.widget.validate(e.key, self.widget.regexp) === '') {
                    e.originalEvent.preventDefault();
                }
            };

            self.dispose = function () {
                _reset();
                if (self.widget) {
                    self.widget.el.off('keyup', '.ValueOutput', self.onKeyUp);
                    self.widget.el.off('keydown', '.ValueOutput', self.onKeyDown);
                    self.widget.el.off(BreaseEvent.MOUSE_DOWN, '.ValueButton, .ActionButton, .ActionImage', self.onButtonMouseDown);
                    self.widget.el.off(self.clickEventName, '.ValueButton', self.onValueButtonClick);
                    self.widget.el.off(self.clickEventName, '.ActionButton', self.onActionButtonClick);
                    self.widget.el.off(self.clickEventName, '.ActionImage', self.onActionButtonClick);
                    self.widget = null;
                    self.valueButtons = null;
                }
                self.eventDispatcher.removeEventListener('Keyboard.Hide', _reset);
            };

            function _onShift() {
                if (self.shift === true && self.capslock === true) {
                    _reset();
                } else if (self.shift === true && self.capslock === false) {
                    self.special = false;
                    self.capslock = true;
                    self.layer = 2;
                } else {
                    self.widget.el.find('[data-action="special"]').removeClass('selected');
                    self.widget.el.find('[data-action="shift"]').addClass('selected');
                    self.special = false;
                    self.shift = true;
                    self.layer = 2;
                }
                self.eventDispatcher.dispatchEvent({
                    type: 'Input.Focus'
                });
            }

            function _onSpecial() {
                if (self.special === true) {
                    _reset();
                } else {
                    self.widget.el.find('[data-action="special"]').addClass('selected');
                    self.widget.el.find('[data-action="shift"]').removeClass('selected');
                    self.shift = false;
                    self.capslock = false;
                    self.special = true;
                    self.layer = 3;
                }
                self.eventDispatcher.dispatchEvent({
                    type: 'Input.Focus'
                });
            }

            function _switchLayer() {
                var attr,
                    shiftAttr,
                    specAttr;

                switch (self.layer) {
                    case 1:
                        attr = 'display';
                        shiftAttr = 'shift-display';
                        specAttr = 'special-display';
                        break;
                    case 2:
                        attr = 'shift-display';
                        shiftAttr = '';
                        specAttr = '';
                        break;
                    case 3:
                        attr = 'special-display';
                        shiftAttr = '';
                        specAttr = '';
                        break;
                }
                self.valueButtons.each(function (index, button) {
                    var html = '';
                    if (specAttr) {
                        html += '<sub>' + button.getAttribute('data-' + specAttr) + '</sub>';
                    }
                    html += '<span>' + button.getAttribute('data-' + attr) + '</span>';
                    if (shiftAttr) {
                        html += '<sup>' + button.getAttribute('data-' + shiftAttr) + '</sup>';
                    }
                    button.innerHTML = html;
                });
            }

            function _reset() {
                self.shift = false;
                self.capslock = false;
                self.special = false;
                self.layer = 1;
                if (self.widget) {
                    self.widget.el.find('[data-action="special"]').removeClass('selected');
                    self.widget.el.find('[data-action="shift"]').removeClass('selected');
                    _switchLayer();
                }
            }
        }

        function _getValueByLayer(btnEl, layer) {
            var value = '';
            switch (layer) {
                case 1:
                    value = btnEl.getAttribute('data-value');
                    break;
                case 2:
                    value = btnEl.getAttribute('data-shift-value');
                    break;
                case 3:
                    value = btnEl.getAttribute('data-special-value');
                    break;
            }
            return value;
        }

        function _getEventConfig(kbdConf) {
            if (!kbdConf) {
                return BreaseEvent.CLICK;
            }
            if (kbdConf.InputProcessing) {
                return kbdConf.InputProcessing.onKeyDown === true ? BreaseEvent.MOUSE_DOWN : BreaseEvent.CLICK;
            } else {
                return BreaseEvent.CLICK;
            }
        }

        return KeyCollector;
    });
