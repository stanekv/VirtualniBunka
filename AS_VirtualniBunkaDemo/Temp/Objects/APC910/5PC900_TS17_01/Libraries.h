/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _LIBRARIES_20210727141430_
#define _LIBRARIES_20210727141430_

__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/DataObj/DataObj.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/DataObj/DataObj.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/DataObj/DataObj.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsICMP/AsICMP.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsICMP/AsICMP.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsICMP/AsICMP.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUDP/AsUDP.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUDP/AsUDP.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUDP/AsUDP.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsGuard/AsGuard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsGuard/AsGuard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsGuard/AsGuard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrMath/AsBrMath.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrMath/AsBrMath.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrMath/AsBrMath.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsUSB/AsUSB.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CSCom/Types.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CSCom/CSComError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CSCom/CSComFacilities.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CSCom/Constants.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CSCom/CSCom.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FDLink/FDLink.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FDLink/FDLinkError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FDLink/FDLink.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FDLink/FDLink.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTDiag/MTDiag.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTDiag/MTDiag.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTDiag/MTDiag.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTHash/Types.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTHash/Constants.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTHash/MTHash.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplate.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplate.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplate.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateFacilities.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateABBTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateModGen.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateModGen.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateRobotCtrl.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateRobotCtrl.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateCommTcp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateToolDec.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateWobjDec.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateTargetEditor.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateTeaching.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateTeaching.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateRestApi.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateRestApi.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateLicense.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateLicense.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateLogbook.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateLogbook.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateIdentHandler.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RBTemplate/RBTemplateIdentHandler.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RsComm/RsComm.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RsComm/RsComm.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/RsComm/RsComm.var\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */
#ifdef _OPERATOR_EXPORT
	__asm__(".ascii \"plcexport \\\"SIZEOF\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ADR\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ADRINST\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SHR\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ROR\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ROL\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SHL\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"AND\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"XOR\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"OR\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"NOT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ADD\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MUL\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SUB\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DIV\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MOD\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MOVE\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MAX\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MIN\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"LIMIT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SEL\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MUX\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"GE\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"EQ\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"GT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"LE\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"LT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"NE\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ABS\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SQRT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"LN\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"LOG\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"EXP\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SIN\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"COS\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"TAN\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ASIN\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ACOS\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ATAN\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"EXPT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"TRUNC\\\" FUN\\n\"");
#endif
#ifdef _RUNTIME_EXPORT
	__asm__(".ascii \"plcexport \\\"r_trig\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"f_trig\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"rf_trig\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"GetTime\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SFCActionControl\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"SFCAC2\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RealTan\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealAtan\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealAsin\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealAcos\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealExp\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealLn\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealLog\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealExpt\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealAbs\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealSin\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealCos\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RealSqrt\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SFCAC3\\\" FUB\\n\"");
#endif
#ifdef _ASTIME_EXPORT
	__asm__(".ascii \"plcexport \\\"DTExSetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DTSetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DTGetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"clock_ms\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"UtcDTGetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UtcDTSetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UtcDTExSetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DTStructureGetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DTStructureSetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DTStructureExSetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UtcDTStructureGetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UtcDTStructureSetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UtcDTStructureExSetTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UtcDT_TO_LocalDTStructure\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"LocalDT_TO_UtcDTStructure\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UtcDTStructure_TO_LocalDT\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"LocalDTStructure_TO_UtcDT\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ascTIMEStructure\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ascDTStructure\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ascTIME\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ascDT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"TIMEStructure_TO_TIME\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"TIME_TO_TIMEStructure\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DTStructure_TO_DT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DT_TO_DTStructure\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DiffT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DiffDT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"TimeDeviceGetInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DstGetInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DstGetInfoDT\\\" FUB\\n\"");
#endif
#ifdef _BRSYSTEM_EXPORT
	__asm__(".ascii \"plcexport \\\"MEMInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"MEMxInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"SysInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RTInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TARGETInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"HWInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"SysconfInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"SysconfSet\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"BatteryInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"EXCInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ZYKVLenable\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"PMemGet\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"PMemPut\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"PMemSize\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ARwinWindowsInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ARwinEthWinInfo\\\" FUB\\n\"");
#endif
#ifdef _DATAOBJ_EXPORT
	__asm__(".ascii \"plcexport \\\"DatObjCreate\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DatObjWrite\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DatObjRead\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DatObjDelete\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DatObjMove\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DatObjCopy\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DatObjInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DatObjChangeDate\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DatObjAttach\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DatObjDetach\\\" FUB\\n\"");
#endif
#ifdef _FILEIO_EXPORT
	__asm__(".ascii \"plcexport \\\"FileCreate\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileOpen\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileClose\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileRead\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileReadEx\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileWrite\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileWriteEx\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileRename\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileCopy\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileDelete\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileTruncate\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirCreate\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirOpen\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirClose\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirRead\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirReadEx\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirRename\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirCopy\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirDelete\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DirDeleteEx\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"SetAttributes\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"GetAttributes\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DevMemInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DevLink\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DevUnlink\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"GetVolumeLabel\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"GetVolumeSerialNo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FileIoGetSysError\\\" FUN\\n\"");
#endif
#ifdef _STANDARD_EXPORT
	__asm__(".ascii \"plcexport \\\"RF_TRIG\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"CTUD\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"CTD\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"CTU\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"F_TRIG\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"R_TRIG\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"SR\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RS\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"SEMA\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TON\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TOF\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TP\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TON_10ms\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TOF_10ms\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TP_10ms\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"LEN\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"LEFT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RIGHT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MID\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"CONCAT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"INSERT\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DELETE\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"REPLACE\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FIND\\\" FUN\\n\"");
#endif
#ifdef _SYS_LIB_EXPORT
	__asm__(".ascii \"plcexport \\\"Byte2Bit\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"Bit2Byte\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"KEY_read\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"KEY_enadis\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_fix\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_info\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_copy\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_store\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_burn\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_delete\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_ident\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_read\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_write\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DA_create\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DIS_clr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DIS_chr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DIS_str\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ERRxfatal\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ERR_read\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ERRxread\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ERR_fatal\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ERR_warning\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ERRxwarning\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SM_release\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SM_attach\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SM_delete\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SM_ident\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SM_create\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"TIM_ticks\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"TIM_musec\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SW_settime\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SW_gettime\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RTC_settime\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RTC_gettime\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"TMP_free\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"TMP_alloc\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MEM_free\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MEM_alloc\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"AVT_info\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"AVT_release\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"AVT_attach\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"AVT_ident\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"AVT_cancel\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"AVT_create\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"UT_sleep\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"UT_exit\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"UT_freemsg\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"UT_recmsg\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"UT_sendmsg\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"UT_resume\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"UT_suspend\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"UT_ident\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ST_name\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ST_info\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ST_allsuspend\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ST_tmp_resume\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ST_tmp_suspend\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ST_resume\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ST_suspend\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ST_ident\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FORCE_info\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MO_ver\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MO_list\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MO_info\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"SYS_battery\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SYSreset\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SYSxinfo\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"SYS_info\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_xlist\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_ninfo\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_item\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_ident\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_list\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_info\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_xgetval\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_xsetval\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_xgetadr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_getadr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_getval\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"PV_setval\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"slMoList\\\" FUB\\n\"");
#endif
#ifdef _ASTCP_EXPORT
	__asm__(".ascii \"plcexport \\\"TcpOpen\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TcpOpenSsl\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TcpServer\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TcpClient\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TcpClose\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TcpSend\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TcpRecv\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"TcpIoctl\\\" FUB\\n\"");
#endif
#ifdef _ASHTTP_EXPORT
	__asm__(".ascii \"plcexport \\\"httpService\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpClient\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpsService\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpsClient\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpUtf8ToString\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpStringToUtf8\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpUtf8ToWString\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpWStringToUtf8\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpDecodeBase64\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpEncodeBase64\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpEncodeUrl\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpDecodeUrl\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpGetParamUrl\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpSetParamUrl\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpSetBoundary\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpGetBoundary\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpSetMultipartMessage\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"httpGetMultipartMessage\\\" FUB\\n\"");
#endif
#ifdef _ASICMP_EXPORT
	__asm__(".ascii \"plcexport \\\"IcmpPing\\\" FUB\\n\"");
#endif
#ifdef _ASUDP_EXPORT
	__asm__(".ascii \"plcexport \\\"UdpOpen\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UdpClose\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UdpConnect\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UdpDisconnect\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UdpSend\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UdpRecv\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UdpIoctl\\\" FUB\\n\"");
#endif
#ifdef _ASGUARD_EXPORT
	__asm__(".ascii \"plcexport \\\"guardRegisterLicense\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardCheckLicense\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardDeregisterLicense\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardGetContext\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardUpdateLicenses\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardGetDongles\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardReadOperatingTime\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardStartCustomOpTimeCounter\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardStopCustomOpTimeCounter\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardReadData\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardWriteData\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardGetStatus\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardGetLicenses\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardGetDongleLicenses\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"guardGetNeededLicenses\\\" FUB\\n\"");
#endif
#ifdef _AREVENTLOG_EXPORT
	__asm__(".ascii \"plcexport \\\"ArEventLogCreate\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogDelete\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogGetIdent\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogWrite\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogGetLatestRecordID\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogGetPreviousRecordID\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogRead\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogReadDescription\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogReadErrorNumber\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogReadAddData\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogReadObjectID\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogMakeEventID\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogIsError\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogIsWarning\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogIsInformation\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogIsSuccess\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogIsCustomerArea\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogAddDataInit\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogAddDataUdint\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogAddDataDint\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogAddDataString\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"ArEventLogAddDataQuantity\\\" FUN\\n\"");
#endif
#ifdef _ASBRMATH_EXPORT
	__asm__(".ascii \"plcexport \\\"brmatan2\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmceil\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmcosh\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmfloor\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmfmod\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmfrexp\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmldexp\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmmodf\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmpow\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmsinh\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brmtanh\\\" FUN\\n\"");
#endif
#ifdef _ASBRSTR_EXPORT
	__asm__(".ascii \"plcexport \\\"brsftoa\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsatof\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsatod\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsitoa\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsatoi\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsmemset\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsmemcpy\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsmemmove\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsmemcmp\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsstrcat\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsstrlen\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsstrcpy\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"brsstrcmp\\\" FUN\\n\"");
#endif
#ifdef _ASZIP_EXPORT
	__asm__(".ascii \"plcexport \\\"zipArchive\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"zipExtract\\\" FUB\\n\"");
#endif
#ifdef _ASIODIAG_EXPORT
	__asm__(".ascii \"plcexport \\\"DiagCreateInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DiagDisposeInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DiagGetNumInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DiagGetStrInfo\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"DiagCpuIsSimulated\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"DiagCpuIsARsim\\\" FUN\\n\"");
#endif
#ifdef _ASUSB_EXPORT
	__asm__(".ascii \"plcexport \\\"UsbNodeListGet\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UsbNodeGet\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UsbDescriptorGet\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"UsbMsDeviceReady\\\" FUB\\n\"");
#endif
#ifdef _CSCOM_EXPORT
	__asm__(".ascii \"plcexport \\\"CSComBasic\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"CSComAdvanced\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"CSComLinkCheck\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"CSComLinkMemCheck\\\" FUN\\n\"");
#endif
#ifdef _FDLINK_EXPORT
	__asm__(".ascii \"plcexport \\\"FDLinkBasic\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkBasicCopy\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkPathSplit\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkCreateParam\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkPathType\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkChangePathType\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkArchiveAddExt\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkFileAddExt\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkFileRemoveExt\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkFindPathIndx\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkCreatePath\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkPathLevel\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkUSBNum\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkFindPath\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkFindPathBoth\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkConfigPath\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkConfigPathAll\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"FDLinkDirHandle\\\" FUB\\n\"");
#endif
#ifdef _MTDIAG_EXPORT
	__asm__(".ascii \"plcexport \\\"MTDiagFillInfo\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MTDiagFillStatusInfo\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MTDiagFillInternalInfo\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MTDiagCreateStatusID\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MTDiagGetCodeFromStatusID\\\" FUN\\n\"");
#endif
#ifdef _MTHASH_EXPORT
	__asm__(".ascii \"plcexport \\\"MTHashMd5\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"MTHashMd5Decode\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"MTHashMd5Convert\\\" FUN\\n\"");
#endif
#ifdef _RBTEMPLATE_EXPORT
	__asm__(".ascii \"plcexport \\\"RBTemplateLicense\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateWobjDec\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateAngleToQuater\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateQuaterToAngle\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRestApiResetCmdEdge\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRestApiItox\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRestApiFindValue\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateGetFileNameFromPath\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRestApi\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"FillInInstructionData\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateTeaching\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRemoveModExt\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateToolDec\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateCommTcp\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRobotCtrlGetCount\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRobotCtrlRobotError\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRobotCtrlRobotEvent\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRobotCtrlAddEvent\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRobotCtrlSetError\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateRobotCtrl\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateTargetEditor\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateCmpVersion\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setZoneInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setStoppointInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setToolInst\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setPoseInst\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setWorkObjectInst\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setREALstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setINTstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setBOOLstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setToPointInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"CheckQs\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"settLoaddataInst\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setPredefStoppointInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setSpeedInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setTargetExtaxInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setTargetConfInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setOrientInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setTransInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"setMoveInstr\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateModGen\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateLogbookSetError\\\" FUN\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateLogbook\\\" FUB\\n\"");
	__asm__(".ascii \"plcexport \\\"RBTemplateIdentHandler\\\" FUB\\n\"");
#endif
#ifdef _RSCOMM_EXPORT
	__asm__(".ascii \"plcexport \\\"RsComm\\\" FUB\\n\"");
#endif

__asm__(".previous");


#endif /* _LIBRARIES_20210727141430_ */

