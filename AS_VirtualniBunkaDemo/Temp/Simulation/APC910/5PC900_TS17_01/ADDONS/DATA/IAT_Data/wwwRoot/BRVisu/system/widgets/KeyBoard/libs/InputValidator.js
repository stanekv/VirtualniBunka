define(['brease/core/Utils'],
    function (Utils) {

        'use strict';
        function InputValidator() {
            var self = this;
            self.value = '';
            self.init = function (eventDispatcher, widget) {
                self.widget = widget;
                self.eventDispatcher = eventDispatcher;
                self.eventDispatcher.addEventListener('Collector.Set', self.onSet);
                self.eventDispatcher.addEventListener('Collector.Change', self.onChange);
                self.eventDispatcher.addEventListener('Collector.Input', self.onInput);
                self.eventDispatcher.addEventListener('Collector.Submit', self.onSubmit);
                self.eventDispatcher.addEventListener('Collector.Clear', self.onClear);
                self.eventDispatcher.addEventListener('Processor.Change', self.setValue);
            };

            self.setValue = function (e) {
                self.value = e.detail.value;
            };

            self.getValue = function () {
                return self.value;
            };

            self.setRestriction = function (restriction) {
                if (restriction) {
                    self.regexp = new RegExp(restriction);
                } else {
                    self.regexp = undefined;
                }
            };

            self.onSet = function (e) {
                var value = self.validate(e.detail.value);
                self.eventDispatcher.dispatchEvent({
                    type: 'Validator.Set',
                    detail: {
                        'value': value
                    }
                });
            };

            self.onChange = function (e) {
                var inputValue = e.detail.value,
                    validatedValue = self.validate(inputValue);

                self.eventDispatcher.dispatchEvent({
                    type: 'Validator.Change',
                    detail: {
                        'value': validatedValue,
                        'originalValue': inputValue
                    }
                });
            };

            self.onInput = function (e) {
                var value = '';
                if (Utils.isString(e.detail.value)) {
                    value = self.validate(e.detail.value);
                }

                self.eventDispatcher.dispatchEvent({
                    type: 'Validator.Input',
                    detail: {
                        'value': value
                    }
                });
            };

            self.onSubmit = function () {
                var submitValue = self.validate(self.value);
                self.eventDispatcher.dispatchEvent({
                    type: 'Validator.Submit',
                    detail: {
                        'value': submitValue
                    }
                });
            };

            self.onClear = function () {
                self.setValue({ detail: { value: '' } });
            };

            self.validate = function (value) {
                return self.regexp === undefined ? value : value.split('').filter(function (actChar) {
                    return self.regexp.test(actChar);
                }).join('');
            };

            self.dispose = function () {
                self.eventDispatcher.removeEventListener('Collector.Set', self.onSet);
                self.eventDispatcher.removeEventListener('Collector.Change', self.onChange);
                self.eventDispatcher.removeEventListener('Collector.Input', self.onInput);
                self.eventDispatcher.removeEventListener('Collector.Submit', self.onSubmit);
                self.eventDispatcher.removeEventListener('Collector.Clear', self.onClear);
                self.eventDispatcher.removeEventListener('Processor.Change', self.setValue);
                self.value = '';
                self.restriction = undefined;
            };
        }

        return InputValidator;
    });
