
TYPE
	StationInputType : 	STRUCT 
		ConvSensor : BOOL; (*Presence of piece at the end of upper conveyor*)
		CameraDone : BOOL; (*Cords from camera are valid*)
		PalletOut1 : BOOL; (*Pallet 1 is outside CNC*)
		PalletOut2 : BOOL; (*Pallet 2 is outside CNC*)
		GripperOpen : BOOL; (*Gripper is open*)
		GripperClosed : BOOL; (*Gripper is closed*)
	END_STRUCT;
	StationOutputType : 	STRUCT 
		ConvUpperEnable : BOOL; (*Start/stop upper conveyor*)
		ConvLowerEnable : BOOL; (*Start/stop lower conveyor*)
		CameraExecute : BOOL; (*Get cords from camera*)
		CncStart1 : BOOL; (*Move pallet 1 to CNC*)
		CncStart2 : BOOL; (*Move pallet 2 to CNC*)
		GripperOpen : BOOL; (*Open gripper*)
		GripperClose : BOOL; (*Close gripper*)
	END_STRUCT;
END_TYPE
