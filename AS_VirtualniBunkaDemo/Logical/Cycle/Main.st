PROGRAM _INIT
	(* RobotCtrl *)
	RobotCtrl_0(Enable := TRUE, ControllerIpAdrSim := '127.0.0.1');
	
	(* RsComm *)
	RsComm_0.Enable := TRUE;
END_PROGRAM

PROGRAM _CYCLIC
	(*

	Vychozi stav: Sverak 1 je ve stroji s kusem. Sverak 2 je prazdny.
	
	1. Start robot
	2. Start dopravnik spodni
	3. Cekani na kus
	4. Zastaveni dopravniku
	5. Foto
	6. Cekani na kameru (BOOL)
	7. Prejezd k uchopeni
	9. Zavreni chapadla
	10. Odjezd od dopravniku
	11. Prejezd ke sverakum
		- zalozeni do 2
	12. Otevreni chapadla
	13. Odjezd do home
	13. Start obrabeni
		- sverak 2 ve stroji
	14. Pockani na vyjezd sveraku
		- sverak 1 venku
	15. Uchopeni obrobku
	16. Polozeni na spodni dopravnik
	17. Start spodniho dopravniku na 2 s.
	
	18. Robot pro dalsi kus (2 - 13) 
	
	*)
	 
	StationIn.ConvSensor := RsComm_0.InputBuffer.Digital[IN_CONV_SENSOR];
	StationIn.CameraDone := RsComm_0.InputBuffer.Digital[IN_CAMERA_DONE];
	StationIn.PalletOut1 := RsComm_0.InputBuffer.Digital[IN_PALLET_OUT1];
	StationIn.PalletOut2 := RsComm_0.InputBuffer.Digital[IN_PALLET_OUT2];
	StationIn.GripperOpen := RsComm_0.InputBuffer.Digital[IN_GRIPPER_OPEN];
	StationIn.GripperClosed := RsComm_0.InputBuffer.Digital[IN_GRIPPER_CLOSED];
	
	CASE State OF
		
		0: (* Initial state *)
			
		1: (* Zero output *)
			IF TON_0.Q THEN
				TON_0.IN := FALSE;
				Cycle := 0;
				State := 2;
			ELSE
				TON_0.IN := TRUE;
				TON_0.PT := TIME#2s;
				brsmemset(ADR(StationOut), 0, SIZEOF(StationOut));
			END_IF;
		
		2: (* Init Pallet 1*)
			IF TON_0.Q THEN
				StationOut.CncStart1 := FALSE;
				TON_0.IN := FALSE;
				State := 3;
			ELSE
				StationOut.CncStart1 := TRUE;
				TON_0.IN := TRUE;
				TON_0.PT := TIME#2s;
			END_IF;
			
		3: (* Init Pallet 2*)
			IF TON_0.Q THEN
				StationOut.CncStart2 := FALSE;
				TON_0.IN := FALSE;
				State := 5;
			ELSE
				StationOut.CncStart2 := TRUE;
				TON_0.IN := TRUE;
				TON_0.PT := TIME#2s;
			END_IF;
		
		5: (* Init CNC *)
			IF StationIn.PalletOut2 THEN
				StationOut.CncStart1 := FALSE;
				StationOut.CncStart2 := FALSE;
				State := 10;
			ELSE
				StationOut.CncStart1 := TRUE;
				StationOut.CncStart2 := FALSE;
			END_IF;
		
		10: (* Start motors *)
			IF RobotCtrl_0.ControllerStatus.MotorsOn THEN
				RobotCtrl_0.ControllerInput.MotorsOn := FALSE;
				State := 20;
			ELSE		
				RobotCtrl_0.ControllerInput.MotorsOn := TRUE;
			END_IF;
			
		20: (* Start motion tasks / start at main *)
			IF RobotCtrl_0.ControllerStatus.ExecutingMotionTask THEN
				RobotCtrl_0.ControllerInput.StartAtMain := FALSE;
				State := 30;
			ELSE
				RobotCtrl_0.ControllerInput.StartAtMain := TRUE;
			END_IF;	
						
		30: (* Start upper conveyor and wait for piece *)
			IF StationIn.ConvSensor THEN
				StationOut.ConvUpperEnable := FALSE;
				State := 35;
			ELSE
				StationOut.ConvUpperEnable := TRUE;
			END_IF;
			
		35: (* Start camera *)
			IF StationIn.CameraDone THEN
				StationOut.CameraExecute := FALSE;
				RobotCtrl_0.RobotInput[0].RelToolCords.X := RsComm_0.InputBuffer.Analog[CAMERA_CORDS_X];
				RobotCtrl_0.RobotInput[0].RelToolCords.Y := RsComm_0.InputBuffer.Analog[CAMERA_CORDS_Y];
				RobotCtrl_0.RobotInput[0].RelToolCords.Z := 0;
				RobotCtrl_0.RobotInput[0].RelToolCords.Rx := 0;
				RobotCtrl_0.RobotInput[0].RelToolCords.Ry := 0;
				RobotCtrl_0.RobotInput[0].RelToolCords.Rz := RsComm_0.InputBuffer.Analog[CAMERA_CORDS_RZ];
				State := 40;
			ELSE
				StationOut.CameraExecute := TRUE;
			END_IF;
			
		40: (* Go to conveyor *)
			IF RobotCtrl_0.RobotStatus[0].ProcDone THEN
				RobotCtrl_0.RobotInput[0].StartProcedure := FALSE;
				RobotCtrl_0.RobotInput[0].RelTool := FALSE;
				State := 50;
			ELSE
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvHome_ConvIN';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
				RobotCtrl_0.RobotInput[0].RelTool := TRUE;
			END_IF;
			
		50: (* Close gripper *)
			IF StationIn.GripperClosed THEN
				StationOut.GripperClose := FALSE;
				StationOut.GripperOpen := FALSE;
				State := 60;
			ELSE
				StationOut.GripperClose := TRUE;
				StationOut.GripperOpen := FALSE;
			END_IF;
			
		60: (* Go to home *)
			IF RobotCtrl_0.RobotStatus[0].ProcDone THEN
				RobotCtrl_0.RobotInput[0].StartProcedure := FALSE;
				State := 70;
			ELSE
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvConv_Home';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
			END_IF;
			
		70: (* Go to CNC *)
			IF RobotCtrl_0.RobotStatus[0].ProcDone THEN
				RobotCtrl_0.RobotInput[0].StartProcedure := FALSE;
				State := 80;
			ELSIF StationIn.PalletOut1 THEN
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvHome_CNC1';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
			ELSIF StationIn.PalletOut2 THEN
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvHome_CNC2';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
			ELSE
				(* wait for cnc *)
			END_IF;	
			
		80: (* Open gripper *)
			IF StationIn.GripperOpen THEN
				StationOut.GripperClose := FALSE;
				StationOut.GripperOpen := FALSE;
				State := 90;
			ELSE
				StationOut.GripperClose := FALSE;
				StationOut.GripperOpen := TRUE;
			END_IF;	
			
		90: (* Move to home *)
			IF RobotCtrl_0.RobotStatus[0].ProcDone THEN
				RobotCtrl_0.RobotInput[0].StartProcedure := FALSE;
				State := 100;
			ELSE
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvCNC_Home';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
			END_IF;	
			
		100: (* Start CNC *)
			IF StationIn.PalletOut1 THEN
				StationOut.CncStart1 := TRUE;
				StationOut.CncStart2 := FALSE;
				State := 105;
			ELSIF StationIn.PalletOut2 THEN
				StationOut.CncStart1 := FALSE;
				StationOut.CncStart2 := TRUE;
				State := 106;
			ELSE
				(* ... *)
			END_IF;	
			
		105: (* Wait for Pallet 2 *)
			IF StationIn.PalletOut2 THEN
				StationOut.CncStart1 := FALSE;
				StationOut.CncStart2 := FALSE;
				State := 109;
			ELSE
				StationOut.CncStart1 := TRUE;
				StationOut.CncStart2 := FALSE;
			END_IF;
			
		106: (* Wait for Pallet 1 *)
			IF StationIn.PalletOut1 THEN
				StationOut.CncStart1 := FALSE;
				StationOut.CncStart2 := FALSE;
				State := 109;
			ELSE
				StationOut.CncStart1 := FALSE;
				StationOut.CncStart2 := TRUE;
			END_IF;
			
		109: (* For first cycle do not pick piece from CNC, insert another one *)
			IF Cycle = 0 THEN
				Cycle := Cycle +1;
				State := 30;
			ELSE
				State := 110;
			END_IF;
				
		110: (* Pick piece from CNC *)
			IF RobotCtrl_0.RobotStatus[0].ProcDone THEN
				RobotCtrl_0.RobotInput[0].StartProcedure := FALSE;
				State := 120;
			ELSIF StationIn.PalletOut1 THEN
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvHome_CNC1';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
			ELSIF StationIn.PalletOut2 THEN
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvHome_CNC2';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
			ELSE
				(* wait for cnc *)
			END_IF;	
			
		120: (* Close gripper *)
			IF StationIn.GripperClosed THEN
				StationOut.GripperClose := FALSE;
				StationOut.GripperOpen := FALSE;
				State := 130;
			ELSE
				StationOut.GripperClose := TRUE;
				StationOut.GripperOpen := FALSE;
			END_IF;	
			
		130: (* Move to home *)
			IF RobotCtrl_0.RobotStatus[0].ProcDone THEN
				RobotCtrl_0.RobotInput[0].StartProcedure := FALSE;
				State := 140;
			ELSE
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvCNC_Home';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
			END_IF;	
			
		140: (* Move to lower conveyor *)
			IF RobotCtrl_0.RobotStatus[0].ProcDone THEN
				RobotCtrl_0.RobotInput[0].StartProcedure := FALSE;
				State := 150;
			ELSE
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvHome_ConvOUT';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
			END_IF;	
			
		150: (* Open gripper, start conveyor *)
			IF StationIn.GripperOpen THEN
				StationOut.GripperClose := FALSE;
				StationOut.GripperOpen := FALSE;
				StationOut.ConvLowerEnable := TRUE;
				TON_0.IN := TRUE;
				TON_0.PT := TIME#2s;
				State := 160;
			ELSE
				StationOut.GripperClose := FALSE;
				StationOut.GripperOpen := TRUE;
			END_IF;
		
		160: (* Move to home *)
			IF RobotCtrl_0.RobotStatus[0].ProcDone THEN
				RobotCtrl_0.RobotInput[0].StartProcedure := FALSE;
				Cycle := Cycle + 1;
				State := 30;
			ELSE
				RobotCtrl_0.RobotInput[0].ProcedureName := 'mvCNC_Home';
				RobotCtrl_0.RobotInput[0].StartProcedure := TRUE;
			END_IF;	
	END_CASE;
	
	TON_0();
	TON_0.IN := TON_0.IN AND NOT TON_0.Q;
	
	StationOut.ConvLowerEnable := StationOut.ConvLowerEnable AND NOT TON_0.Q;
	
	(* connect signals to RsComm *)
	RsComm_0.OutputBuffer.Digital[OUT_CONV_UPPER_EN] := StationOut.ConvUpperEnable;
	RsComm_0.OutputBuffer.Digital[OUT_CONV_LOWER_EN] := StationOut.ConvLowerEnable;
	RsComm_0.OutputBuffer.Digital[OUT_CAMERA_EXEC] := StationOut.CameraExecute;
	RsComm_0.OutputBuffer.Digital[OUT_CNC_START1] := StationOut.CncStart1;
	RsComm_0.OutputBuffer.Digital[OUT_CNC_START2] := StationOut.CncStart2;
	RsComm_0.OutputBuffer.Digital[OUT_GRIPPER_OPEN] := StationOut.GripperOpen;
	RsComm_0.OutputBuffer.Digital[OUT_GRIPPER_CLOSE] := StationOut.GripperClose;
	
	(* RBTemplateRobotCtrl *)
	RobotCtrl_0();
	
	(* RsComm *)
	RsComm_0();
END_PROGRAM

PROGRAM _EXIT
	(* RBTemplateRobotCtrl *)
	RobotCtrl_0(Enable := FALSE);
	
	(* RsComm *)
	RsComm_0(Enable := FALSE);
END_PROGRAM

