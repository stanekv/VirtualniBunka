#include <bur/plctypes.h>
#include <math.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	/* set parameters */
	brsmemcpy((UDINT) &RBTemplateTeaching_0.ControllerIpAdr, (UDINT) &ControllerIpAdr, sizeof(RBTemplateTeaching_0.ControllerIpAdr));
	brsmemcpy((UDINT) &RBTemplateTeaching_0.ControllerIpAdrSim, (UDINT) &ControllerIpAdrSim, sizeof(RBTemplateTeaching_0.ControllerIpAdrSim));
	
	/* declare MoveInstructionList_0 before use */
	/* set parameters */
	RBTemplateTeaching_0.Parameters.pInstructionList = (RBTemplateABBMoveInstrListType*) &(MoveInstructionList_0);
	RBTemplateTeaching_0.Parameters.InstructionListLen = sizeof(MoveInstructionList_0);
	brsmemcpy((UDINT) &RBTemplateTeaching_0.Parameters.Path, (UDINT) &Path, sizeof(RBTemplateTeaching_0.Parameters.Path));
	brsmemcpy((UDINT) &RBTemplateTeaching_0.Parameters.PathSim, (UDINT) &PathSim, sizeof(RBTemplateTeaching_0.Parameters.PathSim));
	brsmemcpy((UDINT) &RBTemplateTeaching_0.Parameters.FileName, 
		(UDINT) &("MODULE1.mod"), 
		sizeof(RBTemplateTeaching_0.Parameters.FileName));
}

void _CYCLIC ProgramCyclic(void)
{
	/* visu service page */
		
	/* enable */
	RBTemplateTeaching_0.Enable = gRobotTeaching.Enable;
	
	/* error reset */
	RBTemplateTeaching_0.ErrorReset = gRobotTeaching.ErrorReset;
	
	/* file */
	brsmemcpy((UDINT) &RBTemplateTeaching_0.Parameters.Path, (UDINT) &Path, sizeof(RBTemplateTeaching_0.Parameters.Path));
	brsmemcpy((UDINT) &RBTemplateTeaching_0.Parameters.PathSim, (UDINT) &PathSim, sizeof(RBTemplateTeaching_0.Parameters.PathSim));
	brsstrcpy((UDINT) &RBTemplateTeaching_0.Parameters.FileName, (UDINT) &gRobotTeaching.ProcedureName);
	brsstrcat((UDINT) &RBTemplateTeaching_0.Parameters.FileName, (UDINT) &(".mod"));
	
	RBTemplateTeaching_0.Parameters.KeepPositionOnEdit = gRobotTeaching.KeepTargetPosition;
	
	/* robot index */
	RBTemplateTeaching_0.Parameters.RobotIndex = gRobotTeaching.RobotIndex;
	
	/* instruction parameters */
	brsmemcpy((UDINT) &RBTemplateTeaching_0.Parameters.Instruction, (UDINT) &gRobotTeaching.Instruction, sizeof(RBTemplateTeachingParInstrType));
	
	/* teaching input */
	brsmemcpy((UDINT) &RBTemplateTeaching_0.Commands, (UDINT) &gRobotTeaching.Command, sizeof(RBTemplateTeachingCmdType));
			
	/* call FUB */
	RBTemplateTeaching(&RBTemplateTeaching_0);
	
	/* active flag */
	gRobotTeaching.Active = RBTemplateTeaching_0.Active;
	/* error flag */
	gRobotTeaching.Error = RBTemplateTeaching_0.Error;
	/* error code */
	gRobotTeaching.ErrorCode = RBTemplateTeaching_0.Info.Diag.StatusID.Code;
	
	/* max robot index */
	gRobotTeaching.RobotIndexMax = (RBTemplateTeaching_0.Info.Controller.RobotCount > 0) ? RBTemplateTeaching_0.Info.Controller.RobotCount-1 : 0;
	/* instructions count */
	gRobotTeaching.InstrCount = RBTemplateTeaching_0.Info.InstructionsCount;
	/* motion data */
	gRobotTeaching.MotionData = RBTemplateTeaching_0.MotionData;
	
//	brsstrcpy((UDINT) &gRobotTeaching.InstructionList.TableRows, 
//		(UDINT) &("{\"specRows\": [ {\"from\":0, \"to\":10, \"visible\":true}]}"));
	
	/* convert MoveInstructionList to format for mapp View table */
	brsmemset((UDINT) &gRobotTeaching.InstructionList, 0, sizeof(gRobotTeaching.InstructionList));
	unsigned int index;
	for (index = 0; index < RBTemplateTeaching_0.Info.InstructionsCount; index++)
	{
		brsstrcpy((UDINT) &gRobotTeaching.InstructionList.Name[index], (UDINT) &MoveInstructionList_0[index].Name);
		gRobotTeaching.InstructionList.Index[index] = index;
		brsitoa(MoveInstructionList_0[index].PredefinedSpeed, (UDINT) &gRobotTeaching.InstructionList.PredefinedSpeed[index][0]);
		
		if (MoveInstructionList_0[index].PredefinedZone == abbZONE_FINE)
			brsstrcpy((UDINT) &gRobotTeaching.InstructionList.PredefinedZone[index][0], (UDINT) &("fine"));
		else
			brsitoa(MoveInstructionList_0[index].PredefinedZone, (UDINT) &gRobotTeaching.InstructionList.PredefinedZone[index][0]);
		
		if (MoveInstructionList_0[index].Type == abbMOVE_J)
			brsstrcpy((UDINT) &gRobotTeaching.InstructionList.Type[index][0], (UDINT) &("MoveJ"));
		else if (MoveInstructionList_0[index].Type == abbMOVE_L)
			brsstrcpy((UDINT) &gRobotTeaching.InstructionList.Type[index][0], (UDINT) &("MoveL"));
	}
}

void _EXIT ProgramExit(void)
{
	RBTemplateTeaching_0.Enable = 0;
	RBTemplateTeaching(&RBTemplateTeaching_0);
}
