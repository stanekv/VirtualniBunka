#include <bur/plctypes.h>
#include <math.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	/* set parameters */
	brsmemcpy((UDINT) &RBTemplateRobotCtrl_0.ControllerIpAdr, (UDINT) &ControllerIpAdr, sizeof(RBTemplateRobotCtrl_0.ControllerIpAdr));
	brsmemcpy((UDINT) &RBTemplateRobotCtrl_0.ControllerIpAdrSim, (UDINT) &ControllerIpAdrSim, sizeof(RBTemplateRobotCtrl_0.ControllerIpAdrSim));
		
	/* save file from controller parameters */
	brsstrcpy((UDINT) &RBTemplateRobotCtrl_0.ControllerInput.DstPath, (UDINT) &DstPath);
	brsstrcpy((UDINT) &RBTemplateRobotCtrl_0.ControllerInput.DstPathSim, (UDINT) &DstPathSim);
//	brsstrcpy((UDINT) &RBTemplateRobotCtrl_0.ControllerInput.SrcFile, (UDINT) &RobotFile);
	
	/* default paths */
	brsstrcpy((UDINT) &gRobotCtrl.RobotInput1.ModuleSrcPath, (UDINT) &SrcPath);
	brsstrcpy((UDINT) &gRobotCtrl.RobotInput2.ModuleSrcPath, (UDINT) &SrcPath);
	brsstrcpy((UDINT) &gRobotCtrl.RobotInput3.ModuleSrcPath, (UDINT) &SrcPath);
	brsstrcpy((UDINT) &gRobotCtrl.RobotInput4.ModuleSrcPath, (UDINT) &SrcPath);

	brsstrcpy((UDINT) &gRobotCtrl.RobotInput1.ModuleSrcPathSim, (UDINT) &SrcPathSim);
	brsstrcpy((UDINT) &gRobotCtrl.RobotInput2.ModuleSrcPathSim, (UDINT) &SrcPathSim);
	brsstrcpy((UDINT) &gRobotCtrl.RobotInput3.ModuleSrcPathSim, (UDINT) &SrcPathSim);
	brsstrcpy((UDINT) &gRobotCtrl.RobotInput4.ModuleSrcPathSim, (UDINT) &SrcPathSim);
	
	/* set default speed override */
	gRobotCtrl.RobotInput1.SpeedOverride = 20;
	gRobotCtrl.RobotInput2.SpeedOverride = 20;
	gRobotCtrl.RobotInput3.SpeedOverride = 20;
	gRobotCtrl.RobotInput4.SpeedOverride = 20;
	
	/* default module and proc */
	brsstrcpy((unsigned long) &gRobotCtrl.RobotInput1.ModuleName, (unsigned long) &("testicek.mod"));
	brsstrcpy((unsigned long) &gRobotCtrl.RobotInput2.ModuleName, (unsigned long) &("testicek_ext.mod"));
	brsstrcpy((unsigned long) &gRobotCtrl.RobotInput3.ModuleName, (unsigned long) &("testicek.mod"));
	brsstrcpy((unsigned long) &gRobotCtrl.RobotInput4.ModuleName, (unsigned long) &("testicek.mod"));
	
	brsstrcpy((unsigned long) &gRobotCtrl.RobotInput1.ProcedureName, (unsigned long) &("ptesticek"));
	brsstrcpy((unsigned long) &gRobotCtrl.RobotInput2.ProcedureName, (unsigned long) &("ptesticek_ext"));
	brsstrcpy((unsigned long) &gRobotCtrl.RobotInput3.ProcedureName, (unsigned long) &("ptesticek"));
	brsstrcpy((unsigned long) &gRobotCtrl.RobotInput4.ProcedureName, (unsigned long) &("ptesticek"));
	
	
}

void _CYCLIC ProgramCyclic(void)
{
	/* visu service page */
	/* motion data */
	if (gRobotCtrl.RobotIndex >= RBTemplateRobotCtrl_0.Info.Controller.RobotCount)
		brsmemset((UDINT) &gRobotCtrl.MotionData, 0, sizeof(RBTemplateRobotMotionDataType));
	else
		brsmemcpy((UDINT) &gRobotCtrl.MotionData, (UDINT) &RBTemplateRobotCtrl_0.MotionData[gRobotCtrl.RobotIndex], sizeof(RBTemplateRobotMotionDataType));
		
	/* enable */
	RBTemplateRobotCtrl_0.Enable = gRobotCtrl.Enable;
	
	/* error reset */
	RBTemplateRobotCtrl_0.ErrorReset = gRobotCtrl.ErrorReset;
		
	/* robot input */
	brsmemcpy((UDINT) &RBTemplateRobotCtrl_0.RobotInput[0], (UDINT) &gRobotCtrl.RobotInput1, sizeof(RBTemplateRobotCtrlInputRobType));
	brsmemcpy((UDINT) &RBTemplateRobotCtrl_0.RobotInput[1], (UDINT) &gRobotCtrl.RobotInput2, sizeof(RBTemplateRobotCtrlInputRobType));
	brsmemcpy((UDINT) &RBTemplateRobotCtrl_0.RobotInput[2], (UDINT) &gRobotCtrl.RobotInput3, sizeof(RBTemplateRobotCtrlInputRobType));
	brsmemcpy((UDINT) &RBTemplateRobotCtrl_0.RobotInput[3], (UDINT) &gRobotCtrl.RobotInput4, sizeof(RBTemplateRobotCtrlInputRobType));
	
	/* controller input */
	brsmemcpy((UDINT) &RBTemplateRobotCtrl_0.ControllerInput, (UDINT) &gRobotCtrl.ControllerInput, sizeof(RBTemplateRobotCtrlInputCmdType));
			
	/* call FUB */
	RBTemplateRobotCtrl(&RBTemplateRobotCtrl_0);
	
	/* error code */
	gRobotCtrl.ErrorCode = RBTemplateRobotCtrl_0.Info.Diag.StatusID.Code;
	
	/* controller status */
	gRobotCtrl.ControllerStatus = RBTemplateRobotCtrl_0.ControllerStatus;
	
	/* robot status */
	brsmemcpy((UDINT) &gRobotCtrl.RobotStatus1, (UDINT) &RBTemplateRobotCtrl_0.RobotStatus[0], sizeof(RBTemplateRobotCtrlRobInfoType));
	brsmemcpy((UDINT) &gRobotCtrl.RobotStatus2, (UDINT) &RBTemplateRobotCtrl_0.RobotStatus[1], sizeof(RBTemplateRobotCtrlRobInfoType));
	brsmemcpy((UDINT) &gRobotCtrl.RobotStatus3, (UDINT) &RBTemplateRobotCtrl_0.RobotStatus[2], sizeof(RBTemplateRobotCtrlRobInfoType));
	brsmemcpy((UDINT) &gRobotCtrl.RobotStatus4, (UDINT) &RBTemplateRobotCtrl_0.RobotStatus[3], sizeof(RBTemplateRobotCtrlRobInfoType));
		
}

void _EXIT ProgramExit(void)
{
	RBTemplateRobotCtrl_0.Enable = 0;
	RBTemplateRobotCtrl(&RBTemplateRobotCtrl_0);
}
