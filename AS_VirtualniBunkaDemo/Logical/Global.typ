(*Robot*)

TYPE
	gRobotCtrlInterfaceType : 	STRUCT 
		Enable : BOOL;
		Error : BOOL;
		ErrorCode : UINT;
		ErrorReset : BOOL;
		ControllerInput : RBTemplateRobotCtrlInputCmdType;
		ControllerStatus : RBTemplateRobotCtrlStatusType;
		RobotInput1 : RBTemplateRobotCtrlInputRobType;
		RobotInput2 : RBTemplateRobotCtrlInputRobType;
		RobotInput3 : RBTemplateRobotCtrlInputRobType;
		RobotInput4 : RBTemplateRobotCtrlInputRobType;
		RobotStatus1 : RBTemplateRobotCtrlRobInfoType;
		RobotStatus2 : RBTemplateRobotCtrlRobInfoType;
		RobotStatus3 : RBTemplateRobotCtrlRobInfoType;
		RobotStatus4 : RBTemplateRobotCtrlRobInfoType;
		RobotIndex : USINT;
		MotionData : RBTemplateRobotMotionDataType;
	END_STRUCT;
	gRobotTeachingVisuInstrListType : 	STRUCT 
		Index : ARRAY[0..29]OF UINT;
		Type : ARRAY[0..29]OF STRING[12];
		PredefinedSpeed : ARRAY[0..29]OF STRING[10];
		PredefinedZone : ARRAY[0..29]OF STRING[10];
		Name : ARRAY[0..29]OF STRING[32];
		TableRows : STRING[200];
	END_STRUCT;
	gRobotTeachingInterfaceType : 	STRUCT 
		Enable : BOOL;
		Error : BOOL;
		ErrorCode : UINT;
		ErrorReset : BOOL;
		Active : BOOL;
		Command : RBTemplateTeachingCmdType;
		Instruction : RBTemplateTeachingParInstrType;
		InstrCount : UINT;
		Index : UINT;
		KeepTargetPosition : BOOL;
		RobotIndex : USINT;
		RobotIndexMax : USINT;
		MotionData : RBTemplateRobotMotionDataType;
		ProcedureName : STRING[36];
		InstructionList : gRobotTeachingVisuInstrListType;
	END_STRUCT;
END_TYPE
