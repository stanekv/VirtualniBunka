
{REDUND_OK} FUNCTION_BLOCK MTHashMd5 (*Creates MD5 Hash from input string.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Execute : BOOL;
		ErrorReset : BOOL;
		pString : REFERENCE TO UDINT;
		StringLength : UDINT;
	END_VAR
	VAR_OUTPUT
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL;
		Result : STRING[32];
		StatusID : DINT;
	END_VAR
	VAR
		Internal : MTHashMd5InternalType;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION MTHashMd5Decode : USINT
	VAR_INPUT
		Input : REFERENCE TO BYTE;
		Output : REFERENCE TO UDINT;
		OutputLen : UDINT;
	END_VAR
END_FUNCTION

FUNCTION MTHashMd5Convert : USINT
	VAR_INPUT
		pInput : REFERENCE TO UDINT;
		pStr : UDINT;
	END_VAR
END_FUNCTION
