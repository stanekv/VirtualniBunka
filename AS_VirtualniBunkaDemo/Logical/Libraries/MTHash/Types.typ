
TYPE
	MTHashMd5StateEnum : 
		(
		mtHASH_MD5_INIT := 0,
		mtHASH_MD5_PREPARE := 10,
		mtHASH_MD5_PROCESS := 20,
		mtHASH_MD5_DONE := 90,
		mtHASH_MD5_ERROR := 100
		);
	MTHashMd5ErrorEnum : 
		(
		mtHASH_MD5_ERR_INPUT_INV_PARAM := 0, (*Any of the input parameters is invalid.*)
		mtHASH_MD5_ERR_INPUT_TOO_LONG := 10,
		mtHASH_MD5_ERR_BUFFER_SIZE := 20
		);
	MTHashMd5InternalStatusType : 	STRUCT 
		ExecuteOld : BOOL; (*Value of Execute input from previous cycle*)
		ErrorResetOld : BOOL; (*Value of ErrorReset input from previous cycle*)
	END_STRUCT;
	MTHashMd5InternalType : 	STRUCT 
		Count : UDINT; (*Number of bits, modulo 2^64*)
		Buffer : ARRAY[0..mtHASH_MD5_BUFFER_SIZE]OF BYTE; (*Contains padded message (because malloc cannot be used!)*)
		BlocksBuffer : ARRAY[0..15]OF UDINT; (*Buffer for actual hash algorithm*)
		ActChunk : UDINT; (*Currently processed chunk*)
		ChunksCount : UDINT; (*Number of 512-bits chunks*)
		HashBuffer : ARRAY[0..3]OF UDINT; (*Buffer for actual hash algorithm*)
		Hash : ARRAY[0..3]OF UDINT; (*Result is stored here*)
		ResultBuffer : ARRAY[0..31]OF BYTE; (*Buffer for UDINT -> HEX string conversion*)
		StrLen : ARRAY[0..1]OF UDINT; (*64-bit (2*32-bit) representation of input string length (low-order word first)*)
		NewStrLen : UDINT; (*String length after padding*)
		Status : MTHashMd5InternalStatusType;
		State : MTHashMd5StateEnum; (*State*)
	END_STRUCT;
END_TYPE
