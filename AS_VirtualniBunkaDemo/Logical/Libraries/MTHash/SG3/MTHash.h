/* Automation Studio generated header file */
/* Do not edit ! */
/* MTHash  */

#ifndef _MTHASH_
#define _MTHASH_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
/* Constants */
#ifdef _REPLACE_CONST
 _WEAK const unsigned long mtHASH_MD5_KONST[64] = {3614090360U,3905402710U,606105819U,3250441966U,4118548399U,1200080426U,2821735955U,4249261313U,1770035416U,2336552879U,4294925233U,2304563134U,1804603682U,4254626195U,2792965006U,1236535329U,4129170786U,3225465664U,643717713U,3921069994U,3593408605U,38016083U,3634488961U,3889429448U,568446438U,3275163606U,4107603335U,1163531501U,2850285829U,4243563512U,1735328473U,2368359562U,4294588738U,2272392833U,1839030562U,4259657740U,2763975236U,1272893353U,4139469664U,3200236656U,681279174U,3936430074U,3572445317U,76029189U,3654602809U,3873151461U,530742520U,3299628645U,4096336452U,1126891415U,2878612391U,4237533241U,1700485571U,2399980690U,4293915773U,2240044497U,1873313359U,4264355552U,2734768916U,1309151649U,4149444226U,3174756917U,718787259U,3951481745U};
 _WEAK const unsigned char mtHASH_MD5_SHIFT[64] = {7U,12U,17U,22U,7U,12U,17U,22U,7U,12U,17U,22U,7U,12U,17U,22U,5U,9U,14U,20U,5U,9U,14U,20U,5U,9U,14U,20U,5U,9U,14U,20U,4U,11U,16U,23U,4U,11U,16U,23U,4U,11U,16U,23U,4U,11U,16U,23U,6U,10U,15U,21U,6U,10U,15U,21U,6U,10U,15U,21U,6U,10U,15U,21U};
 #define mtHASH_MD5_BUFFER_SIZE 1000U
#else
 _GLOBAL_CONST unsigned long mtHASH_MD5_KONST[64];
 _GLOBAL_CONST unsigned char mtHASH_MD5_SHIFT[64];
 _GLOBAL_CONST unsigned long mtHASH_MD5_BUFFER_SIZE;
#endif




/* Datatypes and datatypes of function blocks */
typedef enum MTHashMd5StateEnum
{	mtHASH_MD5_INIT = 0,
	mtHASH_MD5_PREPARE = 10,
	mtHASH_MD5_PROCESS = 20,
	mtHASH_MD5_DONE = 90,
	mtHASH_MD5_ERROR = 100
} MTHashMd5StateEnum;

typedef enum MTHashMd5ErrorEnum
{	mtHASH_MD5_ERR_INPUT_INV_PARAM = 0,
	mtHASH_MD5_ERR_INPUT_TOO_LONG = 10,
	mtHASH_MD5_ERR_BUFFER_SIZE = 20
} MTHashMd5ErrorEnum;

typedef struct MTHashMd5InternalStatusType
{	plcbit ExecuteOld;
	plcbit ErrorResetOld;
} MTHashMd5InternalStatusType;

typedef struct MTHashMd5InternalType
{	unsigned long Count;
	plcbyte Buffer[1001];
	unsigned long BlocksBuffer[16];
	unsigned long ActChunk;
	unsigned long ChunksCount;
	unsigned long HashBuffer[4];
	unsigned long Hash[4];
	plcbyte ResultBuffer[32];
	unsigned long StrLen[2];
	unsigned long NewStrLen;
	struct MTHashMd5InternalStatusType Status;
	enum MTHashMd5StateEnum State;
} MTHashMd5InternalType;

typedef struct MTHashMd5
{
	/* VAR_INPUT (analog) */
	unsigned long* pString;
	unsigned long StringLength;
	/* VAR_OUTPUT (analog) */
	plcstring Result[33];
	signed long StatusID;
	/* VAR (analog) */
	struct MTHashMd5InternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Execute;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Done;
	plcbit Busy;
	plcbit Error;
} MTHashMd5_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void MTHashMd5(struct MTHashMd5* inst);
_BUR_PUBLIC unsigned char MTHashMd5Decode(plcbyte* Input, unsigned long* Output, unsigned long OutputLen);
_BUR_PUBLIC unsigned char MTHashMd5Convert(unsigned long* pInput, unsigned long pStr);


#ifdef __cplusplus
};
#endif
#endif /* _MTHASH_ */

