
FUNCTION MTDiagFillInfo : BOOL (*Calls MTDiagFillStatusInfo and MTDiagFillInternalInfo functions*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		StatusID : REFERENCE TO DINT;
		InternalID : REFERENCE TO DINT;
		Info : MTDiagType;
	END_VAR
END_FUNCTION

FUNCTION MTDiagFillStatusInfo : BOOL (*Divides given StatusID and fills StatusID info structure*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		StatusID : REFERENCE TO DINT;
		Info : MTDiagStatusIDType;
	END_VAR
END_FUNCTION

FUNCTION MTDiagFillInternalInfo : BOOL (*Divides given InternalID and fills InternaIID info structure*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		InternalID : REFERENCE TO DINT;
		Info : MTDiagInternalIDType;
	END_VAR
END_FUNCTION

FUNCTION MTDiagCreateStatusID : DINT (*Creates StatusID from given Severity, Facility and Code*)
	VAR_INPUT
		Severity : BYTE;
		Facility : DINT;
		Code : DINT;
		StatusID : REFERENCE TO DINT;
	END_VAR
END_FUNCTION

FUNCTION MTDiagGetCodeFromStatusID : DINT (*Returns Code from given StatusID*)
	VAR_INPUT
		StatusID : DINT;
		Code : REFERENCE TO DINT;
	END_VAR
END_FUNCTION
