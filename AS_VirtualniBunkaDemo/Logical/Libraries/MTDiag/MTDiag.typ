
TYPE
	MTDiagSeverityEnum : 
		(
		mtDIAG_SEV_SUCCESS := 0, (*Success - no error*)
		mtDIAG_SEV_INFORMATIONAL := 1, (*Severity: Informational*)
		mtDIAG_SEV_WARNING := 2, (*Severity: Warning*)
		mtDIAG_SEV_ERROR := 3 (*Severity: Error*)
		);
	MTDiagType : 	STRUCT  (*Diagnostics type*)
		StatusID : MTDiagStatusIDType; (*StatusID diagnostics structure*)
		Internal : MTDiagInternalIDType; (*Internal error diagnostics structure*)
	END_STRUCT;
	MTDiagStatusIDType : 	STRUCT  (*Status ID type*)
		ID : DINT; (*Status ID*)
		Severity : MTDiagSeverityEnum; (*Severity of StatusID*)
		Code : UINT; (*Decoded error number*)
	END_STRUCT;
	MTDiagInternalIDType : 	STRUCT  (*Internal ID type*)
		ID : DINT; (*Status ID*)
		Severity : MTDiagSeverityEnum; (*Severity of StatusID*)
		Facility : DINT; (*Facility of Status ID (Origin Status)*)
		Code : UINT; (*Decoded error number*)
	END_STRUCT;
END_TYPE
