/* Automation Studio generated header file */
/* Do not edit ! */
/* MTDiag  */

#ifndef _MTDIAG_
#define _MTDIAG_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
/* Datatypes and datatypes of function blocks */
typedef enum MTDiagSeverityEnum
{	mtDIAG_SEV_SUCCESS = 0,
	mtDIAG_SEV_INFORMATIONAL = 1,
	mtDIAG_SEV_WARNING = 2,
	mtDIAG_SEV_ERROR = 3
} MTDiagSeverityEnum;

typedef struct MTDiagStatusIDType
{	signed long ID;
	enum MTDiagSeverityEnum Severity;
	unsigned short Code;
} MTDiagStatusIDType;

typedef struct MTDiagInternalIDType
{	signed long ID;
	enum MTDiagSeverityEnum Severity;
	signed long Facility;
	unsigned short Code;
} MTDiagInternalIDType;

typedef struct MTDiagType
{	struct MTDiagStatusIDType StatusID;
	struct MTDiagInternalIDType Internal;
} MTDiagType;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC plcbit MTDiagFillInfo(signed long* StatusID, signed long* InternalID, struct MTDiagType* Info);
_BUR_PUBLIC plcbit MTDiagFillStatusInfo(signed long* StatusID, struct MTDiagStatusIDType* Info);
_BUR_PUBLIC plcbit MTDiagFillInternalInfo(signed long* InternalID, struct MTDiagInternalIDType* Info);
_BUR_PUBLIC signed long MTDiagCreateStatusID(plcbyte Severity, signed long Facility, signed long Code, signed long* StatusID);
_BUR_PUBLIC signed long MTDiagGetCodeFromStatusID(signed long StatusID, signed long* Code);


#ifdef __cplusplus
};
#endif
#endif /* _MTDIAG_ */

