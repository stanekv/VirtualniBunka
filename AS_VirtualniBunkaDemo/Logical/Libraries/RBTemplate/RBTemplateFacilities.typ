
TYPE
	RBTemplateFacilitiesEnum : 
		( (*RoboTemplate facilities enumeration*)
		rbTEMPLATE_ROBOT_FAC := 230,
		rbTEMPLATE_TCP_COM_FAC := 250, (*TCP communication facility*)
		rbTEMPLATE_FD_LINK_FAC_BASIC := 270, (*File device link basic facility*)
		rbTEMPLATE_HW_DIAG_FAC_BASIC := 290, (*Hardware diagnostic facilities basic facility*)
		rbTEMPLATE_DG_DATA_FAC_BASIC := 300, (*Diagnostic data facilities basic facility*)
		rbTEMPLATE_TEACHING_FAC := 320, (*RBTemplateTeaching facility*)
		rbTEMPLATE_RESTAPI_FAC := 340, (*RBTemplateRestApi facility*)
		rbTEMPLATE_LICENSE_FAC := 350, (*RBTemplateRestApi facility*)
		rbTEMPLATE_LOGBOOK_FAC := 360 (*RBTemplateLogbook facility*)
		);
END_TYPE
