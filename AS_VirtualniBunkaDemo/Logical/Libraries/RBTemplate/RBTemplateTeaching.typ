(*Enumeration*)

TYPE
	RBTemplateTeachingStateEnum : 
		( (*FUB states*) (* *) (*#OMIT*)
		rbTEMPLATE_TEACHING_NOT_ENABLED := 0,
		rbTEMPLATE_TEACHING_PING := 10,
		rbTEMPLATE_TEACHING_IDENT_LOAD := 15,
		rbTEMPLATE_TEACHING_CONNECT := 20,
		rbTEMPLATE_TEACHING_WAIT := 30,
		rbTEMPLATE_TEACHING_CREATE_INSTR := 40,
		rbTEMPLATE_TEACHING_EDIT_INSTR := 50,
		rbTEMPLATE_TEACHING_INSERT_INSTR := 60,
		rbTEMPLATE_TEACHING_REMOVE_INSTR := 70,
		rbTEMPLATE_TEACHING_SAVE := 80,
		rbTEMPLATE_TEACHING_DISABLE := 90,
		rbTEMPLATE_TEACHING_ERROR := 100
		);
	RBTemplateTeachingErrorEnum : 
		( (*Error codes, 45250 - 45299*)
		rbTEMPLATE_TEACHING_ERR_IPADR := 45250, (*IP address for current mode (HW or simulation) is not filled*)
		rbTEMPLATE_TEACHING_ERR_PATH := 45251, (*Folder path for current mode (HW or simulation) is not filled*)
		rbTEMPLATE_TEACHING_ERR_LIST := 45252, (*Invalid pointer to list or zero list length specified*)
		rbTEMPLATE_TEACHING_ERR_PING := 45255, (*IcmpPing returned an error*)
		rbTEMPLATE_TEACHING_ERR_COMM := 45256, (*Communication (motion data reading) error*)
		rbTEMPLATE_TEACHING_ERR_SAVE := 45257, (*Error during mod saving*)
		rbTEMPLATE_TEACHING_ERR_PNAME := 45258, (*Point name is too long*)
		rbTEMPLATE_TEACHING_ERR_EDINDEX := 45260, (*Invalid Index: Attempt to edit instruction that has not been created yet*)
		rbTEMPLATE_TEACHING_ERR_FULL_LST := 45261, (*MoveInstructionList is full, new instruction cannot be learned*)
		rbTEMPLATE_TEACHING_ERR_RMINDEX := 45262, (*Invalid Index: Attempt to remove instruction that has not been created yet*)
		rbTEMPLATE_TEACHING_ERR_INSINDEX := 45263, (*Invalid Index: Attempt to insert instruction into a blank list or behind learned instructions*)
		rbTEMPLATE_TEACHING_ERR_FNAME := 45264, (*Empty file name*)
		rbTEMPLATE_TEACHING_ERR_STOPDATA := 45270 (*The argument "type" in stoppointdata may not be followtime in the instructions MoveJ, MoveAbsJ and MoveExtJ.*)
		);
END_TYPE

(*Structure*)

TYPE
	RBTemplateTeachingCmdType : 	STRUCT 
		CreateInstruction : BOOL; (*Create new instruction at the end of the instruction list.*)
		EditInstruction : BOOL; (*Edit instruction at index given by InstructionIndex in Parameters. Original instruction data are replaced by current motion data and Parameters.*)
		InsertInstruction : BOOL; (*Insert instruction after instruction given by InstructionIndex in Parameters. Remaining instructions are automatically shifted.*)
		RemoveInstruction : BOOL; (*Remove instruction at index given by InstructionIndex in Parameters. Remaining instructions are automatically shifted.*)
		CreateFile : BOOL; (*Create mod file from instruction list. Name and path are specified in Parameters.*)
		ClearList : BOOL; (*Clear content of instruction list*)
	END_STRUCT;
	RBTemplateTeachingParInstrType : 	STRUCT 
		Index : USINT; (*Index of instruction to edit / remove / insert after.*)
		Name : STRING[32]; (*Optional. In mod file showed in comment. If left blank point[index] name is generated. Only alphanumeric characters are allowed!*)
		Type : RBTemplateABBMoveInstrEnum; (*Type of move instruction (MoveJ, MoveL)*)
		PredefinedSpeed : RBTemplateABBPredefSpeedEnum; (*It will be used as speed parameter in instruction if specified (!= abbSPEED_NONE)*)
		Time : REAL; (*NOT IMPLEMENTED YET! Optional - This argument is used to specify the total time in seconds during which the robot moves. It is then substituted for the corresponding speed data.*)
		Velocity : REAL; (*NOT IMPLEMENTED YET! Optional - This argument is used to specify the velocity of the TCP in mm/s directly in the instruction. It is then substituted for the corresponding velocity specified in the speed data.*)
		Speed : RBTemplateABBSpeeddataType; (*The speed data that applies to movements. Speed data defines the velocity for the tool center point, the tool reorientation, and external axes.*)
		PredefinedZone : RBTemplateABBPredefZoneEnum; (*It will be used as zone parameter in instruction if specified (!= abbZONE_NONE)*)
		Zone : RBTemplateABBZonedataType; (*Optional - Zone data for the movement. Zone data describes the size of the generated corner path.*)
		PredefinedStoppoint : RBTemplateABBPredefStoppointEnum; (*Optional - This argument is used to specify the convergence criteria for the position of the robot's TCP in the stop point. The stop point data substitutes the zone specified in the Zone parameter.*)
		InPosition : RBTemplateABBStoppointdataType; (*NOT IMPLEMENTED YET! Optional - This argument is used to specify the convergence criteria for the position of the robot's TCP in the stop point. The stop point data substitutes the zone specified in the Zone parameter.*)
		TotalLoad : RBTemplateABBLoaddataType; (*NOT IMPLEMENTED YET! Optional - The \TLoad argument describes the total load used in the movement. The total load is the tool load together with the payload that the tool is carrying. If the \TLoad argument is used, then the loaddata in the current tooldata is not considered. If the \TLoad argument is set to load0, then the \TLoad argument is not considered and the loaddata in the current tooldata is used instead. To be able to use the \TLoad argument it is necessary to set the value of the system parameter ModalPayLoadMode to 0. If ModalPayLoadMode is set to 0, it is no longer possible to use the instruction GripLoad. The total load can be identified with the service routine LoadIdentify. If the system parameter ModalPayLoadMode is set to 0, the operator has the possibility to copy the loaddata from the tool to an existing or new loaddata persistent variable when running the service routine. It is possible to test run the program without any payload by using a digital input signal connected to the system input SimMode (Simulated Mode). If the digital input signal is set to 1, the loaddata in the optional argument \TLoad is not considered, and the loaddata in the current tooldata is used instead.*)
	END_STRUCT;
	RBTemplateTeachingParType : 	STRUCT 
		Path : {REDUND_UNREPLICABLE} STRING[rbtROBOT_CTRL_PATH_STRING_LEN]; (*Pointer to a path to a module file*)
		PathSim : {REDUND_UNREPLICABLE} STRING[rbtROBOT_CTRL_PATH_STRING_LEN]; (*Pointer to a path to a module file while in simulation*)
		FileName : {REDUND_UNREPLICABLE} STRING[36]; (*Module file name*)
		RobotIndex : USINT; (*Robot selector*)
		KeepPositionOnEdit : BOOL; (*Keep target position of learned instruction. Edit command changes other parameters (speed, zone, ...).*)
		pInstructionList : REFERENCE TO RBTemplateABBMoveInstrListType; (*Pointer to instruction list (array)*)
		InstructionListLen : UDINT; (*Length of move instruction list*)
		Instruction : RBTemplateTeachingParInstrType; (*Specific instruction parameters*)
	END_STRUCT;
	RBTemplateTeachingInfoType : 	STRUCT 
		ARSimUsed : BOOL; (*PLC is in simulation (TRUE)*)
		Controller : RBTemplateRobotCtrlConInfoType;
		InstructionsCount : UDINT; (*Number of learned instructions*)
		Diag : MTDiagType;
	END_STRUCT;
	RBTemplateTeachingIntCmdType : 	STRUCT  (*Internal commands*) (* *) (*#OMIT*)
		BypassPing : BOOL; (* *) (* *) (*#OMIT*)
		PingRobot : BOOL;
		ClearList : BOOL;
		ShiftListDownwards : BOOL; (*Used in INSERT_INSTR state*)
		SystemInfoReceived : BOOL; (*Robot info data has been successfully received*)
		RequestData : UDINT; (*Request data type*)
		MotionDataIndex : USINT;
	END_STRUCT;
	RBTemplateTeachingIntStatusType : 	STRUCT  (*Internal status*) (* *) (*#OMIT*)
		Ping : BOOL; (*Robot responds to ping (TRUE)*)
		Connected : BOOL; (*Communication is active (TRUE)*)
		CommandEdge : RBTemplateTeachingCmdType; (*Holds values of input Commands from previous cycle*)
		MotionDataValid : BOOL; (*Motion data for instruction creation / edit are valid*)
		ErrorState : RBTemplateTeachingStateEnum; (*State in which last error has occured*)
		ErrorResetOld : BOOL; (*Holds ErrorReset input value from previous cycle*)
	END_STRUCT;
	RBTemplateTeachingIntDataType : 	STRUCT  (*Internal data*) (* *) (*#OMIT*)
		RequestData : RBTemplateSystemReqDataType;
		SystemInfo : RBTemplateSystemInfoType;
		MotionData : RBTemplateRobotMotionDataType;
		InstructionParameters : RBTemplateTeachingParInstrType; (*Instruction parameters internal copy*)
		Instruction : RBTemplateABBMoveInstrListType; (*Temporary storage for created instruction*)
		ListInstructionsCount : UDINT; (*Total number of items in instruction list. Calculated from list size and list type size*)
		LearnedInstructionsCount : UDINT; (*Index of the last instruction created in MoveInstructionList*)
		RobotCount : USINT;
		RobotIpAdr : STRING[15]; (*Internal copy of active robot IP address (either pRobotIpAdr or pSimRobotIpAdr)*)
	END_STRUCT;
	RBTemplateTeachingIntFubType : 	STRUCT  (* *) (* *) (*#OMIT*)
		TON_0 : TON;
		IcmpPing_0 : IcmpPing;
		RBTemplateCommTcp_0 : RBTemplateCommTcp;
		RBTemplateModGen_0 : RBTemplateModGen;
		RBTemplateIdentHandler_0 : RBTemplateIdentHandler;
	END_STRUCT;
	RBTemplateTeachingInternalType : 	STRUCT  (* *) (* *) (*#OMIT*)
		State : RBTemplateTeachingStateEnum; (*Next (current) state*)
		Commands : RBTemplateTeachingIntCmdType;
		Status : RBTemplateTeachingIntStatusType;
		Data : RBTemplateTeachingIntDataType;
		FUB : RBTemplateTeachingIntFubType;
	END_STRUCT;
END_TYPE
