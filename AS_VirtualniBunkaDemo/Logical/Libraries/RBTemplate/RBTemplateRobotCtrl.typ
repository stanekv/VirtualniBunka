
TYPE
	RBTemplateRobotCtrlProcExecEnum : 
		( (*Old*)
		rbtROBOT_CTRL_WAIT_FOR_CMD := 0,
		rbtROBOT_CTRL_WAIT_LOAD_CMD := 10,
		rbtROBOT_CTRL_WAIT_LOAD_STAT := 20,
		rbtROBOT_CTRL_WAIT_START_CMD := 30,
		rbtROBOT_CTRL_WAIT_START_STAT := 40,
		rbtROBOT_CTRL_PROC_EXECUTING := 50
		);
	RBTemplateRobotCtrlCommStateEnum : 
		(
		rbtABB_ROB_CTRL_COM_NOT_EN := 0,
		rbtABB_ROB_CTRL_COM_PING := 10,
		rbtABB_ROB_CTRL_COM_IDENT_LOAD := 15,
		rbtABB_ROB_CTRL_COM_IDENT_SAVE := 16,
		rbtABB_ROB_CTRL_COM_HANDSHAKE := 20,
		rbtABB_ROB_CTRL_COM_START := 30,
		rbtABB_ROB_CTRL_COM_WAIT := 40,
		rbtABB_ROB_CTRL_COM_DISABLE := 50,
		rbtABB_ROB_CTRL_COM_ERROR := 60
		);
	RBTemplateRobotCtrlRobAutExeEnum : 
		(
		rbtROBOT_CTRL_STATE_ROB_AE_WAIT := 0,
		rbtROBOT_CTRL_STATE_ROB_AE_TRNSF := 1,
		rbtROBOT_CTRL_STATE_ROB_AE_LOAD := 2,
		rbtROBOT_CTRL_STATE_ROB_AE_START := 3
		);
	RBTemplateRobotCtrlRobStateEnum : 
		(
		rbtROBOT_CTRL_STATE_ROB_INIT := 0, (*Init*)
		rbtROBOT_CTRL_STATE_ROB_WAIT := 10, (*Wait for command*)
		rbtROBOT_CTRL_STATE_ROB_LOAD := 20, (*Load module*)
		rbtROBOT_CTRL_STATE_ROB_START := 30, (*Start procedure*)
		rbtROBOT_CTRL_STATE_ROB_RUN := 40, (*Procedure is running*)
		rbtROBOT_CTRL_STATE_ROB_TRANSFER := 50, (*Transfer file to controller*)
		rbtROBOT_CTRL_STATE_ROB_ERROR := 60 (*Wait for ErrorReset*)
		);
	RBTemplateRobotCtrlRobErrorEnum : 
		( (*45135 - 45149*)
		rbtROBOT_CTRL_ERR_ROB_NONE := 0, (*No error*)
		rbtROBOT_CTRL_ERR_ROB_CMD_TOUT := 45135, (*Timer for command to be completed has elapsed*)
		rbtROBOT_CTRL_ERR_ROB_MOD_NAME := 45136, (*Module name is empty*)
		rbtROBOT_CTRL_ERR_ROB_MOD_LOAD := 45137, (*Module has not been loaded, see RapidError*)
		rbtROBOT_CTRL_ERR_ROB_PROC_NAME := 45138, (*Procedure name is empty*)
		rbtROBOT_CTRL_ERR_ROB_PROC_START := 45139, (*Procedure has not started, see RapidError*)
		rbtROBOT_CTRL_ERR_ROB_PROC_EXEC := 45140, (*Procedure execution error, see RapidError*)
		rbtROBOT_CTRL_ERR_ROB_FILE_NAME := 45141, (*File name to transfer is empty*)
		rbtROBOT_CTRL_ERR_ROB_TRANSFER := 45142, (*File has not been transfered, see Internal info*)
		rbtROBOT_CTRL_ERR_ROB_TRANS_TOUT := 45143, (*Time for file transfer has elapsed, connection to target is not possible*)
		rbtROBOT_CTRL_ERR_ROB_MOT_SUP := 45144 (*Motor supervision triggered*)
		);
	RBTemplateRobCtrlErrorEnum : 
		( (*45100 - 45134*)
		rbtROBOT_CTRL_ERR_NO_IP_ADDRESS := 45100,
		rbtROBOT_CTRL_ERR_PING := 45101, (*Could not check ping*)
		rbtROBOT_CTRL_ERR_HANDSHAKE := 45102, (*Handshake failed*)
		rbtROBOT_CTRL_ERR_COMMUNICATION := 45103, (*Communication error, check if communication tasks are running in robot*)
		rbtROBOT_CTRL_ERR_VERSION := 45104, (*Robot and PLC ROBOTemplate versions do not match*)
		rbtROBOT_CTRL_ERR_PLC_LICENSE := 45105, (*RBTemplate license is not valid or could not be checked*)
		rbtROBOT_CTRL_ERR_ROB_LICENSE := 45106, (*Robot license is invalid*)
		rbtROBOT_CTRL_INFO_MANUAL_MODE := 45107, (*Robot is in manual mode*)
		rbtROBOT_CTRL_ERR_RESERVE1 := 45108, (*Reserved for future use*)
		rbtROBOT_CTRL_ERR_RESERVE2 := 45109, (*Reserved for future use*)
		rbtROBOT_CTRL_ERR_EMSTOP := 45110, (*ABB Robot: Emergency Stop*)
		rbtROBOT_CTRL_ERR_EXEERR := 45111, (*ABB Robot: Execution Error*)
		rbtROBOT_CTRL_ERR_RUNCHAIN := 45112, (*ABB Robot: Runchain*)
		rbtROBOT_CTRL_ERR_SAFEMOVE := 45113, (*ABB Robot: Safe Move Violation*)
		rbtROBOT_CTRL_ERR_RESERVE3 := 45114, (*Reserved for future use*)
		rbtROBOT_CTRL_ERR_UNEXP_PRG_STOP := 45115, (*Unexpected program stop*)
		rbtROBOT_CTRL_ERR_CMD_UNFINISHED := 45116, (*Command does not finished before timer has elapsed.*)
		rbtROBOT_CTRL_ERR_INV_SRC_PATH := 45117, (*Invalid path to .mod files source directory*)
		rbtROBOT_CTRL_ERR_UPLOAD_MOD := 45118, (*Module file upload error*)
		rbtROBOT_CTRL_ERR_EMP_ROB_LIC := 45119, (*It is not allowed to upload empty license key *)
		rbtROBOT_CTRL_ERR_UPL_ROB_LIC := 45120, (*Upload license key to robot failed, check if ROBOTemplate directory in controller $home exists*)
		rbtROBOT_CTRL_ERR_SAVE_FILE_UNS := 45121, (*Unspecified save file error, check internal diag structure*)
		rbtROBOT_CTRL_ERR_SAVE_FILE_BUF := 45122 (*Buffer is not long enough, file content could not be read and saved. Increase rbtROBOT_CTLR_VAR_FILE_BUFFER, recompile library and try again*)
		);
	RBTemplateRobotCtrlStatusEnum : 
		( (*Success status codes*)
		rbtROBOT_CTRL_STAT_MOT_ON := 45100,
		rbtROBOT_CTRL_STAT_MOT_OFF := 45101,
		rbtROBOT_CTRL_STAT_START := 45102,
		rbtROBOT_CTRL_STAT_STOP := 45103,
		rbtROBOT_CTRL_STAT_MOD_TRANSF := 45104,
		rbtROBOT_CTRL_STAT_MOD_LOAD := 45105,
		rbtROBOT_CTRL_STAT_PROC_START := 45106,
		rbtROBOT_CTRL_STAT_AUTO_EXEC := 45107
		);
	RBTemplateRobotCtrlStateEnum : 
		(
		rbtROBOT_CTRL_NOT_ENABLED := 0, (*Function block is not enabled*)
		rbtROBOT_CTRL_CONNECT := 10,
		rbtROBOT_CTRL_CHK_ROB_LICENSE := 12, (*Check if robot license is valid*)
		rbtROBOT_CTRL_UPL_ROB_LICENSE := 13, (*Upload license key to robot*)
		rbtROBOT_CTRL_CHECK_LICENSE := 15,
		rbtROBOT_CTRL_CHECK_VERSION := 20,
		rbtROBOT_CTRL_ROB_MODE := 25,
		rbtROBOT_CTRL_GET_ROB_COUNT := 26,
		rbtROBOT_CTRL_READY := 30,
		rbtROBOT_CTRL_ACTIVE := 40,
		rbtROBOT_CTRL_TURNING_MOTORS_ON := 50,
		rbtROBOT_CTRL_TURNING_MOTORS_OFF := 60,
		rbtROBOT_CTRL_PROG_RUNNING := 70,
		rbtROBOT_CTRL_TRAP_ROUTINE := 80, (*Trap routine state, not used yet*)
		rbtROBOT_CTRL_WAIT_FOR_PRG_START := 90, (*Wait for execution status*)
		rbtROBOT_CTRL_WAIT_FOR_PRG_STOP := 100, (*Wait for stop *)
		rbtROBOT_CTRL_UPLOAD_MODULE := 120, (*Upload module file to robot controller*)
		rbtROBOT_CTRL_SAVE_FILE := 140, (*Save file from robot controller to PLC*)
		rbtROBOT_CTRL_CHECK_ROB_ERR := 200,
		rbtROBOT_CTRL_RESET_ROB_EMSTOP := 210, (*Reset robot emergency stop*)
		rbtROBOT_CTRL_RESET_ROB_EXEERR := 220, (*Reset robot execution error*)
		rbtROBOT_CTRL_ERROR := 300 (*Wait for ErrorReset*)
		);
END_TYPE

(*------------------------------ FUB Input data structure ------------------------------*)

TYPE
	RBTemplateRobotCtrlInputCmdType : 	STRUCT 
		MotorsOff : BOOL; (*Stop motors*)
		MotorsOn : BOOL; (*Start motors*)
		Start : BOOL; (*Start program*)
		StartAtMain : BOOL; (*Start program at main*)
		Stop : BOOL; (*Stop program*)
		StopAtEndInstr : BOOL; (*Stop process after current instruction is completed*)
		SrcFile : STRING[rbtROBOT_CTRL_PATH_STRING_LEN]; (*File to save from robot, root directory is $home, it is possible to include folders, i. e. "abc/xyz/file.txt" will save file located at "$home/abc/xyz/file.txt"*)
		DstPath : STRING[rbtROBOT_CTRL_PATH_STRING_LEN]; (*Destination directory*)
		DstPathSim : STRING[rbtROBOT_CTRL_PATH_STRING_LEN]; (*Destination directory*)
		SaveFileFromController : BOOL; (*Download file from robot*)
		RobotLicenseKey : STRING[44]; (*Robot license key*)
		ApplyRobotLicense : BOOL; (*Upload license key to robot*)
	END_STRUCT;
	RBTemplateRobotCtrlInputRobType : 	STRUCT 
		ErrorReset : BOOL; (*Error reset*)
		LimitSpeed : BOOL; (*Limit speed on/off*)
		AutoExecute : BOOL; (*Do Transfer Module -> Load Module -> Start Procedure*)
		ModuleSrcPath : STRING[rbtROBOT_CTRL_PATH_STRING_LEN]; (*Directory with source .mod files (for upload PLC -> robot)*)
		ModuleSrcPathSim : STRING[rbtROBOT_CTRL_PATH_STRING_LEN]; (*Directory with source .mod files (for upload PLC -> robot)*)
		ModuleName : STRING[rbtROBOT_CTRL_MODULE_STRING_LEN]; (*Name of module file to load (including .mod extension)*)
		TransferModuleToController : BOOL; (*Upload module file to controller from source given in parameters*)
		LoadModule : BOOL; (*Load module to robot module buffer*)
		ProcedureName : STRING[rbtROBOT_CTRL_PROC_STRING_LEN]; (*Name of procedure to start*)
		StartProcedure : BOOL; (*Execute procedure*)
		SpeedOverride : USINT; (*Speed override for all robot moves, value is multiplied with speed override set on FP.*)
		RelToolCords : RBTemplateRelToolType; (*RelToolCords*)
		RelTool : BOOL; (*RelTool (Relative Tool) is used to add a displacement and/or a rotation, expressed in the active tool coordinate system, to a robot position.*)
	END_STRUCT;
END_TYPE

(*------------------------------ FUB Output data structure ------------------------------							*)

TYPE
	RBTemplateRobotCtrlRobInfoType : 	STRUCT  (*Information about robot*)
		Active : BOOL; (*Robot is present and ready*)
		Error : BOOL; (*Robot is in error*)
		ErrorCode : RBTemplateRobotCtrlRobErrorEnum; (*Error number*)
		LimitSpeed : BOOL; (*Limit speed is enabled (TRUE)*)
		MechUnitNotMove : BOOL; (*Robot is not moving (TRUE)*)
		MotSupTriggered : BOOL; (*Motor supervision has been triggered (TRUE)*)
		TransferDone : BOOL; (*TransferFileToController command has finished*)
		LoadModuleDone : BOOL; (*LoadModule command finished*)
		ProcExecuting : BOOL; (*Robot is executing procedure*)
		ProcDone : BOOL; (*Procedure was finished*)
		Tool : ARRAY[0..31]OF STRING[rbtROBOT_CTRL_MAX_TOOL_NAMES]; (*Defined tools with tRT prefix*)
		Workobject : ARRAY[0..31]OF STRING[rbtROBOT_CTRL_MAX_WOBJ_NAMES]; (*Defined workobjects with wobjRT prefix*)
	END_STRUCT;
	RBTemplateRobotCtrlStatusType : 	STRUCT  (*Information about controller*)
		Ping : BOOL; (*Robot responds to ping*)
		Connected : BOOL; (*Communication with controller (robot) is running*)
		OperatorModeAutoChange : BOOL; (*Automatic mode needs to be acknowledged*)
		AutomatOn : BOOL; (*Controller is in automatic mode*)
		MotorsOn : BOOL; (*Motors are enabled*)
		MotorsOff : BOOL; (*Motors are disabled*)
		ExecutingMotionTask : BOOL; (*Any of the motion tasks is running*)
		EmStop : BOOL; (*Emergency stop triggered*)
		ExeError : BOOL; (*Execution error has occured*)
		RunChainOk : BOOL; (*All runchains are closed*)
		ESChainOk : BOOL; (*Emergency stop*)
		ASChainOk : BOOL; (*Auto stop*)
		GSChainOk : BOOL; (*General stop*)
		SSChainOk : BOOL; (*Superior stop*)
		SafeMoveViolated : BOOL; (*Safe Move violation*)
		WorldZone : ARRAY[0..19]OF BOOL; (*World zones, TRUE if robot is in zone*)
		SaveFileFromControllerDone : BOOL; (*Download file from robot finished*)
		ApplyRobotLicenseDone : BOOL; (*ApplyRobotLicense command finished*)
	END_STRUCT;
	RBTemplateRobotCtrlConInfoType : 	STRUCT  (*Controller info*)
		SerialNumber : STRING[31]; (*Serial number of first robot*)
		SwVersion : STRING[31];
		SwVersionName : STRING[31];
		ControllerID : STRING[31];
		WAN_IPAddress : STRING[31];
		SystemLanguage : STRING[3];
		SystemName : STRING[31];
		RoboTemplateVersion : STRING[11]; (*Version of RBTemplate in robot, format XX.YY.ZZ, XX and YY must match version of RBTemplate in PLC*)
		RobotCount : USINT; (*Number of robots connected to controller*)
		RobotType : ARRAY[0..3]OF STRING[31]; (*Array is also used to determine number of connected robots*)
	END_STRUCT;
	RBTemplateRobotCtrlInfoType : 	STRUCT 
		ARSimUsed : BOOL; (*PLC is in simulation (TRUE)*)
		CmdTime : UDINT; (*Processing time of last controller command*)
		Controller : RBTemplateRobotCtrlConInfoType; (*Controller and robot(s) info*)
		Diag : MTDiagType; (*Error diagnostic structure*)
	END_STRUCT;
END_TYPE

(*------------------------------ Internal data structure ------------------------------*)
(*Common structures for controller / all robots*)

TYPE
	RBTemplateRobotCtrlIntEdgeType : 	STRUCT  (*Controller commands edge*)
		MotorsOff : BOOL; (*Stop motors*)
		MotorsOn : BOOL; (*Start motors*)
		Start : BOOL; (*Start program*)
		StartAtMain : BOOL; (*Start program at main*)
		Stop : BOOL; (*Stop program*)
		StopAtEndInstr : BOOL; (*Stop process after current instruction is completed*)
		ReqTools : BOOL; (*Not implemented. Read tRT_ tools defined in robot*)
		ReqWorkobjects : BOOL; (*Not implemented. Read wobjRT_ workobjects defined in robot*)
		SaveFileFromController : BOOL; (*SaveFileFromController command edge *)
	END_STRUCT;
	RBTemplateRobotCtrlIntCommType : 	STRUCT 
		Active : BOOL; (*Communication loop is initialized, communication is running*)
		ControllerIpAdr : STRING[15]; (*Robot IP address*)
		BypassPing : BOOL; (*Do not wait for ping response*)
		Enable : BOOL; (*Enable communication loop*)
		Error : BOOL; (*Communication error*)
		ErrorCode : DINT; (*Error code of communication loop*)
		ErrorReset : BOOL; (*Error reset command, handled by control loop*)
		ErrorResetActive : BOOL; (*Error reset command, handled by control loop*)
		ErrorResetOld : BOOL; (*Error reset command, handled by control loop*)
		FirstCycleDone : BOOL; (*First cmd data sent and first status data received*)
		IdentSaveDone : BOOL; (*Communication idents has been saved to data object*)
		MotionDataIndex : UDINT; (*Index for Motion data reading*)
		Offset : USINT; (*Used when reading tool or wobj names from robot*)
		Ping : BOOL; (*Robot responds (TRUE) to ping*)
		PingRobot : BOOL; (*Ping robot command, handled by control loop*)
		RequestData : UDINT; (*Request data type*)
		State : RBTemplateRobotCtrlCommStateEnum; (*Communication loop state*)
		SystemInfoReceived : BOOL; (*Robot info data has been successfully received*)
		Timeout : UDINT; (*Maximum communication delay before entering error state (ms)*)
		ToolWobjIndex : UDINT; (*Index for Tool/Wobj reading*)
		Cycle : ARRAY[0..29]OF UDINT;
		CycleIndex : USINT;
		CycleAvg : UDINT; (*Communication cycle (request-response) average time*) (* *) (*#OMIT*)
		CycleMin : UDINT; (*Communication cycle (request-response) minimal time*) (* *) (*#OMIT*)
		CycleMax : UDINT; (*Communication cycle (request-response) maximal time*) (* *) (*#OMIT*)
	END_STRUCT;
	RBTemplateRobotCtrlIntDataType : 	STRUCT  (*CommunicationData*)
		SystemCommand : RBTemplateSystemCmdType;
		SystemStatus : RBTemplateSystemStatusType;
		RequestData : RBTemplateSystemReqDataType;
		RobotMotionData : RBTemplateRobotMotionDataType;
		SystemInfo : RBTemplateSystemInfoType;
		Handshake : RBTemplateHandshakeDataType;
		StringData : STRING[32];
		UserDataToController : RBTemplateUserDataType;
		FileContentBuffer : STRING[rbtROBOT_CTLR_VAR_FILE_BUFFER];
		UserDataFromController : RBTemplateUserDataType;
	END_STRUCT;
	RBTemplateRobotCtrlIntFlagsType : 	STRUCT 
		ApplyRobotLicenseOld : BOOL; (*ApplyRobotLicense command edge *)
		ControllerCmdEdge : RBTemplateRobotCtrlIntEdgeType; (*Input controller Cmd edge*)
		Error : BOOL; (*Error occured*)
		ErrorResetActive : BOOL; (*Error reset is in progress*)
		ErrorResetOld : BOOL; (*ErrorReset command edge*)
		ErrorRobot : BOOL; (*Any robot is in error*)
		ProfiSafeOpAck : BOOL;
	END_STRUCT;
	RBTemplateRobotCtrlIntFUBType : 	STRUCT 
		RBCommCmdStat_0 : RBTemplateCommTcp;
		RBCommReqData_0 : RBTemplateCommTcp; (*Request data command structure*)
		RBCommUserData_0 : RBTemplateCommTcp; (*User data structure*)
		IcmpPing_0 : IcmpPing;
		TON_0 : TON; (*Communication timer*)
		TON_1 : TON; (*Timer for commands*)
		TON_2 : TON; (*ProfiSafeOpAck timer *)
		RBTemplateRestApi_0 : RBTemplateRestApi;
		RBTemplateLicense_0 : RBTemplateLicense;
		RBTemplateLogbook_0 : RBTemplateLogbook;
		RBTemplateIdentHandler_0 : RBTemplateIdentHandler;
	END_STRUCT;
	RBTemplateRobotCtrlIntStateType : 	STRUCT 
		LastBeforeError : RBTemplateRobotCtrlStateEnum;
		Main : RBTemplateRobotCtrlStateEnum; (*Main FUB state*)
		DownloadFileNextState : RBTemplateRobotCtrlStateEnum; (*State after file save*)
	END_STRUCT;
	RBTemplateRobotCtrlIntType : 	STRUCT 
		Comm : RBTemplateRobotCtrlIntCommType;
		Data : RBTemplateRobotCtrlIntDataType;
		Flags : RBTemplateRobotCtrlIntFlagsType;
		FUB : RBTemplateRobotCtrlIntFUBType;
		Robot : ARRAY[0..3]OF RBTemplateRobotCtrlRobType;
		RobotCount : USINT; (*Number of robots*)
		RobotIndex : USINT;
		State : RBTemplateRobotCtrlIntStateType;
	END_STRUCT;
END_TYPE

(*Structures for each robot*)

TYPE
	RBTemplateRobotCtrlRobEdgeType : 	STRUCT 
		AutoExecute : BOOL;
		ErrorReset : BOOL;
		LoadModule : BOOL;
		StartProcedure : BOOL;
		TransferModuleToController : BOOL;
	END_STRUCT;
	RBTemplateRobotCtrlRobFlagsType : 	STRUCT 
		AutoExecuteDone : BOOL; (*AutoExecute command finished*)
		CheckErrors : BOOL; (*Check robot errors (emstop, exeerr, runchain...)*)
		Edge : RBTemplateRobotCtrlRobEdgeType; (*Input robot Cmd edge*)
		Enable : BOOL; (*Control for this robot is enabled*)
		Error : BOOL;
		ErrorReset : BOOL; (*Check robot errors (emstop, exeerr, runchain...)*)
		LimitSpeed : BOOL;
		ProcDone : BOOL; (*Proc finished*)
		TransferDone : BOOL; (*TransferModuleToController finished*)
	END_STRUCT;
	RBTemplateRobotCtrlRobType : 	STRUCT 
		Cmd : RBTemplateRobotCmdType; (*Communication command data*)
		DataBuffer : STRING[rbtROBOT_CTLR_VAR_FILE_BUFFER]; (*Data buffer for file upload*)
		ErrorCode : DINT; (*Error code*)
		Flags : RBTemplateRobotCtrlRobFlagsType;
		RBTemplateRestApi_R : RBTemplateRestApi;
		State : RBTemplateRobotCtrlRobStateEnum;
		StateAutoExec : RBTemplateRobotCtrlRobAutExeEnum; (*Not implemented. AutoExecute command state (0: waiting, 1: transfer, 2: load, 3: start)*)
		Status : RBTemplateRobotStatusType; (*Communication status data*)
		TON_R : TON; (*Timer for robot*)
	END_STRUCT;
END_TYPE

(*------------------------------ RBTemplateRobotCtrl Communication ------------------------------*)
