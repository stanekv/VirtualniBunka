/* Automation Studio generated header file */
/* Do not edit ! */
/* RBTemplate 1.02.4 */

#ifndef _RBTEMPLATE_
#define _RBTEMPLATE_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _RBTemplate_VERSION
#define _RBTemplate_VERSION 1.02.4
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG3
		#include "FileIO.h"
		#include "AsTCP.h"
		#include "AsHttp.h"
		#include "AsICMP.h"
		#include "AsGuard.h"
		#include "ArEventLog.h"
		#include "AsBrMath.h"
		#include "AsBrStr.h"
		#include "AsIODiag.h"
		#include "FDLink.h"
		#include "MTDiag.h"
		#include "MTHash.h"
#endif
#ifdef _SG4
		#include "FileIO.h"
		#include "AsTCP.h"
		#include "AsHttp.h"
		#include "AsICMP.h"
		#include "AsGuard.h"
		#include "ArEventLog.h"
		#include "AsBrMath.h"
		#include "AsBrStr.h"
		#include "AsIODiag.h"
		#include "FDLink.h"
		#include "MTDiag.h"
		#include "MTHash.h"
#endif
#ifdef _SGC
		#include "FileIO.h"
		#include "AsTCP.h"
		#include "AsHttp.h"
		#include "AsICMP.h"
		#include "AsGuard.h"
		#include "ArEventLog.h"
		#include "AsBrMath.h"
		#include "AsBrStr.h"
		#include "AsIODiag.h"
		#include "FDLink.h"
		#include "MTDiag.h"
		#include "MTHash.h"
#endif


/* Constants */
#ifdef _REPLACE_CONST
 #define rbtIDENT_HANDLER_MEM_TYPE 3U
 #define rbtIDENT_HANDLER_FILE_NAME "rbt_ident"
 #define rbTEMPLATE_LOGBOOK_VAR_DATA_LEN 255U
 #define rbTEMPLATE_LOGBOOK_VAR_NAME "robotlog"
 #define rbTEMPLATE_LIC_VAR_INFO_ARR_SIZE 30U
 #define rbTEMPLATE_LIC_VAR_DONG_ARR_SIZE 5U
 #define rbTEMPLATE_LIC_VAR_STR1_SIZE 30U
 #define rbTEMPLATE_RESTAPI_ENC_URL_BUF 200U
 #define rbTEMPLATE_RESTAPI_QOP_BUF 20U
 #define rbTEMPLATE_RESTAPI_OPAQUE_BUF 20U
 #define rbTEMPLATE_RESTAPI_CNONCE_BUF 100U
 #define rbTEMPLATE_RESTAPI_NC_BUF 10U
 #define rbTEMPLATE_RESTAPI_NONCE_BUF 100U
 #define rbTEMPLATE_RESTAPI_REALM_BUF 30U
 #define rbTEMPLATE_RESTAPI_DIGEST_A_BUF 32U
 #define rbTEMPLATE_RESTAPI_HASH_IN_BUF 1000U
 #define rbTEMPLATE_RESTAPI_PARSE_VAL_BUF 60U
 #define rbTEMPLATE_RESTAPI_PARSE_KEY_BUF 20U
 #define rbTEMPLATE_RESTAPI_RES_DATA_BUF 8000U
 #define rbTEMPLATE_RESTAPI_RES_RAW_BUF 2000U
 #define rbTEMPLATE_RESTAPI_REQ_DATA_BUF 8000U
 #define rbTEMPLATE_RESTAPI_REQ_RAW_BUF 3000U
 #define rbTEMPLATE_RESTAPI_REQ_URI_BUF 200U
 #define rbTEMPLATE_TEACHING_PING_DELAY 5.0f
 #define rbTEMPLATE_TEACHING_TIMEOUT 5000.0f
 #define rbTEMPLATE_TEACHING_MDAT_P_PLC 12020U
 #define rbTEMPLATE_TEACHING_MDAT_P_ROB 3510U
 #define rbtROBOT_CTRL_STR_EV_AUTO_EXEC ": auto execute triggered"
 #define rbtROBOT_CTRL_STR_EV_START_PROC ": start procedure "
 #define rbtROBOT_CTRL_STR_EV_LOAD_MOD ": load module "
 #define rbtROBOT_CTRL_STR_EV_TRANSF_MOD ": transfer module "
 #define rbtROBOT_CTRL_STR_EV_ROBOT_PREF "RBTemplateRobotCtrl robot "
 #define rbtROBOT_CTLR_VAR_FILE_BUFFER 64000U
 #define rbtROBOT_CTRL_PATH_STRING_LEN 64U
 #define rbtROBOT_CTRL_PROC_STRING_LEN 32U
 #define rbtROBOT_CTRL_MODULE_STRING_LEN 36U
 #define rbtROBOT_CTRL_MAX_WOBJ_NAMES 20U
 #define rbtROBOT_CTRL_MAX_TOOL_NAMES 20U
 #define rbtROBOT_CTRL_GET_WOBJ_NAMES 255U
 #define rbtROBOT_CTRL_GET_TOOL_NAMES 255U
 #define rbtROBOT_CTRL_GET_ROBOT_INFO 10U
 #define rbtROBOT_CTRL_GET_MOTION_DATA 0U
 #define rbtROBOT_CTRL_PLC_PORT_USER_DATA 12030U
 #define rbtROBOT_CTRL_ROB_PORT_USER_DATA 3520U
 #define rbtROBOT_CTRL_PLC_PORT_MOT_INFO 12020U
 #define rbtROBOT_CTRL_ROB_PORT_MOT_INFO 3510U
 #define rbtROBOT_CTRL_PLC_PORT_CMD_STAT 12010U
 #define rbtROBOT_CTRL_ROB_PORT_CMD_STAT 3500U
 #define rbtROBOT_CTRL_MAX_TIME_DIFF 3600000000U
 #define rbtROBOT_CTRL_TRANSFER_TIMEOUT 1000U
 #define rbtROBOT_CTRL_PROFISAFE_ACK_TIME 1000U
 #define rbtROBOT_CTRL_CMD_DONE_TIMEOUT 5000U
 #define rbtROBOT_CTRL_INFO_DATA_TIMEOUT 1000U
 #define rbtROBOT_CTRL_COMM_TIMEOUT 20000U
 #define rbtROBOT_CTRL_PING_REPEAT_DELAY 2000U
 #define rbtROBOT_CTRL_ICMP_PING_TIMEOUT 100U
 #define PATH_SIM "C:\\UserDisk\\RobotPrograms\\"
 #define PATH "F:\\UserDisk\\RobotPrograms\\"
 #define rbTEMPLATE_ASCII_SEMICOLON 59U
 #define rbTEMPLATE_ASCII_NEWLINE 10U
 #define rbTEMPLATE_MAX_WOBJS_MIN_1 99U
 #define rbTEMPLATE_MAX_WOBJS 100U
 #define rbTEMPLATE_MAX_TOOLS_MIN_1 99U
 #define rbTEMPLATE_MAX_TOOLS 100U
 #define rbTEMPLATE_EMPTY_STRING_32 ""
 #define rbTEMPLATE_EMPTY_STRING_255 ""
#else
 _GLOBAL_CONST unsigned char rbtIDENT_HANDLER_MEM_TYPE;
 _GLOBAL_CONST plcstring rbtIDENT_HANDLER_FILE_NAME[11];
 _GLOBAL_CONST unsigned short rbTEMPLATE_LOGBOOK_VAR_DATA_LEN;
 _GLOBAL_CONST plcstring rbTEMPLATE_LOGBOOK_VAR_NAME[11];
 _GLOBAL_CONST unsigned char rbTEMPLATE_LIC_VAR_INFO_ARR_SIZE;
 _GLOBAL_CONST unsigned char rbTEMPLATE_LIC_VAR_DONG_ARR_SIZE;
 _GLOBAL_CONST unsigned char rbTEMPLATE_LIC_VAR_STR1_SIZE;
 _GLOBAL_CONST unsigned char rbTEMPLATE_RESTAPI_ENC_URL_BUF;
 _GLOBAL_CONST unsigned char rbTEMPLATE_RESTAPI_QOP_BUF;
 _GLOBAL_CONST unsigned char rbTEMPLATE_RESTAPI_OPAQUE_BUF;
 _GLOBAL_CONST unsigned char rbTEMPLATE_RESTAPI_CNONCE_BUF;
 _GLOBAL_CONST unsigned char rbTEMPLATE_RESTAPI_NC_BUF;
 _GLOBAL_CONST unsigned char rbTEMPLATE_RESTAPI_NONCE_BUF;
 _GLOBAL_CONST unsigned char rbTEMPLATE_RESTAPI_REALM_BUF;
 _GLOBAL_CONST unsigned long rbTEMPLATE_RESTAPI_DIGEST_A_BUF;
 _GLOBAL_CONST unsigned long rbTEMPLATE_RESTAPI_HASH_IN_BUF;
 _GLOBAL_CONST unsigned long rbTEMPLATE_RESTAPI_PARSE_VAL_BUF;
 _GLOBAL_CONST unsigned long rbTEMPLATE_RESTAPI_PARSE_KEY_BUF;
 _GLOBAL_CONST unsigned long rbTEMPLATE_RESTAPI_RES_DATA_BUF;
 _GLOBAL_CONST unsigned long rbTEMPLATE_RESTAPI_RES_RAW_BUF;
 _GLOBAL_CONST unsigned long rbTEMPLATE_RESTAPI_REQ_DATA_BUF;
 _GLOBAL_CONST unsigned long rbTEMPLATE_RESTAPI_REQ_RAW_BUF;
 _GLOBAL_CONST unsigned long rbTEMPLATE_RESTAPI_REQ_URI_BUF;
 _GLOBAL_CONST float rbTEMPLATE_TEACHING_PING_DELAY;
 _GLOBAL_CONST float rbTEMPLATE_TEACHING_TIMEOUT;
 _GLOBAL_CONST unsigned short rbTEMPLATE_TEACHING_MDAT_P_PLC;
 _GLOBAL_CONST unsigned short rbTEMPLATE_TEACHING_MDAT_P_ROB;
 _GLOBAL_CONST plcstring rbtROBOT_CTRL_STR_EV_AUTO_EXEC[41];
 _GLOBAL_CONST plcstring rbtROBOT_CTRL_STR_EV_START_PROC[41];
 _GLOBAL_CONST plcstring rbtROBOT_CTRL_STR_EV_LOAD_MOD[41];
 _GLOBAL_CONST plcstring rbtROBOT_CTRL_STR_EV_TRANSF_MOD[41];
 _GLOBAL_CONST plcstring rbtROBOT_CTRL_STR_EV_ROBOT_PREF[41];
 _GLOBAL_CONST unsigned long rbtROBOT_CTLR_VAR_FILE_BUFFER;
 _GLOBAL_CONST unsigned char rbtROBOT_CTRL_PATH_STRING_LEN;
 _GLOBAL_CONST unsigned char rbtROBOT_CTRL_PROC_STRING_LEN;
 _GLOBAL_CONST unsigned char rbtROBOT_CTRL_MODULE_STRING_LEN;
 _GLOBAL_CONST unsigned char rbtROBOT_CTRL_MAX_WOBJ_NAMES;
 _GLOBAL_CONST unsigned char rbtROBOT_CTRL_MAX_TOOL_NAMES;
 _GLOBAL_CONST unsigned char rbtROBOT_CTRL_GET_WOBJ_NAMES;
 _GLOBAL_CONST unsigned char rbtROBOT_CTRL_GET_TOOL_NAMES;
 _GLOBAL_CONST unsigned char rbtROBOT_CTRL_GET_ROBOT_INFO;
 _GLOBAL_CONST unsigned char rbtROBOT_CTRL_GET_MOTION_DATA;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_PLC_PORT_USER_DATA;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_ROB_PORT_USER_DATA;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_PLC_PORT_MOT_INFO;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_ROB_PORT_MOT_INFO;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_PLC_PORT_CMD_STAT;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_ROB_PORT_CMD_STAT;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_MAX_TIME_DIFF;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_TRANSFER_TIMEOUT;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_PROFISAFE_ACK_TIME;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_CMD_DONE_TIMEOUT;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_INFO_DATA_TIMEOUT;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_COMM_TIMEOUT;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_PING_REPEAT_DELAY;
 _GLOBAL_CONST unsigned long rbtROBOT_CTRL_ICMP_PING_TIMEOUT;
 _GLOBAL_CONST plcstring PATH_SIM[81];
 _GLOBAL_CONST plcstring PATH[81];
 _GLOBAL_CONST unsigned char rbTEMPLATE_ASCII_SEMICOLON;
 _GLOBAL_CONST unsigned char rbTEMPLATE_ASCII_NEWLINE;
 _GLOBAL_CONST unsigned char rbTEMPLATE_MAX_WOBJS_MIN_1;
 _GLOBAL_CONST unsigned char rbTEMPLATE_MAX_WOBJS;
 _GLOBAL_CONST unsigned char rbTEMPLATE_MAX_TOOLS_MIN_1;
 _GLOBAL_CONST unsigned char rbTEMPLATE_MAX_TOOLS;
 _GLOBAL_CONST plcstring rbTEMPLATE_EMPTY_STRING_32[33];
 _GLOBAL_CONST plcstring rbTEMPLATE_EMPTY_STRING_255[256];
#endif




/* Datatypes and datatypes of function blocks */
typedef enum RBTemplateFacilitiesEnum
{	rbTEMPLATE_ROBOT_FAC = 230,
	rbTEMPLATE_TCP_COM_FAC = 250,
	rbTEMPLATE_FD_LINK_FAC_BASIC = 270,
	rbTEMPLATE_HW_DIAG_FAC_BASIC = 290,
	rbTEMPLATE_DG_DATA_FAC_BASIC = 300,
	rbTEMPLATE_TEACHING_FAC = 320,
	rbTEMPLATE_RESTAPI_FAC = 340,
	rbTEMPLATE_LICENSE_FAC = 350,
	rbTEMPLATE_LOGBOOK_FAC = 360
} RBTemplateFacilitiesEnum;

typedef enum RBTemplateABBPredefStoppointEnum
{	abbSTOPPOINT_NONE = 0,
	abbSTOPPOINT_INPOS20 = 10,
	abbSTOPPOINT_INPOS50 = 11,
	abbSTOPPOINT_INPOS100 = 12,
	abbSTOPPOINT_STOPTIME0_5 = 20,
	abbSTOPPOINT_STOPTIME1_0 = 21,
	abbSTOPPOINT_STOPTIME1_5 = 22,
	abbSTOPPOINT_FLLWTIME0_5 = 30,
	abbSTOPPOINT_FLLWTIME1_0 = 31,
	abbSTOPPOINT_FLLWTIME1_5 = 32
} RBTemplateABBPredefStoppointEnum;

typedef enum RBTemplateABBPredefSpeedEnum
{	abbSPEED_NONE = 0,
	abbSPEED_V5 = 5,
	abbSPEED_V10 = 10,
	abbSPEED_V20 = 20,
	abbSPEED_V30 = 30,
	abbSPEED_V40 = 40,
	abbSPEED_V50 = 50,
	abbSPEED_V60 = 60,
	abbSPEED_V70 = 70,
	abbSPEED_V80 = 80,
	abbSPEED_V100 = 100,
	abbSPEED_V150 = 150,
	abbSPEED_V200 = 200,
	abbSPEED_V300 = 300,
	abbSPEED_V400 = 400,
	abbSPEED_V500 = 500,
	abbSPEED_V600 = 600,
	abbSPEED_V800 = 800,
	abbSPEED_V1000 = 1000,
	abbSPEED_V1500 = 1500,
	abbSPEED_V2000 = 2000,
	abbSPEED_V2500 = 2500,
	abbSPEED_V3000 = 3000,
	abbSPEED_V4000 = 4000,
	abbSPEED_V5000 = 5000,
	abbSPEED_V6000 = 6000,
	abbSPEED_V7000 = 7000
} RBTemplateABBPredefSpeedEnum;

typedef enum RBTemplateABBPredefZoneEnum
{	abbZONE_Z0 = 0,
	abbZONE_Z1 = 1,
	abbZONE_Z5 = 5,
	abbZONE_Z10 = 10,
	abbZONE_Z15 = 15,
	abbZONE_Z20 = 20,
	abbZONE_Z30 = 30,
	abbZONE_Z40 = 40,
	abbZONE_Z50 = 50,
	abbZONE_Z60 = 60,
	abbZONE_Z80 = 80,
	abbZONE_Z100 = 100,
	abbZONE_Z150 = 150,
	abbZONE_Z200 = 200,
	abbZONE_FINE = 998,
	abbZONE_NONE = 999
} RBTemplateABBPredefZoneEnum;

typedef enum RBTemplateABBMoveInstrEnum
{	abbMOVE_J = 0,
	abbMOVE_L = 1
} RBTemplateABBMoveInstrEnum;

typedef enum RBTemplateABBStoppointTypeEnum
{	abbSTOPPOINT_TYPE_NONE = 0,
	abbSTOPPOINT_TYPE_INPOS = 1,
	abbSTOPPOINT_TYPE_STOPTIME = 2,
	abbSTOPPOINT_TYPE_FOLLOWTIME = 3
} RBTemplateABBStoppointTypeEnum;

typedef enum RBTemplateModGenStateEnum
{	rbTEMPLATE_MOD_GEN_NOT_ENABLED = 0,
	rbTEMPLATE_MOD_GEN_WAIT_CMD = 100,
	rbTEMPLATE_MOD_GEN_CHK_INSTR_LST = 110,
	rbTEMPLATE_MOD_GEN_GEN_INSTR_LST = 120,
	rbTEMPLATE_MOD_GEN_ERROR_RESET = 1000
} RBTemplateModGenStateEnum;

typedef enum RBTemplateModGenFileGenState
{	rbTEMPLATE_MOD_GEN_OPEN_DEVICE = 0,
	rbTEMPLATE_MOD_GEN_GET_INFO_FILE = 10,
	rbTEMPLATE_MOD_GEN_DELETE_FILE = 30,
	rbTEMPLATE_MOD_GEN_CREATE_FILE = 40,
	rbTEMPLATE_MOD_GEN_CREATE_HEADER = 50,
	rbTEMPLATE_MOD_GEN_WRITE_HEADER = 60,
	rbTEMPLATE_MOD_GEN_CREATE_INSTR = 70,
	rbTEMPLATE_MOD_GEN_WRITE_INSTR = 80,
	rbTEMPLATE_MOD_GEN_WRITE_CLOSE = 90,
	rbTEMPLATE_MOD_GEN_CLOSE_FILE = 100
} RBTemplateModGenFileGenState;

typedef enum RBTemplateModGenErrorEnum
{	rbTEMPLATE_MODGEN_ERR_INVFILE = 45300,
	rbTEMPLATE_MODGEN_ERR_INVLIST = 45301,
	rbTEMPLATE_MODGEN_ERR_LSTLEN = 45302,
	rbTEMPLATE_MODGEN_ERR_FILE = 45303
} RBTemplateModGenErrorEnum;

typedef enum RBTemplateRobotCtrlProcExecEnum
{	rbtROBOT_CTRL_WAIT_FOR_CMD = 0,
	rbtROBOT_CTRL_WAIT_LOAD_CMD = 10,
	rbtROBOT_CTRL_WAIT_LOAD_STAT = 20,
	rbtROBOT_CTRL_WAIT_START_CMD = 30,
	rbtROBOT_CTRL_WAIT_START_STAT = 40,
	rbtROBOT_CTRL_PROC_EXECUTING = 50
} RBTemplateRobotCtrlProcExecEnum;

typedef enum RBTemplateRobotCtrlCommStateEnum
{	rbtABB_ROB_CTRL_COM_NOT_EN = 0,
	rbtABB_ROB_CTRL_COM_PING = 10,
	rbtABB_ROB_CTRL_COM_IDENT_LOAD = 15,
	rbtABB_ROB_CTRL_COM_IDENT_SAVE = 16,
	rbtABB_ROB_CTRL_COM_HANDSHAKE = 20,
	rbtABB_ROB_CTRL_COM_START = 30,
	rbtABB_ROB_CTRL_COM_WAIT = 40,
	rbtABB_ROB_CTRL_COM_DISABLE = 50,
	rbtABB_ROB_CTRL_COM_ERROR = 60
} RBTemplateRobotCtrlCommStateEnum;

typedef enum RBTemplateRobotCtrlRobAutExeEnum
{	rbtROBOT_CTRL_STATE_ROB_AE_WAIT = 0,
	rbtROBOT_CTRL_STATE_ROB_AE_TRNSF = 1,
	rbtROBOT_CTRL_STATE_ROB_AE_LOAD = 2,
	rbtROBOT_CTRL_STATE_ROB_AE_START = 3
} RBTemplateRobotCtrlRobAutExeEnum;

typedef enum RBTemplateRobotCtrlRobStateEnum
{	rbtROBOT_CTRL_STATE_ROB_INIT = 0,
	rbtROBOT_CTRL_STATE_ROB_WAIT = 10,
	rbtROBOT_CTRL_STATE_ROB_LOAD = 20,
	rbtROBOT_CTRL_STATE_ROB_START = 30,
	rbtROBOT_CTRL_STATE_ROB_RUN = 40,
	rbtROBOT_CTRL_STATE_ROB_TRANSFER = 50,
	rbtROBOT_CTRL_STATE_ROB_ERROR = 60
} RBTemplateRobotCtrlRobStateEnum;

typedef enum RBTemplateRobotCtrlRobErrorEnum
{	rbtROBOT_CTRL_ERR_ROB_NONE = 0,
	rbtROBOT_CTRL_ERR_ROB_CMD_TOUT = 45135,
	rbtROBOT_CTRL_ERR_ROB_MOD_NAME = 45136,
	rbtROBOT_CTRL_ERR_ROB_MOD_LOAD = 45137,
	rbtROBOT_CTRL_ERR_ROB_PROC_NAME = 45138,
	rbtROBOT_CTRL_ERR_ROB_PROC_START = 45139,
	rbtROBOT_CTRL_ERR_ROB_PROC_EXEC = 45140,
	rbtROBOT_CTRL_ERR_ROB_FILE_NAME = 45141,
	rbtROBOT_CTRL_ERR_ROB_TRANSFER = 45142,
	rbtROBOT_CTRL_ERR_ROB_TRANS_TOUT = 45143,
	rbtROBOT_CTRL_ERR_ROB_MOT_SUP = 45144
} RBTemplateRobotCtrlRobErrorEnum;

typedef enum RBTemplateRobCtrlErrorEnum
{	rbtROBOT_CTRL_ERR_NO_IP_ADDRESS = 45100,
	rbtROBOT_CTRL_ERR_PING = 45101,
	rbtROBOT_CTRL_ERR_HANDSHAKE = 45102,
	rbtROBOT_CTRL_ERR_COMMUNICATION = 45103,
	rbtROBOT_CTRL_ERR_VERSION = 45104,
	rbtROBOT_CTRL_ERR_PLC_LICENSE = 45105,
	rbtROBOT_CTRL_ERR_ROB_LICENSE = 45106,
	rbtROBOT_CTRL_INFO_MANUAL_MODE = 45107,
	rbtROBOT_CTRL_ERR_RESERVE1 = 45108,
	rbtROBOT_CTRL_ERR_RESERVE2 = 45109,
	rbtROBOT_CTRL_ERR_EMSTOP = 45110,
	rbtROBOT_CTRL_ERR_EXEERR = 45111,
	rbtROBOT_CTRL_ERR_RUNCHAIN = 45112,
	rbtROBOT_CTRL_ERR_SAFEMOVE = 45113,
	rbtROBOT_CTRL_ERR_RESERVE3 = 45114,
	rbtROBOT_CTRL_ERR_UNEXP_PRG_STOP = 45115,
	rbtROBOT_CTRL_ERR_CMD_UNFINISHED = 45116,
	rbtROBOT_CTRL_ERR_INV_SRC_PATH = 45117,
	rbtROBOT_CTRL_ERR_UPLOAD_MOD = 45118,
	rbtROBOT_CTRL_ERR_EMP_ROB_LIC = 45119,
	rbtROBOT_CTRL_ERR_UPL_ROB_LIC = 45120,
	rbtROBOT_CTRL_ERR_SAVE_FILE_UNS = 45121,
	rbtROBOT_CTRL_ERR_SAVE_FILE_BUF = 45122
} RBTemplateRobCtrlErrorEnum;

typedef enum RBTemplateRobotCtrlStatusEnum
{	rbtROBOT_CTRL_STAT_MOT_ON = 45100,
	rbtROBOT_CTRL_STAT_MOT_OFF = 45101,
	rbtROBOT_CTRL_STAT_START = 45102,
	rbtROBOT_CTRL_STAT_STOP = 45103,
	rbtROBOT_CTRL_STAT_MOD_TRANSF = 45104,
	rbtROBOT_CTRL_STAT_MOD_LOAD = 45105,
	rbtROBOT_CTRL_STAT_PROC_START = 45106,
	rbtROBOT_CTRL_STAT_AUTO_EXEC = 45107
} RBTemplateRobotCtrlStatusEnum;

typedef enum RBTemplateRobotCtrlStateEnum
{	rbtROBOT_CTRL_NOT_ENABLED = 0,
	rbtROBOT_CTRL_CONNECT = 10,
	rbtROBOT_CTRL_CHK_ROB_LICENSE = 12,
	rbtROBOT_CTRL_UPL_ROB_LICENSE = 13,
	rbtROBOT_CTRL_CHECK_LICENSE = 15,
	rbtROBOT_CTRL_CHECK_VERSION = 20,
	rbtROBOT_CTRL_ROB_MODE = 25,
	rbtROBOT_CTRL_GET_ROB_COUNT = 26,
	rbtROBOT_CTRL_READY = 30,
	rbtROBOT_CTRL_ACTIVE = 40,
	rbtROBOT_CTRL_TURNING_MOTORS_ON = 50,
	rbtROBOT_CTRL_TURNING_MOTORS_OFF = 60,
	rbtROBOT_CTRL_PROG_RUNNING = 70,
	rbtROBOT_CTRL_TRAP_ROUTINE = 80,
	rbtROBOT_CTRL_WAIT_FOR_PRG_START = 90,
	rbtROBOT_CTRL_WAIT_FOR_PRG_STOP = 100,
	rbtROBOT_CTRL_UPLOAD_MODULE = 120,
	rbtROBOT_CTRL_SAVE_FILE = 140,
	rbtROBOT_CTRL_CHECK_ROB_ERR = 200,
	rbtROBOT_CTRL_RESET_ROB_EMSTOP = 210,
	rbtROBOT_CTRL_RESET_ROB_EXEERR = 220,
	rbtROBOT_CTRL_ERROR = 300
} RBTemplateRobotCtrlStateEnum;

typedef enum RBTemplateCommTcpStateEnum
{	rbTEMPLATE_COMM_TCP_NOT_ENABLED = 0,
	rbTEMPLATE_COMM_TCP_OPEN = 10,
	rbTEMPLATE_COMM_TCP_SOCK_PARAM = 20,
	rbTEMPLATE_COMM_TCP_CONNECT = 30,
	rbTEMPLATE_COMM_TCP_MODE = 40,
	rbTEMPLATE_COMM_TCP_RECV = 50,
	rbTEMPLATE_COMM_TCP_SEND = 60,
	rbTEMPLATE_COMM_TCP_CLOSE = 70,
	rbTEMPLATE_COMM_TCP_ERROR = 80
} RBTemplateCommTcpStateEnum;

typedef enum RBTemplateCommTcpErrorEnum
{	rbTEMPLATE_COMM_TCP_ERR_INV_DATA = 45209,
	rbTEMPLATE_COMM_TCP_ERR_INV_IDEN = 45210,
	rbTEMPLATE_COMM_TCP_ERR_OPEN = 45211,
	rbTEMPLATE_COMM_TCP_ERR_PARAM = 45212,
	rbTEMPLATE_COMM_TCP_ERR_CLOSE = 45213,
	rbTEMPLATE_COMM_TCP_ERR_CONNECT = 45214,
	rbTEMPLATE_COMM_TCP_ERR_RECV = 45215,
	rbTEMPLATE_COMM_TCP_ERR_SEND = 45216,
	rbTEMPLATE_COMM_TCP_ERR_TOUT = 45217
} RBTemplateCommTcpErrorEnum;

typedef enum RBTemplateToolDecStateEnum
{	rbtTOOL_DEC_DISABLED = 0,
	rbtTOOL_DEC_WAIT_FOR_ENABLED = 10,
	rbtTOOL_DEC_WAIT_FOR_CMD = 20,
	rbtTOOL_DEC_OPEN_FILE = 30,
	rbtTOOL_DEC_GET_FILE_INFO = 35,
	rbtTOOL_DEC_READ_DATA_FOR_NAMES = 40,
	rbtTOOL_DEC_FIND_OFFSETS = 50,
	rbtTOOL_DEC_FIND_NAMES = 60,
	rbtTOOL_DEC_CHECK_NAME = 70,
	rbtTOOL_DEC_READ_DATA_FOR_PARAMS = 80,
	rbtTOOL_DEC_SEPARATE_ROW = 90,
	rbtTOOL_DEC_FIND_TOOL_NAME = 100,
	rbtTOOL_DEC_FIND_ROBHOLD = 110,
	rbtTOOL_DEC_FIND_PARAMS = 120,
	rbtTOOL_DEC_FIND_VALUE = 130,
	rbtTOOL_DEC_CLOSE_FILE = 140,
	rbtTOOL_DEC_ERROR = 255
} RBTemplateToolDecStateEnum;

typedef enum RBTemplateToolDecStatusIDEnum
{	rbtTOOL_DEC_FUB_OK = 0,
	rbtTOOL_DEC_FUB_BUSY = 65535,
	rbtTOOL_DEC_ERR_NO_TOOLS = 45000,
	rbtTOOL_DEC_ERR_BAD_TOOL_SYNTAX = 45001,
	rbtTOOL_DEC_ERR_BAD_TOOL_NAME = 45002,
	rbtTOOL_DEC_ERR_TOO_MANY_TOOLS = 45003,
	rbtTOOL_DEC_ERR_ROW_END_NOT_FND = 45004,
	rbtTOOL_DEC_ERR_BAD_TOOL_INDEX = 45005,
	rbtTOOL_DEC_ERR_ROBH_TR_AND_FALS = 45006,
	rbtTOOL_DEC_ERR_ROBH_NOT_FOUND = 45007,
	rbtTOOL_DEC_ERR_BAD_PARAMS_SNTX = 45008
} RBTemplateToolDecStatusIDEnum;

typedef enum RBTemplateWobjDecStateEnum
{	rbtWOBJ_DEC_DISABLED = 0,
	rbtWOBJ_DEC_WAIT_FOR_ENABLED = 10,
	rbtWOBJ_DEC_WAIT_FOR_CMD = 20,
	rbtWOBJ_DEC_OPEN_FILE = 30,
	rbtWOBJ_DEC_GET_FILE_INFO = 35,
	rbtWOBJ_DEC_READ_DATA_FOR_NAMES = 40,
	rbtWOBJ_DEC_FIND_OFFSETS = 50,
	rbtWOBJ_DEC_FIND_NAMES = 60,
	rbtWOBJ_DEC_CHECK_NAME = 70,
	rbtWOBJ_DEC_READ_DATA_FOR_PARAMS = 80,
	rbtWOBJ_DEC_SEPARATE_ROW = 90,
	rbtWOBJ_DEC_FIND_WOBJ_NAME = 100,
	rbtWOBJ_DEC_FIND_ROBHOLD = 110,
	rbtWOBJ_DEC_FIND_USR_FRAM_PROG = 120,
	rbtWOBJ_DEC_FIND_USR_FRAM_MECH = 130,
	rbtWOBJ_DEC_FIND_PARAMS = 140,
	rbtWOBJ_DEC_FIND_VALUE = 150,
	rbtWOBJ_DEC_CLOSE_FILE = 160,
	rbtWOBJ_DEC_ERROR = 255
} RBTemplateWobjDecStateEnum;

typedef enum RBTemplateWobjDecStatusIDEnum
{	rbtWOBJ_DEC_FUB_OK = 0,
	rbtWOBJ_DEC_FUB_BUSY = 65535,
	rbtWOBJ_DEC_ERR_NO_WOBJS = 45050,
	rbtWOBJ_DEC_ERR_BAD_WOBJ_SYNTAX = 45051,
	rbtWOBJ_DEC_ERR_BAD_WOBJ_NAME = 45052,
	rbtWOBJ_DEC_ERR_TOO_MANY_WOBJS = 45053,
	rbtWOBJ_DEC_ERR_ROW_END_NOT_FND = 45054,
	rbtWOBJ_DEC_ERR_BAD_WOBJ_INDEX = 45055,
	rbtWOBJ_DEC_ERR_ROBH_TR_AND_FALS = 45056,
	rbtWOBJ_DEC_ERR_ROBH_NOT_FOUND = 45057,
	rbtWOBJ_DEC_ERR_UFP_TR_AND_FALS = 45058,
	rbtWOBJ_DEC_ERR_UFP_NOT_FOUND = 45059,
	rbtWOBJ_DEC_ERR_UFMU_NOT_FOUND = 45060,
	rbtWOBJ_DEC_ERR_BAD_PARAMS_SNTX = 45061
} RBTemplateWobjDecStatusIDEnum;

typedef enum RBTemplateTargetEditorStateEnum
{	rbtTARGET_EDIT_DISABLED = 0,
	rbtTARGET_EDIT_WAIT_FOR_ENABLED = 10,
	rbtTARGET_EDIT_WAIT_FOR_CMD = 20,
	rbtTARGET_EDIT_OPEN_FILE = 30,
	rbtTARGET_EDIT_GET_FILE_INFO = 40,
	rbtTARGET_EDIT_READ_DATA = 50,
	rbtTARGET_EDIT_FIND_NAME = 60,
	rbtTARGET_EDIT_SEPARATE_ROW = 70,
	rbtTARGET_EDIT_FIND_PARAMS = 80,
	rbtTARGET_EDIT_FIND_VALUE = 90,
	rbtTARGET_EDIT_MAKE_TARGET_STR = 100,
	rbtTARGET_EDIT_FIND_END_OF_ROW = 110,
	rbtTARGET_EDIT_WRITE_TARGET = 120,
	rbtTARGET_EDIT_FIND_NEWLINE = 130,
	rbtTARGET_EDIT_READ_DATA_FOR_INS = 140,
	rbtTARGET_EDIT_MOVE_DATA = 150,
	rbtTARGET_EDIT_INSERT_DATA = 160,
	rbtTARGET_EDIT_CLOSE_FILE = 170,
	rbtTARGET_EDIT_ERROR = 255
} RBTemplateTargetEditorStateEnum;

typedef enum RBTemplateTargetEditStatusIDEnum
{	rbtTARGET_EDIT_FUB_OK = 0,
	rbtTARGET_EDIT_FUB_BUSY = 65535,
	rbtTARGET_EDIT_ERR_TAR_TYPE = 45150,
	rbtTARGET_EDIT_ERR_NO_CMD = 45151,
	rbtTARGET_EDIT_ERR_TAR_NOT_FOUND = 45152,
	rbtTARGET_EDIT_ERR_NO_ROW_END = 45153,
	rbtTARGET_EDIT_ERR_PARAMS_SNTX = 45154,
	rbtTARGET_EDIT_ERR_NEWLINE = 45155,
	rbtTARGET_EDIT_ERR_NO_TAR_NAME = 45156
} RBTemplateTargetEditStatusIDEnum;

typedef enum RBTemplateTeachingStateEnum
{	rbTEMPLATE_TEACHING_NOT_ENABLED = 0,
	rbTEMPLATE_TEACHING_PING = 10,
	rbTEMPLATE_TEACHING_IDENT_LOAD = 15,
	rbTEMPLATE_TEACHING_CONNECT = 20,
	rbTEMPLATE_TEACHING_WAIT = 30,
	rbTEMPLATE_TEACHING_CREATE_INSTR = 40,
	rbTEMPLATE_TEACHING_EDIT_INSTR = 50,
	rbTEMPLATE_TEACHING_INSERT_INSTR = 60,
	rbTEMPLATE_TEACHING_REMOVE_INSTR = 70,
	rbTEMPLATE_TEACHING_SAVE = 80,
	rbTEMPLATE_TEACHING_DISABLE = 90,
	rbTEMPLATE_TEACHING_ERROR = 100
} RBTemplateTeachingStateEnum;

typedef enum RBTemplateTeachingErrorEnum
{	rbTEMPLATE_TEACHING_ERR_IPADR = 45250,
	rbTEMPLATE_TEACHING_ERR_PATH = 45251,
	rbTEMPLATE_TEACHING_ERR_LIST = 45252,
	rbTEMPLATE_TEACHING_ERR_PING = 45255,
	rbTEMPLATE_TEACHING_ERR_COMM = 45256,
	rbTEMPLATE_TEACHING_ERR_SAVE = 45257,
	rbTEMPLATE_TEACHING_ERR_PNAME = 45258,
	rbTEMPLATE_TEACHING_ERR_EDINDEX = 45260,
	rbTEMPLATE_TEACHING_ERR_FULL_LST = 45261,
	rbTEMPLATE_TEACHING_ERR_RMINDEX = 45262,
	rbTEMPLATE_TEACHING_ERR_INSINDEX = 45263,
	rbTEMPLATE_TEACHING_ERR_FNAME = 45264,
	rbTEMPLATE_TEACHING_ERR_STOPDATA = 45270
} RBTemplateTeachingErrorEnum;

typedef enum RBTemplateRestApiStateEnum
{	rbTEMPLATE_RESTAPI_NOT_ENABLED = 0,
	rbTEMPLATE_RESTAPI_CONNECT = 10,
	rbTEMPLATE_RESTAPI_WAIT = 20,
	rbTEMPLATE_RESTAPI_UPL_FILE_P = 30,
	rbTEMPLATE_RESTAPI_UPL_FILE = 31,
	rbTEMPLATE_RESTAPI_UPL_FILE_W = 32,
	rbTEMPLATE_RESTAPI_READFILE = 40,
	rbTEMPLATE_RESTAPI_RMSHIP_REQ_S = 50,
	rbTEMPLATE_RESTAPI_RMSHIP_REQ_W = 51,
	rbTEMPLATE_RESTAPI_FILE_READ_S = 52,
	rbTEMPLATE_RESTAPI_FILE_READ_W = 53,
	rbTEMPLATE_RESTAPI_DOWNLOAD_P = 60,
	rbTEMPLATE_RESTAPI_DOWNLOAD_S = 61,
	rbTEMPLATE_RESTAPI_DOWNLOAD_W = 62,
	rbTEMPLATE_RESTAPI_DOWNLOAD_F = 63,
	rbTEMPLATE_RESTAPI_UPL_DATA_P = 70,
	rbTEMPLATE_RESTAPI_UPL_DATA = 71,
	rbTEMPLATE_RESTAPI_UPL_DATA_W = 72,
	rbTEMPLATE_RESTAPI_DISABLE = 90,
	rbTEMPLATE_RESTAPI_ERROR = 100
} RBTemplateRestApiStateEnum;

typedef enum RBTemplateRestApiErrorEnum
{	rbTEMPLATE_RESTAPI_ERR_INV_PAR = 45321,
	rbTEMPLATE_RESTAPI_ERR_INT_BUF = 45322,
	rbTEMPLATE_RESTAPI_ERR_HASH_BUF = 45323,
	rbTEMPLATE_RESTAPI_ERR_CONN = 45324,
	rbTEMPLATE_RESTAPI_ERR_AUTH = 45325,
	rbTEMPLATE_RESTAPI_ERR_HEAD_PARS = 45326,
	rbTEMPLATE_RESTAPI_ERR_UPLOAD = 45327,
	rbTEMPLATE_RESTAPI_ERR_UPL_ENC = 45328,
	rbTEMPLATE_RESTAPI_ERR_UPL_EMPTY = 45329,
	rbTEMPLATE_RESTAPI_ERR_UPL_FNAME = 45330,
	rbTEMPLATE_RESTAPI_ERR_COOKIES = 45331,
	rbTEMPLATE_RESTAPI_ERR_URI_BUF = 45332,
	rbTEMPLATE_RESTAPI_ERR_REQ_D_BUF = 45333,
	rbTEMPLATE_RESTAPI_ERR_UPL_SRCF = 45334,
	rbTEMPLATE_RESTAPI_ERR_READFILE = 45335,
	rbTEMPLATE_RESTAPI_ERR_FILE_NAME = 45336,
	rbTEMPLATE_RESTAPI_ERR_DWL_DSTF = 45337,
	rbTEMPLATE_RESTAPI_ERR_DATA_BUF = 45338,
	rbTEMPLATE_RESTAPI_ERR_DOWNLOAD = 45339,
	rbTEMPLATE_RESTAPI_ERR_SMALL_BUF = 45340,
	rbTEMPLATE_RESTAPI_ERR_NOT_FOUND = 45341,
	rbTEMPLATE_RESTAPI_ERR_PATH_LONG = 45342
} RBTemplateRestApiErrorEnum;

typedef enum RBTemplateRestApiSaveFileEnum
{	rbTEMPLATE_RESTAPI_SF_FDLINK = 0,
	rbTEMPLATE_RESTAPI_SF_CREATE = 10,
	rbTEMPLATE_RESTAPI_SF_DELETE = 20,
	rbTEMPLATE_RESTAPI_SF_WRITE = 30,
	rbTEMPLATE_RESTAPI_SF_CLOSE = 40,
	rbTEMPLATE_RESTAPI_SF_UNLINK = 50
} RBTemplateRestApiSaveFileEnum;

typedef enum RBTemplateRestApiReadFileEnum
{	rbTEMPLATE_RESTAPI_RF_FDLINK = 0,
	rbTEMPLATE_RESTAPI_RF_OPEN = 10,
	rbTEMPLATE_RESTAPI_RF_READ = 20,
	rbTEMPLATE_RESTAPI_RF_CLOSE = 30,
	rbTEMPLATE_RESTAPI_RF_UNLINK = 40
} RBTemplateRestApiReadFileEnum;

typedef enum RBTemplateRestApiConnectEnum
{	rbTEMPLATE_RESTAPI_CON_INIT = 0,
	rbTEMPLATE_RESTAPI_CON_PARSE = 10,
	rbTEMPLATE_RESTAPI_CON_HASH_A1 = 20,
	rbTEMPLATE_RESTAPI_CON_HASH_A1_W = 21,
	rbTEMPLATE_RESTAPI_CON_HASH_A2 = 30,
	rbTEMPLATE_RESTAPI_CON_HASH_A2_W = 31,
	rbTEMPLATE_RESTAPI_CON_PREPARE = 40,
	rbTEMPLATE_RESTAPI_CON_HASH_R = 50,
	rbTEMPLATE_RESTAPI_CON_HASH_R_W = 51,
	rbTEMPLATE_RESTAPI_CON_SEND = 60,
	rbTEMPLATE_RESTAPI_CON_COOKIES = 70,
	rbTEMPLATE_RESTAPI_CON_DONE = 80
} RBTemplateRestApiConnectEnum;

typedef enum RBTemplateLicenseErrorEnum
{	rbTEMPLATE_LIC_ERR_INV_PARAM = 45350,
	rbTEMPLATE_LIC_ERR_DONG = 45351,
	rbTEMPLATE_LIC_ERR_GET_INFO = 45352
} RBTemplateLicenseErrorEnum;

typedef enum RBTemplateLicenseStateEnum
{	rbTEMPLATE_LIC_STATE_INIT,
	rbTEMPLATE_LIC_STATE_DONG_INFO,
	rbTEMPLATE_LIC_STATE_DONG_READ,
	rbTEMPLATE_LIC_STATE_DONG_GET,
	rbTEMPLATE_LIC_STATE_CREATE_INFO,
	rbTEMPLATE_LIC_STATE_GET_INFO,
	rbTEMPLATE_LIC_STATE_CHECK,
	rbTEMPLATE_LIC_STATE_DONE,
	rbTEMPLATE_LIC_STATE_ERROR
} RBTemplateLicenseStateEnum;

typedef enum RBTemplateLogbookErrorEnum
{	rbTEMPLATE_LOGBOOK_ERR_ = 45370,
	rbTEMPLATE_LOGBOOK_ERR_GET_IDENT = 45371,
	rbTEMPLATE_LOGBOOK_ERR_CREATE = 45372,
	rbTEMPLATE_LOGBOOK_ERR_WRITE = 45373
} RBTemplateLogbookErrorEnum;

typedef enum RBTemplateLogbookStateEnum
{	rbTEMPLATE_LOGBOOK_STATE_WAIT = 0,
	rbTEMPLATE_LOGBOOK_STATE_IDENT = 1,
	rbTEMPLATE_LOGBOOK_STATE_CREATE = 2,
	rbTEMPLATE_LOGBOOK_STATE_WRITE = 3,
	rbTEMPLATE_LOGBOOK_STATE_DONE = 4,
	rbTEMPLATE_LOGBOOK_STATE_ERROR = 5
} RBTemplateLogbookStateEnum;

typedef enum RBTemplateIdentHandlerErrorEnum
{	rbtIDENT_HANDLER_ERR_ = 0
} RBTemplateIdentHandlerErrorEnum;

typedef enum RBTemplateIdentHandlerStateEnum
{	rbtIDENT_HANDLER_STATE_ENABLE,
	rbtIDENT_HANDLER_STATE_CHK_FILE,
	rbtIDENT_HANDLER_STATE_CREATE,
	rbtIDENT_HANDLER_STATE_WAIT,
	rbtIDENT_HANDLER_STATE_LOAD,
	rbtIDENT_HANDLER_STATE_SAVE,
	rbtIDENT_HANDLER_STATE_ERROR
} RBTemplateIdentHandlerStateEnum;

typedef struct RBTemplateRelToolType
{	float X;
	float Y;
	float Z;
	float Rx;
	float Ry;
	float Rz;
} RBTemplateRelToolType;

typedef struct RBTemplateControllerCmdType
{	plcbit MotorsOn;
	plcbit MotorsOff;
	plcbit Start;
	plcbit StartAtMain;
	plcbit Stop;
	plcbit StopAtEndInstr;
	plcbit ResetEmergencyStop;
	plcbit ResetExecutionError;
	plcbit SystemRestart;
	plcbit OpModeReqAuto;
	plcbit OpModeReqManual;
	plcbit AckAutoMode;
	plcbit ProfiSafeOpAck;
	plcbit Fill1;
	plcbit Fill2;
	plcbit Fill3;
} RBTemplateControllerCmdType;

typedef struct RBTemplateRobotCmdType
{	float ItemWeight;
	unsigned char SpeedOverride;
	plcbit LimitSpeed;
	plcbit RelTool;
	plcbit ModuleLoad;
	plcbit ProcStart;
	plcbit Fill1;
	plcbit Fill2;
	plcbit Fill3;
	struct RBTemplateRelToolType RelToolCords;
	plcstring ModuleName[37];
	plcstring ProcName[33];
	plcbit Fill4;
	plcbit Fill5;
} RBTemplateRobotCmdType;

typedef struct RBTemplateSystemCmdType
{	struct RBTemplateControllerCmdType Controller;
	struct RBTemplateRobotCmdType Robot[4];
} RBTemplateSystemCmdType;

typedef struct RBTemplateControllerStatusType
{	plcbit LicenceApproved;
	plcbit OperatorModeAutoChange;
	plcbit AutomatOn;
	plcbit MotorsOn;
	plcbit MotorsOff;
	plcbit ExecutingMotionTask;
	plcbit EmStop;
	plcbit ExeError;
	plcbit RunChainOk;
	plcbit ESChainOk;
	plcbit ASChainOk;
	plcbit GSChainOk;
	plcbit SSChainOk;
	plcbit SafeMoveViolated;
	plcbit WorldZone[20];
} RBTemplateControllerStatusType;

typedef struct RBTemplateRobotStatusType
{	plcbit LimitSpeed;
	plcbit MechUnitNotMove;
	plcbit MotSupTriggered;
	plcbit LoadModuleDone;
	plcbit ProcExecuting;
	plcbit RapidError;
	unsigned short RapidErrorNumber;
	unsigned char SpeedOverride;
	plcbit Fill1;
} RBTemplateRobotStatusType;

typedef struct RBTemplateSystemStatusType
{	struct RBTemplateControllerStatusType Controller;
	struct RBTemplateRobotStatusType Robot[4];
} RBTemplateSystemStatusType;

typedef struct RBTemplateRobotRobtargetType
{	float X;
	float Y;
	float Z;
	float Q1;
	float Q2;
	float Q3;
	float Q4;
	signed char Cf1;
	signed char Cf4;
	signed char Cf6;
	signed char Cfx;
} RBTemplateRobotRobtargetType;

typedef struct RBTemplateABBRobjointType
{	float Rax1;
	float Rax2;
	float Rax3;
	float Rax4;
	float Rax5;
	float Rax6;
} RBTemplateABBRobjointType;

typedef struct RBTemplateABBExtjointType
{	float Eaxa;
	float Eaxb;
	float Eaxc;
	float Eaxd;
	float Eaxe;
	float Eaxf;
} RBTemplateABBExtjointType;

typedef struct RBTemplateRobotMotionDataType
{	struct RBTemplateRobotRobtargetType Robtarget;
	struct RBTemplateABBRobjointType Jointtarget;
	struct RBTemplateABBExtjointType ExternalAxis;
	float ActTCPSpeed;
	float RefTCPSpeed;
	plcstring ToolName[33];
	plcstring WobjName[33];
} RBTemplateRobotMotionDataType;

typedef struct RBTemplateSystemInfoType
{	plcstring SerialNumber[32];
	plcstring SwVersion[32];
	plcstring SwVersionName[32];
	plcstring ControllerID[32];
	plcstring WAN_IPAddress[32];
	plcstring SystemLanguage[4];
	plcstring SystemName[32];
	plcstring RoboTemplateVersion[12];
	plcstring RobotType[4][32];
} RBTemplateSystemInfoType;

typedef struct RBTemplateUserDataType
{	plcbit Bool[16];
	signed long Dint[8];
	float Real[8];
} RBTemplateUserDataType;

typedef struct RBTemplateABBPosType
{	float X;
	float Y;
	float Z;
} RBTemplateABBPosType;

typedef struct RBTemplateABBOrientType
{	float Q1;
	float Q2;
	float Q3;
	float Q4;
} RBTemplateABBOrientType;

typedef struct RBTemplateABBRobconfType
{	signed char Cf1;
	signed char Cf4;
	signed char Cf6;
	signed char Cfx;
} RBTemplateABBRobconfType;

typedef struct RBTemplateABBRobtargetType
{	struct RBTemplateABBPosType Trans;
	struct RBTemplateABBOrientType Rot;
	struct RBTemplateABBRobconfType Robconf;
	struct RBTemplateABBExtjointType Extax;
} RBTemplateABBRobtargetType;

typedef struct RBTemplateABBIdentnoType
{	unsigned char syncident;
} RBTemplateABBIdentnoType;

typedef struct RBTemplateABBSpeeddataType
{	float v_tcp;
	float v_ori;
	float v_leax;
	float v_reax;
} RBTemplateABBSpeeddataType;

typedef struct RBTemplateABBZonedataType
{	plcbit finep;
	float pzone_tcp;
	float pzone_ori;
	float pzone_eax;
	float zone_ori;
	float zone_leax;
	float zone_reax;
} RBTemplateABBZonedataType;

typedef struct RBTemplateABBInposDataType
{	float position;
	float speed;
	float mintime;
	float maxtime;
} RBTemplateABBInposDataType;

typedef struct RBTemplateABBStoppointdataType
{	enum RBTemplateABBStoppointTypeEnum type;
	plcbit progsynch;
	struct RBTemplateABBInposDataType inposPosition;
	float stoptime;
	float followtime;
	plcstring signal[17];
	float relation;
	float checkvalue;
} RBTemplateABBStoppointdataType;

typedef struct RBTemplateABBPoseType
{	struct RBTemplateABBPosType Trans;
	struct RBTemplateABBOrientType Rot;
} RBTemplateABBPoseType;

typedef struct RBTemplateABBLoaddataType
{	float Mass;
	struct RBTemplateABBPosType Cog;
	struct RBTemplateABBOrientType Aom;
	float Ix;
	float Iy;
	float Iz;
} RBTemplateABBLoaddataType;

typedef struct RBTemplateABBTooldataType
{	plcbit Robhold;
	struct RBTemplateABBPoseType Tframe;
	struct RBTemplateABBLoaddataType Tload;
	plcstring Tname[51];
} RBTemplateABBTooldataType;

typedef struct RBTemplateABBWobjdataType
{	plcbit Robhold;
	plcbit UserFrameProgrammed;
	plcstring UserFrameMechanicalUnit[81];
	struct RBTemplateABBPoseType UserFrame;
	struct RBTemplateABBPoseType ObjectFrame;
	plcstring WorkObjectName[51];
} RBTemplateABBWobjdataType;

typedef struct RBTemplateABBMoveInstrListType
{	plcstring Name[33];
	enum RBTemplateABBMoveInstrEnum Type;
	unsigned char Concurrent;
	struct RBTemplateABBRobtargetType ToPoint;
	struct RBTemplateABBIdentnoType SynchronizationId;
	struct RBTemplateABBSpeeddataType Speed;
	enum RBTemplateABBPredefSpeedEnum PredefinedSpeed;
	float Time;
	float Velocity;
	enum RBTemplateABBPredefZoneEnum PredefinedZone;
	struct RBTemplateABBZonedataType Zone;
	struct RBTemplateABBStoppointdataType InPosition;
	enum RBTemplateABBPredefStoppointEnum PredefinedStoppoint;
	struct RBTemplateABBTooldataType Tool;
	struct RBTemplateABBWobjdataType WorkObject;
	unsigned char Correction;
	struct RBTemplateABBLoaddataType TotalLoad;
} RBTemplateABBMoveInstrListType;

typedef struct RBTemplateABBJointtargetType
{	struct RBTemplateABBRobjointType Robax;
	struct RBTemplateABBExtjointType Extax;
} RBTemplateABBJointtargetType;

typedef struct RBTemplateModGenInfoType
{	struct MTDiagType Diag;
} RBTemplateModGenInfoType;

typedef struct RBTemplateModGenInternalType
{	enum RBTemplateModGenStateEnum State;
	unsigned long ArrSize;
	struct FDLinkBasic FDLinkBasic_0;
	plcstring PathSim[256];
	plcstring Path[256];
	enum RBTemplateModGenFileGenState GenFileState;
	unsigned long FileOffset;
	unsigned long FileIdent;
	struct FileCreate FileCreate_0;
	struct FileInfo FileInfo_0;
	struct fiFILE_INFO pInfo;
	struct FileClose FileClose_0;
	struct FileWrite FileWrite_0;
	struct FileDelete FileDelete_0;
	unsigned long ActInstNum;
	plcstring ActInstructionStr[20001];
} RBTemplateModGenInternalType;

typedef struct RBTemplateRobotCtrlInputCmdType
{	plcbit MotorsOff;
	plcbit MotorsOn;
	plcbit Start;
	plcbit StartAtMain;
	plcbit Stop;
	plcbit StopAtEndInstr;
	plcstring SrcFile[65];
	plcstring DstPath[65];
	plcstring DstPathSim[65];
	plcbit SaveFileFromController;
	plcstring RobotLicenseKey[45];
	plcbit ApplyRobotLicense;
} RBTemplateRobotCtrlInputCmdType;

typedef struct RBTemplateRobotCtrlInputRobType
{	plcbit ErrorReset;
	plcbit LimitSpeed;
	plcbit AutoExecute;
	plcstring ModuleSrcPath[65];
	plcstring ModuleSrcPathSim[65];
	plcstring ModuleName[37];
	plcbit TransferModuleToController;
	plcbit LoadModule;
	plcstring ProcedureName[33];
	plcbit StartProcedure;
	unsigned char SpeedOverride;
	struct RBTemplateRelToolType RelToolCords;
	plcbit RelTool;
} RBTemplateRobotCtrlInputRobType;

typedef struct RBTemplateRobotCtrlRobInfoType
{	plcbit Active;
	plcbit Error;
	enum RBTemplateRobotCtrlRobErrorEnum ErrorCode;
	plcbit LimitSpeed;
	plcbit MechUnitNotMove;
	plcbit MotSupTriggered;
	plcbit TransferDone;
	plcbit LoadModuleDone;
	plcbit ProcExecuting;
	plcbit ProcDone;
	plcstring Tool[32][21];
	plcstring Workobject[32][21];
} RBTemplateRobotCtrlRobInfoType;

typedef struct RBTemplateRobotCtrlStatusType
{	plcbit Ping;
	plcbit Connected;
	plcbit OperatorModeAutoChange;
	plcbit AutomatOn;
	plcbit MotorsOn;
	plcbit MotorsOff;
	plcbit ExecutingMotionTask;
	plcbit EmStop;
	plcbit ExeError;
	plcbit RunChainOk;
	plcbit ESChainOk;
	plcbit ASChainOk;
	plcbit GSChainOk;
	plcbit SSChainOk;
	plcbit SafeMoveViolated;
	plcbit WorldZone[20];
	plcbit SaveFileFromControllerDone;
	plcbit ApplyRobotLicenseDone;
} RBTemplateRobotCtrlStatusType;

typedef struct RBTemplateRobotCtrlConInfoType
{	plcstring SerialNumber[32];
	plcstring SwVersion[32];
	plcstring SwVersionName[32];
	plcstring ControllerID[32];
	plcstring WAN_IPAddress[32];
	plcstring SystemLanguage[4];
	plcstring SystemName[32];
	plcstring RoboTemplateVersion[12];
	unsigned char RobotCount;
	plcstring RobotType[4][32];
} RBTemplateRobotCtrlConInfoType;

typedef struct RBTemplateRobotCtrlInfoType
{	plcbit ARSimUsed;
	unsigned long CmdTime;
	struct RBTemplateRobotCtrlConInfoType Controller;
	struct MTDiagType Diag;
} RBTemplateRobotCtrlInfoType;

typedef struct RBTemplateRobotCtrlIntEdgeType
{	plcbit MotorsOff;
	plcbit MotorsOn;
	plcbit Start;
	plcbit StartAtMain;
	plcbit Stop;
	plcbit StopAtEndInstr;
	plcbit ReqTools;
	plcbit ReqWorkobjects;
	plcbit SaveFileFromController;
} RBTemplateRobotCtrlIntEdgeType;

typedef struct RBTemplateRobotCtrlIntCommType
{	plcbit Active;
	plcstring ControllerIpAdr[16];
	plcbit BypassPing;
	plcbit Enable;
	plcbit Error;
	signed long ErrorCode;
	plcbit ErrorReset;
	plcbit ErrorResetActive;
	plcbit ErrorResetOld;
	plcbit FirstCycleDone;
	plcbit IdentSaveDone;
	unsigned long MotionDataIndex;
	unsigned char Offset;
	plcbit Ping;
	plcbit PingRobot;
	unsigned long RequestData;
	enum RBTemplateRobotCtrlCommStateEnum State;
	plcbit SystemInfoReceived;
	unsigned long Timeout;
	unsigned long ToolWobjIndex;
	unsigned long Cycle[30];
	unsigned char CycleIndex;
	unsigned long CycleAvg;
	unsigned long CycleMin;
	unsigned long CycleMax;
} RBTemplateRobotCtrlIntCommType;

typedef unsigned long RBTemplateSystemReqDataType;

typedef unsigned long RBTemplateHandshakeDataType;

typedef struct RBTemplateRobotCtrlIntDataType
{	struct RBTemplateSystemCmdType SystemCommand;
	struct RBTemplateSystemStatusType SystemStatus;
	RBTemplateSystemReqDataType RequestData;
	struct RBTemplateRobotMotionDataType RobotMotionData;
	struct RBTemplateSystemInfoType SystemInfo;
	RBTemplateHandshakeDataType Handshake;
	plcstring StringData[33];
	struct RBTemplateUserDataType UserDataToController;
	plcstring FileContentBuffer[64001];
	struct RBTemplateUserDataType UserDataFromController;
} RBTemplateRobotCtrlIntDataType;

typedef struct RBTemplateRobotCtrlIntFlagsType
{	plcbit ApplyRobotLicenseOld;
	struct RBTemplateRobotCtrlIntEdgeType ControllerCmdEdge;
	plcbit Error;
	plcbit ErrorResetActive;
	plcbit ErrorResetOld;
	plcbit ErrorRobot;
	plcbit ProfiSafeOpAck;
} RBTemplateRobotCtrlIntFlagsType;

typedef struct RBTemplateCommTcpParType
{	unsigned long* pRobotIpAdr;
	unsigned short PlcPort;
	unsigned short RobotPort;
	unsigned long RecvDataLen;
	unsigned long* pRecvData;
	unsigned long SendDataLen;
	unsigned long* pSendData;
	unsigned long Timeout;
	unsigned long* pIdent;
} RBTemplateCommTcpParType;

typedef struct RBTemplateCommTcpInfoType
{	struct MTDiagType Diag;
	unsigned long RecvDataCnt;
	unsigned long SentDataCnt;
	unsigned long TimeDiff;
} RBTemplateCommTcpInfoType;

typedef struct RBTemplateCommTcpInternalFubType
{	struct TcpOpen TcpOpen_0;
	struct TcpIoctl TcpIoctl_0;
	struct TcpClient TcpClient_0;
	struct TcpRecv TcpRecv_0;
	struct TcpSend TcpSend_0;
	struct TcpClose TcpClose_0;
	struct TON TON_0;
} RBTemplateCommTcpInternalFubType;

typedef struct RBTemplateCommTcpInternalType
{	enum RBTemplateCommTcpStateEnum State;
	plcstring RobotIpAdr[16];
	plcbit SocketOpen;
	plcbit Connected;
	plcbit InitSendDone;
	unsigned long SocketIdent;
	struct tcpLINGER_typ SocketLinger;
	plcbit InitCloseDone;
	struct RBTemplateCommTcpInternalFubType FUB;
	plcbit ErrorResetOld;
	enum RBTemplateCommTcpStateEnum ErrorState;
} RBTemplateCommTcpInternalType;

typedef struct RBTemplateCommTcp
{
	/* VAR_INPUT (analog) */
	struct RBTemplateCommTcpParType Parameters;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct RBTemplateCommTcpInfoType Info;
	/* VAR (analog) */
	struct RBTemplateCommTcpInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
} RBTemplateCommTcp_typ;

typedef struct RBTemplateRestApiParType
{	unsigned long pRobotIpAdr;
	unsigned long pRobotIpAdrSim;
	unsigned long pUsername;
	unsigned long pPassword;
	unsigned long pSrcPath;
	unsigned long pSrcPathSim;
	unsigned long pSrcDevice;
	unsigned long pSrcFile;
	plcbit LinkSrcPath;
	unsigned long pFileName;
	unsigned long pDataBuffer;
	unsigned long DataBufferLen;
	unsigned long pDstPath;
	unsigned long pDstPathSim;
	unsigned long pDstDevice;
	plcbit LinkDstPath;
	unsigned long pDstFile;
} RBTemplateRestApiParType;

typedef struct RBTemplateRestApiCmdType
{	plcbit UploadFile;
	plcbit UploadData;
	plcbit DownloadFile;
	plcbit ReadFile;
} RBTemplateRestApiCmdType;

typedef struct RBTemplateRestApiInfoType
{	struct MTDiagType Diag;
} RBTemplateRestApiInfoType;

typedef struct RBTemplateRestApiIntCmdType
{	plcbit Connect;
} RBTemplateRestApiIntCmdType;

typedef struct RBTemplateRestApiIntStatusType
{	plcbit Connected;
	struct RBTemplateRestApiCmdType CommandEdge;
	enum RBTemplateRestApiConnectEnum ErrorState;
	plcbit ErrorResetOld;
} RBTemplateRestApiIntStatusType;

typedef struct RBTemplateRestApiIntDataType
{	plcstring RequestUri[201];
	plcstring RequestData[8001];
	struct httpRequestHeader_t RequestHeader;
	plcstring RequestRawHeader[3001];
	plcstring ResponseData[8001];
	struct httpResponseHeader_t ResponseHeader;
	plcstring ResponseRawHeader[2001];
	plcstring ParseKey[21];
	plcstring ParsedValue[61];
	plcstring DigestRealm[31];
	plcstring DigestNonce[101];
	plcstring DigestCnonce[101];
	plcstring DigestOpaque[21];
	plcstring DigestQop[21];
	unsigned long DigestNonceCnt;
	plcstring DigestNonceCntStr[11];
	plcstring StringToHash[1001];
	plcstring DigestA1[33];
	plcstring EncodeUrlBuffer[201];
	plcstring DigestA2[33];
	enum RBTemplateRestApiReadFileEnum ReadFileState;
	enum RBTemplateRestApiSaveFileEnum SaveFileState;
	enum RBTemplateRestApiConnectEnum ConnectState;
	enum RBTemplateRestApiConnectEnum StateAfterConnect;
	plcstring FileName[37];
	unsigned long pDstFile;
	plcstring DevLinkParam[73];
	unsigned long DeviceHandle;
} RBTemplateRestApiIntDataType;

typedef struct RBTemplateRestApiIntFubType
{	struct TON TON_0;
	struct httpClient Client_0;
	struct httpEncodeUrl httpEncodeUrl_0;
	struct MTHashMd5 MTHashMd5_0;
	struct DevLink DevLink_0;
	struct DevUnlink DevUnlink_0;
	struct FileDelete FileDelete_0;
	struct FileCreate FileCreate_0;
	struct FileOpen FileOpen_0;
	struct FileRead FileRead_0;
	struct FileWrite FileWrite_0;
	struct FileClose FileClose_0;
} RBTemplateRestApiIntFubType;

typedef struct RBTemplateRestApiInternalType
{	enum RBTemplateRestApiStateEnum State;
	struct RBTemplateRestApiIntCmdType Commands;
	struct RBTemplateRestApiIntStatusType Status;
	struct RBTemplateRestApiIntDataType Data;
	struct RBTemplateRestApiIntFubType FUB;
} RBTemplateRestApiInternalType;

typedef struct RBTemplateRestApi
{
	/* VAR_INPUT (analog) */
	struct RBTemplateRestApiParType Parameters;
	struct RBTemplateRestApiCmdType Commands;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct RBTemplateRestApiInfoType Info;
	/* VAR (analog) */
	struct RBTemplateRestApiInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
	plcbit Connected;
	plcbit UploadFileDone;
	plcbit UploadDataDone;
	plcbit DownloadFileDone;
	plcbit ReadFileDone;
	plcbit ARSimUsed;
} RBTemplateRestApi_typ;

typedef struct RBTemplateLicenseInternalType
{	struct DiagCreateInfo DiagCreateInfo_0;
	struct DiagGetNumInfo DiagGetNumInfo_0;
	struct guardGetDongles guardGetDongles_0;
	struct guardGetLicenses guardGetLicenses_0;
	struct guardReadData guardReadData_0;
	struct MTHashMd5 MTHash_0;
	plcbit Approved;
	plcbit OldErrorReset;
	plcbit Error;
	enum RBTemplateLicenseStateEnum State;
	struct licenseInfo_t Licenses[31];
	struct dongleInfo_t Dongles[6];
	plcstring Number[11];
} RBTemplateLicenseInternalType;

typedef struct RBTemplateLicense
{
	/* VAR_INPUT (analog) */
	unsigned long pLicenseFile;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct MTDiagType Diag;
	/* VAR (analog) */
	struct RBTemplateLicenseInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Done;
	plcbit Error;
	plcbit Approved;
} RBTemplateLicense_typ;

typedef struct RBTemplateLogbookInfoType
{	struct MTDiagType Diag;
} RBTemplateLogbookInfoType;

typedef struct RBTemplateLogbookInternalType
{	struct ArEventLogCreate ArEventLogCreate_0;
	struct ArEventLogGetIdent ArEventLogGetIdent_0;
	struct ArEventLogWrite ArEventLogWrite_0;
	plcbit ErrorResetOld;
	plcbit ExecuteOld;
	ArEventLogIdentType Ident;
	enum RBTemplateLogbookStateEnum StateError;
	enum RBTemplateLogbookStateEnum State;
} RBTemplateLogbookInternalType;

typedef struct RBTemplateLogbook
{
	/* VAR_INPUT (analog) */
	signed long EventID;
	plcstring Data[256];
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct RBTemplateLogbookInfoType Info;
	/* VAR (analog) */
	struct RBTemplateLogbookInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Execute;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Error;
	plcbit Done;
} RBTemplateLogbook_typ;

typedef struct RBTemplateCommTcpIdentMemoryType
{	unsigned long Ident[4];
} RBTemplateCommTcpIdentMemoryType;

typedef struct RBTemplateIdentHandlerInfoType
{	struct MTDiagType Diag;
} RBTemplateIdentHandlerInfoType;

typedef struct RBTemplateIdentHandlerIntType
{	struct DatObjInfo DatObjInfo_0;
	struct DatObjCreate DatObjCreate_0;
	struct DatObjRead DatObjRead_0;
	struct DatObjWrite DatObjWrite_0;
	unsigned long DatObjIdent;
	plcbit OldErrorReset;
	plcbit OldLoad;
	plcbit OldSave;
	enum RBTemplateIdentHandlerStateEnum State;
} RBTemplateIdentHandlerIntType;

typedef struct RBTemplateIdentHandler
{
	/* VAR_INPUT (analog) */
	struct RBTemplateCommTcpIdentMemoryType Ident;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct RBTemplateIdentHandlerInfoType Info;
	/* VAR (analog) */
	struct RBTemplateIdentHandlerIntType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	plcbit Save;
	plcbit Load;
	/* VAR_OUTPUT (digital) */
	plcbit Busy;
	plcbit Error;
	plcbit SaveDone;
	plcbit LoadDone;
} RBTemplateIdentHandler_typ;

typedef struct RBTemplateRobotCtrlIntFUBType
{	struct RBTemplateCommTcp RBCommCmdStat_0;
	struct RBTemplateCommTcp RBCommReqData_0;
	struct RBTemplateCommTcp RBCommUserData_0;
	struct IcmpPing IcmpPing_0;
	struct TON TON_0;
	struct TON TON_1;
	struct TON TON_2;
	struct RBTemplateRestApi RBTemplateRestApi_0;
	struct RBTemplateLicense RBTemplateLicense_0;
	struct RBTemplateLogbook RBTemplateLogbook_0;
	struct RBTemplateIdentHandler RBTemplateIdentHandler_0;
} RBTemplateRobotCtrlIntFUBType;

typedef struct RBTemplateRobotCtrlIntStateType
{	enum RBTemplateRobotCtrlStateEnum LastBeforeError;
	enum RBTemplateRobotCtrlStateEnum Main;
	enum RBTemplateRobotCtrlStateEnum DownloadFileNextState;
} RBTemplateRobotCtrlIntStateType;

typedef struct RBTemplateRobotCtrlRobEdgeType
{	plcbit AutoExecute;
	plcbit ErrorReset;
	plcbit LoadModule;
	plcbit StartProcedure;
	plcbit TransferModuleToController;
} RBTemplateRobotCtrlRobEdgeType;

typedef struct RBTemplateRobotCtrlRobFlagsType
{	plcbit AutoExecuteDone;
	plcbit CheckErrors;
	struct RBTemplateRobotCtrlRobEdgeType Edge;
	plcbit Enable;
	plcbit Error;
	plcbit ErrorReset;
	plcbit LimitSpeed;
	plcbit ProcDone;
	plcbit TransferDone;
} RBTemplateRobotCtrlRobFlagsType;

typedef struct RBTemplateRobotCtrlRobType
{	struct RBTemplateRobotCmdType Cmd;
	plcstring DataBuffer[64001];
	signed long ErrorCode;
	struct RBTemplateRobotCtrlRobFlagsType Flags;
	struct RBTemplateRestApi RBTemplateRestApi_R;
	enum RBTemplateRobotCtrlRobStateEnum State;
	enum RBTemplateRobotCtrlRobAutExeEnum StateAutoExec;
	struct RBTemplateRobotStatusType Status;
	struct TON TON_R;
} RBTemplateRobotCtrlRobType;

typedef struct RBTemplateRobotCtrlIntType
{	struct RBTemplateRobotCtrlIntCommType Comm;
	struct RBTemplateRobotCtrlIntDataType Data;
	struct RBTemplateRobotCtrlIntFlagsType Flags;
	struct RBTemplateRobotCtrlIntFUBType FUB;
	struct RBTemplateRobotCtrlRobType Robot[4];
	unsigned char RobotCount;
	unsigned char RobotIndex;
	struct RBTemplateRobotCtrlIntStateType State;
} RBTemplateRobotCtrlIntType;

typedef struct ToolDecInternalStatusType
{	plcbit EndStringFound;
	plcbit StartStringFound;
	plcbit RobholdFalseFound;
	plcbit RobholdTrueFound;
	plcbit FindOffsetsDone;
	plcbit FindNamesDone;
} ToolDecInternalStatusType;

typedef struct ToolDecInternalFUBType
{	struct FileOpen FileOpen_0;
	struct FileInfo FileInfo_0;
	struct FileRead FileRead_0;
	struct FileClose FileClose_0;
} ToolDecInternalFUBType;

typedef struct ToolDecInternalDataType
{	unsigned char ReadData[300];
	plcstring TemporaryRow[201];
	plcstring Row[201];
	signed short StartStringPosition;
	unsigned long StringPosition;
	plcstring CutRow[201];
	plcstring StringToReal[21];
	plcstring StartString[11];
	plcstring EndString[11];
	unsigned char Increment;
	unsigned long ReadOffset;
	signed short RowNumber;
	unsigned long ToolsOffset[100];
	signed short ToolIndex;
	plcbit EnableOld;
	signed short StringLength;
	signed short FirstStringLength;
	struct fiFILE_INFO FileInfo;
} ToolDecInternalDataType;

typedef struct RBTemplateToolDecInternalType
{	struct ToolDecInternalStatusType Status;
	struct ToolDecInternalFUBType FUB;
	struct ToolDecInternalDataType Data;
	unsigned char State;
} RBTemplateToolDecInternalType;

typedef struct RBTemplateWobjDecStatusType
{	plcbit EndStringFound;
	plcbit StartStringFound;
	plcbit UserFrProgFalseFound;
	plcbit UserFrProgTrueFound;
	plcbit RobholdFalseFound;
	plcbit RobholdTrueFound;
	plcbit CommaFound;
	plcbit FindOffsetsDone;
	plcbit FindNamesDone;
} RBTemplateWobjDecStatusType;

typedef struct RBTemplateWobjDecFUBType
{	struct FileOpen FileOpen_0;
	struct FileInfo FileInfo_0;
	struct FileRead FileRead_0;
	struct FileClose FileClose_0;
} RBTemplateWobjDecFUBType;

typedef struct RBTemplateWobjDecDataType
{	unsigned char ReadData[800];
	plcstring TemporaryRow[201];
	plcstring Row[201];
	signed short StartStringPosition;
	unsigned long StringPosition;
	plcstring HelpString[21];
	plcstring CutRow[201];
	plcstring StringToReal[21];
	plcstring StartString[11];
	plcstring EndString[11];
	unsigned char Increment;
	unsigned long ReadOffset;
	signed short RowNumber;
	unsigned long WorkObjectsOffset[100];
	signed short WorkObjectIndex;
	plcbit EnableOld;
	signed short StringLength;
	signed short FirstStringLength;
	signed short FirstQuotationMarksPosition;
	signed short SecondQuotationMarksPosition;
	plcstring QuotationMarks[6];
	unsigned char QuotationMarksAscii[2];
	struct fiFILE_INFO FileInfo;
} RBTemplateWobjDecDataType;

typedef struct RBTemplateWobjDecInternalType
{	struct RBTemplateWobjDecStatusType Status;
	struct RBTemplateWobjDecFUBType FUB;
	struct RBTemplateWobjDecDataType Data;
	unsigned char State;
} RBTemplateWobjDecInternalType;

typedef struct RBTemplateTargetEditorStatType
{	plcbit EndStringFound;
	plcbit StartStringFound;
	plcbit NameFound;
	plcbit TargetNotExists;
	plcbit LastMove;
} RBTemplateTargetEditorStatType;

typedef struct RBTemplateTargetEditorFUBType
{	struct FileOpen FileOpen_0;
	struct FileInfo FileInfo_0;
	struct FileRead FileRead_0;
	struct FileWrite FileWrite_0;
	struct FileClose FileClose_0;
} RBTemplateTargetEditorFUBType;

typedef struct RBTemplateTargetEditorDatType
{	unsigned char ReadData[300];
	plcstring TemporaryRow[201];
	plcstring Row[201];
	signed short StartStringPosition;
	unsigned long StringPosition;
	plcstring CutRow[201];
	plcstring StringToReal[21];
	plcstring StartString[11];
	plcstring EndString[11];
	unsigned char Increment;
	signed long ReadOffset;
	plcbit EnableOld;
	signed short StringLength;
	signed short FirstStringLength;
	struct fiFILE_INFO FileInfo;
	unsigned long TargetNameLength;
	unsigned long TargetOffset;
	plcstring TargetString[201];
	plcstring RealToString[31];
	unsigned long RowLength;
	unsigned long TargetStringLength;
	signed long NewLineOffset;
	unsigned long LengthOfDataToMove;
	unsigned long LengthOfDataToRead;
	signed long NewOffset;
	unsigned char NewLineAscii[2];
	plcstring NewLineString[11];
} RBTemplateTargetEditorDatType;

typedef struct RBTemplateTargetEditInternalType
{	struct RBTemplateTargetEditorStatType Status;
	struct RBTemplateTargetEditorFUBType FUB;
	struct RBTemplateTargetEditorDatType Data;
	enum RBTemplateTargetEditorStateEnum State;
} RBTemplateTargetEditInternalType;

typedef struct RBTemplateTeachingCmdType
{	plcbit CreateInstruction;
	plcbit EditInstruction;
	plcbit InsertInstruction;
	plcbit RemoveInstruction;
	plcbit CreateFile;
	plcbit ClearList;
} RBTemplateTeachingCmdType;

typedef struct RBTemplateTeachingParInstrType
{	unsigned char Index;
	plcstring Name[33];
	enum RBTemplateABBMoveInstrEnum Type;
	enum RBTemplateABBPredefSpeedEnum PredefinedSpeed;
	float Time;
	float Velocity;
	struct RBTemplateABBSpeeddataType Speed;
	enum RBTemplateABBPredefZoneEnum PredefinedZone;
	struct RBTemplateABBZonedataType Zone;
	enum RBTemplateABBPredefStoppointEnum PredefinedStoppoint;
	struct RBTemplateABBStoppointdataType InPosition;
	struct RBTemplateABBLoaddataType TotalLoad;
} RBTemplateTeachingParInstrType;

typedef struct RBTemplateTeachingParType
{	plcstring Path[65];
	plcstring PathSim[65];
	plcstring FileName[37];
	unsigned char RobotIndex;
	plcbit KeepPositionOnEdit;
	struct RBTemplateABBMoveInstrListType* pInstructionList;
	unsigned long InstructionListLen;
	struct RBTemplateTeachingParInstrType Instruction;
} RBTemplateTeachingParType;

typedef struct RBTemplateTeachingInfoType
{	plcbit ARSimUsed;
	struct RBTemplateRobotCtrlConInfoType Controller;
	unsigned long InstructionsCount;
	struct MTDiagType Diag;
} RBTemplateTeachingInfoType;

typedef struct RBTemplateTeachingIntCmdType
{	plcbit BypassPing;
	plcbit PingRobot;
	plcbit ClearList;
	plcbit ShiftListDownwards;
	plcbit SystemInfoReceived;
	unsigned long RequestData;
	unsigned char MotionDataIndex;
} RBTemplateTeachingIntCmdType;

typedef struct RBTemplateTeachingIntStatusType
{	plcbit Ping;
	plcbit Connected;
	struct RBTemplateTeachingCmdType CommandEdge;
	plcbit MotionDataValid;
	enum RBTemplateTeachingStateEnum ErrorState;
	plcbit ErrorResetOld;
} RBTemplateTeachingIntStatusType;

typedef struct RBTemplateTeachingIntDataType
{	RBTemplateSystemReqDataType RequestData;
	struct RBTemplateSystemInfoType SystemInfo;
	struct RBTemplateRobotMotionDataType MotionData;
	struct RBTemplateTeachingParInstrType InstructionParameters;
	struct RBTemplateABBMoveInstrListType Instruction;
	unsigned long ListInstructionsCount;
	unsigned long LearnedInstructionsCount;
	unsigned char RobotCount;
	plcstring RobotIpAdr[16];
} RBTemplateTeachingIntDataType;

typedef struct RBTemplateModGen
{
	/* VAR_INPUT (analog) */
	unsigned long* pPath;
	unsigned long* pPathSim;
	unsigned long* pFileName;
	struct RBTemplateABBMoveInstrListType* pMoveInstructionList;
	unsigned long MoveInstructionList_len;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct RBTemplateModGenInfoType Info;
	/* VAR (analog) */
	struct RBTemplateModGenInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	plcbit Save;
	plcbit AutoResetCmd;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Error;
	plcbit ARSimUsed;
	plcbit CommandDone;
} RBTemplateModGen_typ;

typedef struct RBTemplateTeachingIntFubType
{	struct TON TON_0;
	struct IcmpPing IcmpPing_0;
	struct RBTemplateCommTcp RBTemplateCommTcp_0;
	struct RBTemplateModGen RBTemplateModGen_0;
	struct RBTemplateIdentHandler RBTemplateIdentHandler_0;
} RBTemplateTeachingIntFubType;

typedef struct RBTemplateTeachingInternalType
{	enum RBTemplateTeachingStateEnum State;
	struct RBTemplateTeachingIntCmdType Commands;
	struct RBTemplateTeachingIntStatusType Status;
	struct RBTemplateTeachingIntDataType Data;
	struct RBTemplateTeachingIntFubType FUB;
} RBTemplateTeachingInternalType;

typedef struct RBTemplateWobjDec
{
	/* VAR_INPUT (analog) */
	unsigned long Device;
	plcstring FileName[51];
	plcstring WorkObjectName[51];
	/* VAR_OUTPUT (analog) */
	enum RBTemplateWobjDecStatusIDEnum StatusID;
	signed short NumberOfWorkObjects;
	plcstring WorkObjectNames[100][51];
	struct RBTemplateABBWobjdataType WorkObjectData;
	/* VAR (analog) */
	struct RBTemplateWobjDecInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ReadWorkObjectNames;
	plcbit ReadWorkObjectParameters;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Error;
} RBTemplateWobjDec_typ;

typedef struct RBTemplateTeaching
{
	/* VAR_INPUT (analog) */
	plcstring ControllerIpAdr[16];
	plcstring ControllerIpAdrSim[16];
	struct RBTemplateTeachingParType Parameters;
	struct RBTemplateTeachingCmdType Commands;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct RBTemplateRobotMotionDataType MotionData;
	struct RBTemplateTeachingInfoType Info;
	/* VAR (analog) */
	struct RBTemplateTeachingInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
} RBTemplateTeaching_typ;

typedef struct RBTemplateToolDec
{
	/* VAR_INPUT (analog) */
	unsigned long Device;
	plcstring FileName[51];
	plcstring ToolName[51];
	/* VAR_OUTPUT (analog) */
	enum RBTemplateToolDecStatusIDEnum StatusID;
	signed short NumberOfTools;
	plcstring ToolNames[100][51];
	struct RBTemplateABBTooldataType ToolData;
	/* VAR (analog) */
	struct RBTemplateToolDecInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ReadToolNames;
	plcbit ReadToolParameters;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Error;
} RBTemplateToolDec_typ;

typedef struct RBTemplateRobotCtrl
{
	/* VAR_INPUT (analog) */
	plcstring ControllerIpAdr[16];
	plcstring ControllerIpAdrSim[16];
	struct RBTemplateRobotCtrlInputCmdType ControllerInput;
	struct RBTemplateRobotCtrlInputRobType RobotInput[4];
	struct RBTemplateUserDataType UserDataToController;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct RBTemplateRobotCtrlStatusType ControllerStatus;
	struct RBTemplateRobotCtrlRobInfoType RobotStatus[4];
	struct RBTemplateUserDataType UserDataFromController;
	struct RBTemplateRobotMotionDataType MotionData[4];
	struct RBTemplateRobotCtrlInfoType Info;
	/* VAR (analog) */
	struct RBTemplateRobotCtrlIntType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
} RBTemplateRobotCtrl_typ;

typedef struct RBTemplateTargetEditor
{
	/* VAR_INPUT (analog) */
	unsigned long Device;
	plcstring FileName[51];
	plcstring TargetName[51];
	struct RBTemplateABBRobtargetType RobTargetData;
	struct RBTemplateABBJointtargetType JointTargetData;
	/* VAR_OUTPUT (analog) */
	enum RBTemplateTargetEditStatusIDEnum StatusID;
	/* VAR (analog) */
	struct RBTemplateTargetEditInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit TargetTypeRob;
	plcbit TargetTypeJoint;
	plcbit LoadParameters;
	plcbit SetParameters;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Error;
} RBTemplateTargetEditor_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void RBTemplateLicense(struct RBTemplateLicense* inst);
_BUR_PUBLIC void RBTemplateWobjDec(struct RBTemplateWobjDec* inst);
_BUR_PUBLIC void RBTemplateRestApi(struct RBTemplateRestApi* inst);
_BUR_PUBLIC void RBTemplateTeaching(struct RBTemplateTeaching* inst);
_BUR_PUBLIC void RBTemplateToolDec(struct RBTemplateToolDec* inst);
_BUR_PUBLIC void RBTemplateCommTcp(struct RBTemplateCommTcp* inst);
_BUR_PUBLIC void RBTemplateRobotCtrl(struct RBTemplateRobotCtrl* inst);
_BUR_PUBLIC void RBTemplateTargetEditor(struct RBTemplateTargetEditor* inst);
_BUR_PUBLIC void RBTemplateModGen(struct RBTemplateModGen* inst);
_BUR_PUBLIC void RBTemplateLogbook(struct RBTemplateLogbook* inst);
_BUR_PUBLIC void RBTemplateIdentHandler(struct RBTemplateIdentHandler* inst);
_BUR_PUBLIC unsigned char RBTemplateAngleToQuater(float Rx, float Ry, float Rz, struct RBTemplateABBOrientType* Rot);
_BUR_PUBLIC unsigned char RBTemplateQuaterToAngle(struct RBTemplateABBOrientType* Rot, float* Rx, float* Ry, float* Rz);
_BUR_PUBLIC unsigned char RBTemplateRestApiResetCmdEdge(struct RBTemplateRestApiCmdType* pCmdEdge, struct RBTemplateRestApiCmdType* pInputCmd);
_BUR_PUBLIC unsigned char RBTemplateRestApiItox(unsigned long Input, unsigned long pStr);
_BUR_PUBLIC unsigned char RBTemplateRestApiFindValue(unsigned long pKey, unsigned long pHeader, unsigned long pValue, unsigned long ValueLen, unsigned long pValueStart, unsigned long pValueEnd);
_BUR_PUBLIC unsigned char RBTemplateGetFileNameFromPath(unsigned long pStr, unsigned long pFileName, unsigned long FileNameLen);
_BUR_PUBLIC unsigned char FillInInstructionData(struct RBTemplateABBMoveInstrListType* Instruction, struct RBTemplateRobotMotionDataType* MotionData, struct RBTemplateTeachingParInstrType* Parameters);
_BUR_PUBLIC unsigned char RBTemplateRemoveModExt(unsigned long FileName);
_BUR_PUBLIC unsigned char RBTemplateRobotCtrlGetCount(struct RBTemplateSystemInfoType* SystemInfo);
_BUR_PUBLIC unsigned char RBTemplateRobotCtrlRobotError(struct RBTemplateRobotCtrl* inst, signed long ErrorCode);
_BUR_PUBLIC unsigned char RBTemplateRobotCtrlRobotEvent(struct RBTemplateLogbook* Logbook, unsigned char RobotIndex, enum MTDiagSeverityEnum Sev, signed long Code, unsigned long pMsg1, unsigned long pMsg2);
_BUR_PUBLIC unsigned char RBTemplateRobotCtrlAddEvent(struct RBTemplateLogbook* Logbook, enum MTDiagSeverityEnum Sev, signed long Code, unsigned long pMsg);
_BUR_PUBLIC unsigned char RBTemplateRobotCtrlSetError(struct RBTemplateRobotCtrl* inst, signed long ErrorCode, signed long InternalCode);
_BUR_PUBLIC plcbit RBTemplateCmpVersion(plcbyte* pVersionPLC, plcbyte* pVersionRobot);
_BUR_PUBLIC unsigned char setZoneInstr(struct RBTemplateABBZonedataType* Zone, unsigned long destAdr);
_BUR_PUBLIC unsigned char setStoppointInstr(struct RBTemplateABBStoppointdataType* Stoppoint, unsigned long destAdr);
_BUR_PUBLIC unsigned char setToolInst(struct RBTemplateABBTooldataType* Tool, unsigned long destAdr);
_BUR_PUBLIC unsigned char setPoseInst(struct RBTemplateABBPoseType* Pose, unsigned long destAdr);
_BUR_PUBLIC unsigned char setWorkObjectInst(struct RBTemplateABBWobjdataType* WorkObject, unsigned long destAdr);
_BUR_PUBLIC unsigned long setREALstr(float ftNum, unsigned long destAdr);
_BUR_PUBLIC unsigned long setINTstr(signed long iNum, unsigned long destAdr);
_BUR_PUBLIC unsigned long setBOOLstr(plcbit bNum, unsigned long destAdr);
_BUR_PUBLIC unsigned char setToPointInstr(struct RBTemplateABBRobtargetType* ToPoint, unsigned long destAdr);
_BUR_PUBLIC unsigned char CheckQs(struct RBTemplateABBOrientType* Rotation);
_BUR_PUBLIC unsigned char settLoaddataInst(struct RBTemplateABBLoaddataType* Loaddata, unsigned long destAdr);
_BUR_PUBLIC unsigned char setPredefStoppointInstr(enum RBTemplateABBPredefStoppointEnum Stoppoint, unsigned long destAdr);
_BUR_PUBLIC unsigned char setSpeedInstr(struct RBTemplateABBSpeeddataType* Speed, unsigned long destAdr);
_BUR_PUBLIC unsigned long setTargetExtaxInstr(struct RBTemplateABBExtjointType* RobTargetExtax, unsigned long destAdr);
_BUR_PUBLIC unsigned long setTargetConfInstr(struct RBTemplateABBRobconfType* RobTargetConf, unsigned long destAdr);
_BUR_PUBLIC unsigned long setOrientInstr(struct RBTemplateABBOrientType* RobTargetOrient, unsigned long destAdr);
_BUR_PUBLIC unsigned long setTransInstr(struct RBTemplateABBPosType* RobTargetTrans, unsigned long destAdr);
_BUR_PUBLIC unsigned long setMoveInstr(struct RBTemplateABBMoveInstrListType* MoveInstruction, unsigned long destAdr);
_BUR_PUBLIC unsigned char RBTemplateLogbookSetError(struct RBTemplateLogbook* inst, signed long ErrorCode, signed long InternalCode);


__asm__(".section \".plc\"");

/* Additional IEC dependencies */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/DataObj/DataObj.var\\\" scope \\\"global\\\"\\n\"");

__asm__(".previous");

#ifdef __cplusplus
};
#endif
#endif /* _RBTEMPLATE_ */

