
TYPE
	RBTemplateLogbookErrorEnum : 
		( (*45370-45380*)
		rbTEMPLATE_LOGBOOK_ERR_ := 45370,
		rbTEMPLATE_LOGBOOK_ERR_GET_IDENT := 45371,
		rbTEMPLATE_LOGBOOK_ERR_CREATE := 45372,
		rbTEMPLATE_LOGBOOK_ERR_WRITE := 45373
		);
	RBTemplateLogbookStateEnum : 
		(
		rbTEMPLATE_LOGBOOK_STATE_WAIT := 0,
		rbTEMPLATE_LOGBOOK_STATE_IDENT := 1,
		rbTEMPLATE_LOGBOOK_STATE_CREATE := 2,
		rbTEMPLATE_LOGBOOK_STATE_WRITE := 3,
		rbTEMPLATE_LOGBOOK_STATE_DONE := 4,
		rbTEMPLATE_LOGBOOK_STATE_ERROR := 5
		);
	RBTemplateLogbookInfoType : 	STRUCT 
		Diag : MTDiagType;
	END_STRUCT;
	RBTemplateLogbookInternalType : 	STRUCT 
		ArEventLogCreate_0 : ArEventLogCreate;
		ArEventLogGetIdent_0 : ArEventLogGetIdent;
		ArEventLogWrite_0 : ArEventLogWrite;
		ErrorResetOld : BOOL;
		ExecuteOld : BOOL;
		Ident : ArEventLogIdentType;
		StateError : RBTemplateLogbookStateEnum; (*Laste state before error has occured*)
		State : RBTemplateLogbookStateEnum;
	END_STRUCT;
END_TYPE
