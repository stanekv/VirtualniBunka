
TYPE
	RBTemplateTargetEditInternalType : 	STRUCT 
		Status : RBTemplateTargetEditorStatType;
		FUB : RBTemplateTargetEditorFUBType;
		Data : RBTemplateTargetEditorDatType;
		State : RBTemplateTargetEditorStateEnum;
	END_STRUCT;
	RBTemplateTargetEditorDatType : 	STRUCT 
		ReadData : ARRAY[0..299]OF USINT;
		TemporaryRow : STRING[200];
		Row : STRING[200];
		StartStringPosition : INT;
		StringPosition : UDINT;
		CutRow : STRING[200];
		StringToReal : STRING[20];
		StartString : STRING[10];
		EndString : STRING[10];
		Increment : USINT;
		ReadOffset : DINT;
		EnableOld : BOOL;
		StringLength : INT;
		FirstStringLength : INT;
		FileInfo : fiFILE_INFO;
		TargetNameLength : UDINT;
		TargetOffset : UDINT;
		TargetString : STRING[200];
		RealToString : STRING[30];
		RowLength : UDINT;
		TargetStringLength : UDINT;
		NewLineOffset : DINT;
		LengthOfDataToMove : UDINT;
		LengthOfDataToRead : UDINT;
		NewOffset : DINT;
		NewLineAscii : ARRAY[0..1]OF USINT;
		NewLineString : STRING[10];
	END_STRUCT;
	RBTemplateTargetEditorStatType : 	STRUCT 
		EndStringFound : BOOL;
		StartStringFound : BOOL;
		NameFound : BOOL;
		TargetNotExists : BOOL;
		LastMove : BOOL;
	END_STRUCT;
	RBTemplateTargetEditorFUBType : 	STRUCT 
		FileOpen_0 : FileOpen;
		FileInfo_0 : FileInfo;
		FileRead_0 : FileRead;
		FileWrite_0 : FileWrite;
		FileClose_0 : FileClose;
	END_STRUCT;
	RBTemplateTargetEditorStateEnum : 
		(
		rbtTARGET_EDIT_DISABLED := 0, (*Function block is not enabled*)
		rbtTARGET_EDIT_WAIT_FOR_ENABLED := 10, (*Waiting for function block enable*)
		rbtTARGET_EDIT_WAIT_FOR_CMD := 20, (*Waiting for command*)
		rbtTARGET_EDIT_OPEN_FILE := 30, (*Open file with name in FileName*)
		rbtTARGET_EDIT_GET_FILE_INFO := 40, (*Get info of FileName*)
		rbtTARGET_EDIT_READ_DATA := 50, (*Read data to find target*)
		rbtTARGET_EDIT_FIND_NAME := 60, (*Find name of target*)
		rbtTARGET_EDIT_SEPARATE_ROW := 70, (*Separate row from readed data*)
		rbtTARGET_EDIT_FIND_PARAMS := 80, (*Find parameters of target*)
		rbtTARGET_EDIT_FIND_VALUE := 90, (*Find value between StartString and EndString*)
		rbtTARGET_EDIT_MAKE_TARGET_STR := 100, (*Make string of target declaration, which should be written into the file*)
		rbtTARGET_EDIT_FIND_END_OF_ROW := 110, (*Finds semicolon at the end of the row*)
		rbtTARGET_EDIT_WRITE_TARGET := 120, (*Wriite target string to selected file*)
		rbtTARGET_EDIT_FIND_NEWLINE := 130, (*Finds newline tag from the beggining of the file*)
		rbtTARGET_EDIT_READ_DATA_FOR_INS := 140, (*Reads data, which should be moved to make empty place for insert new string*)
		rbtTARGET_EDIT_MOVE_DATA := 150, (*Moves readed data by size of string to insert*)
		rbtTARGET_EDIT_INSERT_DATA := 160, (*Inserts string behind the newline tag*)
		rbtTARGET_EDIT_CLOSE_FILE := 170, (*Closes file*)
		rbtTARGET_EDIT_ERROR := 255 (*Error step*)
		);
	RBTemplateTargetEditStatusIDEnum : 
		( (*Type of information / Description / Suggestion*)
		rbtTARGET_EDIT_FUB_OK := 0, (*State / OK*)
		rbtTARGET_EDIT_FUB_BUSY := 65535, (*State / Busy*)
		rbtTARGET_EDIT_ERR_TAR_TYPE := 45150, (*Error TargetEditor lib / Bad target type settings / The target type must be defined and its not allowed to be both true*)
		rbtTARGET_EDIT_ERR_NO_CMD := 45151, (*Error TargetEditor lib / No command detected / Dont change commands during busy*)
		rbtTARGET_EDIT_ERR_TAR_NOT_FOUND := 45152, (*Error TargetEditor lib / Entered target name not found / Check correctness of entered target name*)
		rbtTARGET_EDIT_ERR_NO_ROW_END := 45153, (*Error TargetEditor lib / Row doesnt have semicolon / Check the semicolons at the end of the rows*)
		rbtTARGET_EDIT_ERR_PARAMS_SNTX := 45154, (*Error TargetEditor lib / Bad parameters syntax / Check syntax of parameters*)
		rbtTARGET_EDIT_ERR_NEWLINE := 45155, (*Error TargetEditor lib / Newline tag not found / Make sure there is an enter in the file*)
		rbtTARGET_EDIT_ERR_NO_TAR_NAME := 45156 (*Error TargetEditor lib / Target name not entered / Enter target name*)
		);
END_TYPE
