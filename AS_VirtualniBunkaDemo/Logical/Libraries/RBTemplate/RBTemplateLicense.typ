
TYPE
	RBTemplateLicenseErrorEnum : 
		( (*45350-45369*)
		rbTEMPLATE_LIC_ERR_INV_PARAM := 45350, (*Invalid FB parameters*)
		rbTEMPLATE_LIC_ERR_DONG := 45351, (*Error reading dongle*)
		rbTEMPLATE_LIC_ERR_GET_INFO := 45352 (*Error reading PLC info*)
		);
	RBTemplateLicenseStateEnum : 
		(
		rbTEMPLATE_LIC_STATE_INIT, (*Init state, wait for enable*)
		rbTEMPLATE_LIC_STATE_DONG_INFO, (*Read dongle information*)
		rbTEMPLATE_LIC_STATE_DONG_READ, (*Read data from dongle*)
		rbTEMPLATE_LIC_STATE_DONG_GET, (*Deprecated. Read licenses from dongle*)
		rbTEMPLATE_LIC_STATE_CREATE_INFO, (*Deprecated. Read PLC info*)
		rbTEMPLATE_LIC_STATE_GET_INFO, (*Deprecated. Read PLC info*)
		rbTEMPLATE_LIC_STATE_CHECK, (*Check license*)
		rbTEMPLATE_LIC_STATE_DONE, (*Done state*)
		rbTEMPLATE_LIC_STATE_ERROR (*Error handling*)
		);
	RBTemplateLicenseInternalType : 	STRUCT 
		DiagCreateInfo_0 : DiagCreateInfo; (*Deprecated.*)
		DiagGetNumInfo_0 : DiagGetNumInfo; (*Deprecated.*)
		guardGetDongles_0 : guardGetDongles;
		guardGetLicenses_0 : guardGetLicenses;
		guardReadData_0 : guardReadData;
		MTHash_0 : MTHashMd5;
		Approved : BOOL;
		OldErrorReset : BOOL;
		Error : BOOL;
		State : RBTemplateLicenseStateEnum;
		Licenses : ARRAY[0..rbTEMPLATE_LIC_VAR_INFO_ARR_SIZE]OF licenseInfo_t;
		Dongles : ARRAY[0..rbTEMPLATE_LIC_VAR_DONG_ARR_SIZE]OF dongleInfo_t;
		Number : STRING[10];
	END_STRUCT;
END_TYPE
