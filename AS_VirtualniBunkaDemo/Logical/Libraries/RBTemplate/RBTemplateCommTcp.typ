(*Enumeration*)

TYPE
	RBTemplateCommTcpStateEnum : 
		( (*FUB states*)
		rbTEMPLATE_COMM_TCP_NOT_ENABLED := 0,
		rbTEMPLATE_COMM_TCP_OPEN := 10,
		rbTEMPLATE_COMM_TCP_SOCK_PARAM := 20,
		rbTEMPLATE_COMM_TCP_CONNECT := 30,
		rbTEMPLATE_COMM_TCP_MODE := 40,
		rbTEMPLATE_COMM_TCP_RECV := 50,
		rbTEMPLATE_COMM_TCP_SEND := 60,
		rbTEMPLATE_COMM_TCP_CLOSE := 70,
		rbTEMPLATE_COMM_TCP_ERROR := 80
		);
	RBTemplateCommTcpErrorEnum : 
		( (*Error codes*)
		rbTEMPLATE_COMM_TCP_ERR_INV_DATA := 45209, (*Invalid pointer to data structure or zero data length specified*)
		rbTEMPLATE_COMM_TCP_ERR_INV_IDEN := 45210, (*Invalid pointer to ident variable*)
		rbTEMPLATE_COMM_TCP_ERR_OPEN := 45211, (*Could not open socket*)
		rbTEMPLATE_COMM_TCP_ERR_PARAM := 45212,
		rbTEMPLATE_COMM_TCP_ERR_CLOSE := 45213, (*Could not close socket*)
		rbTEMPLATE_COMM_TCP_ERR_CONNECT := 45214,
		rbTEMPLATE_COMM_TCP_ERR_RECV := 45215,
		rbTEMPLATE_COMM_TCP_ERR_SEND := 45216,
		rbTEMPLATE_COMM_TCP_ERR_TOUT := 45217 (*Receive timeout timer has elapsed*)
		);
END_TYPE

(*Structure*)

TYPE
	RBTemplateCommTcpParType : 	STRUCT 
		pRobotIpAdr : REFERENCE TO UDINT;
		PlcPort : UINT;
		RobotPort : UINT;
		RecvDataLen : UDINT; (*Length of data to receive*)
		pRecvData : REFERENCE TO UDINT; (*Pointer to data structure to receive*)
		SendDataLen : UDINT; (*Length of data to send*)
		pSendData : REFERENCE TO UDINT; (*Pointer to data structure to send*)
		Timeout : UDINT; (*Maximum wait time (ms) for new data*)
		pIdent : REFERENCE TO UDINT; (*Reuse ident*)
	END_STRUCT;
	RBTemplateCommTcpInfoType : 	STRUCT 
		Diag : MTDiagType;
		RecvDataCnt : UDINT; (*Count of receive data cycles since FUB was enabled*)
		SentDataCnt : UDINT; (*Count of send data cycles since FUB was enabled*)
		TimeDiff : UDINT; (*Period (ms) of the last request-response cycle*)
	END_STRUCT;
	RBTemplateCommTcpInternalFubType : 	STRUCT 
		TcpOpen_0 : TcpOpen;
		TcpIoctl_0 : TcpIoctl;
		TcpClient_0 : TcpClient;
		TcpRecv_0 : TcpRecv;
		TcpSend_0 : TcpSend;
		TcpClose_0 : TcpClose;
		TON_0 : TON;
	END_STRUCT;
	RBTemplateCommTcpInternalType : 	STRUCT 
		State : RBTemplateCommTcpStateEnum;
		RobotIpAdr : STRING[15]; (*Internal copy of pRobotIpAdr param value*)
		SocketOpen : BOOL; (*Socket is open if TRUE*)
		Connected : BOOL;
		InitSendDone : BOOL;
		SocketIdent : UDINT;
		SocketLinger : tcpLINGER_typ;
		InitCloseDone : BOOL;
		FUB : RBTemplateCommTcpInternalFubType;
		ErrorResetOld : BOOL; (*Holds ErrorReset input value from previous cycle*)
		ErrorState : RBTemplateCommTcpStateEnum; (*State in which last error has occured*)
	END_STRUCT;
END_TYPE

(*Ident Memory*)

TYPE
	RBTemplateCommTcpIdentMemoryType : 	STRUCT 
		Ident : ARRAY[0..3]OF UDINT;
	END_STRUCT;
END_TYPE
