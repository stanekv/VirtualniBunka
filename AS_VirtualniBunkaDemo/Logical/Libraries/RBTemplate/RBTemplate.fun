
FUNCTION_BLOCK RBTemplateLicense (*This function block checks if given PLC has valid licence to use RBTemplate library*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*) (*#OMIT*)
	VAR_INPUT
		Enable : BOOL;
		ErrorReset : BOOL;
		pLicenseFile : UDINT;
	END_VAR
	VAR_OUTPUT
		Active : BOOL;
		Busy : BOOL;
		Done : BOOL;
		Error : BOOL;
		Approved : BOOL;
		StatusID : DINT;
		Diag : MTDiagType;
	END_VAR
	VAR
		Internal : RBTemplateLicenseInternalType;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RBTemplateWobjDec
	VAR_INPUT
		Enable : BOOL; (*Enables FB*) (* *) (*#CMD#;*)
		Device : UDINT; (*Name of file device*) (* *) (*#PAR#*)
		FileName : STRING[50]; (*Name of file, which contains work objects data*) (* *) (*#PAR#*)
		WorkObjectName : STRING[50]; (*Name of work object which parameters want to read*) (* *) (*#PAR#*)
		ReadWorkObjectNames : BOOL; (*Command to read work objects name*) (* *) (*#CMD#;*)
		ReadWorkObjectParameters : BOOL; (*Command to read parameters of work object with name in WorkObjectName*) (* *) (*#CMD#;*)
		ErrorReset : BOOL; (*Resets function block errors*) (* *) (*#CMD#;*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#CMD#;*)
		Busy : BOOL; (*Function block is busy*) (* *) (*#CMD#;*)
		Error : BOOL; (*Indicates error*) (* *) (*#CMD#;*)
		StatusID : RBTemplateWobjDecStatusIDEnum; (*Error/Status information*) (* *) (*#CMD#;*)
		NumberOfWorkObjects : INT; (*Number of work objects*) (* *) (*#PAR#*)
		WorkObjectNames : ARRAY[0..rbTEMPLATE_MAX_WOBJS_MIN_1] OF STRING[50]; (*Array filled with work object names*) (* *) (*#PAR#*)
		WorkObjectData : RBTemplateABBWobjdataType; (*Work object data*) (* *) (*#PAR#*)
	END_VAR
	VAR
		Internal : RBTemplateWobjDecInternalType;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION RBTemplateAngleToQuater : USINT (*Transforms angles to quaternions*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Rx : REAL; (*Angle in deg*) (* *) (*#PAR*)
		Ry : REAL; (*Angle in deg*) (* *) (*#PAR*)
		Rz : REAL; (*Angle in deg*) (* *) (*#PAR*)
		Rot : RBTemplateABBOrientType; (*Quaternions*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION RBTemplateQuaterToAngle : USINT (*Transforms quaternions to angles*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Rot : RBTemplateABBOrientType; (*Quaternions*) (* *) (*#PAR*)
		Rx : REFERENCE TO REAL; (*Angle in deg*) (* *) (*#PAR*)
		Ry : REFERENCE TO REAL; (*Angle in deg*) (* *) (*#PAR*)
		Rz : REFERENCE TO REAL; (*Angle in deg*) (* *) (*#PAR*)
	END_VAR
END_FUNCTION

FUNCTION RBTemplateRestApiResetCmdEdge : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		pCmdEdge : RBTemplateRestApiCmdType;
		pInputCmd : RBTemplateRestApiCmdType;
	END_VAR
END_FUNCTION

FUNCTION RBTemplateRestApiItox : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Input : UDINT;
		pStr : UDINT;
	END_VAR
END_FUNCTION

FUNCTION RBTemplateRestApiFindValue : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		pKey : UDINT;
		pHeader : UDINT;
		pValue : UDINT;
		ValueLen : UDINT;
		pValueStart : UDINT;
		pValueEnd : UDINT;
	END_VAR
END_FUNCTION

FUNCTION RBTemplateGetFileNameFromPath : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		pStr : UDINT;
		pFileName : UDINT;
		FileNameLen : UDINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK RBTemplateRestApi
	VAR_INPUT
		Enable : BOOL; (*Enables FUB*) (* *) (*#PAR*)
		ErrorReset : BOOL; (*Allows to reset an error. Edge sensitive.*) (* *) (*#PAR*)
		Parameters : RBTemplateRestApiParType; (* *) (* *) (*#PAR*)
		Commands : RBTemplateRestApiCmdType; (* *) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*TRUE if communication is running and FUB is ready for Commands*) (* *) (*#PAR*)
		Error : BOOL; (*An error has occured*) (* *) (*#PAR*)
		StatusID : DINT; (*StatusID generated from error code*) (* *) (*#PAR*)
		Connected : BOOL; (* *) (* *) (*#PAR*)
		UploadFileDone : BOOL; (* *) (* *) (*#PAR*)
		UploadDataDone : BOOL; (* *) (* *) (*#PAR*)
		DownloadFileDone : BOOL; (* *) (* *) (*#PAR*)
		ReadFileDone : BOOL; (* *) (* *) (*#PAR*)
		ARSimUsed : BOOL; (*PLC is in simulation (TRUE)*) (* *) (*#CMD*)
		Info : RBTemplateRestApiInfoType; (* *) (* *) (*#CMD*)
	END_VAR
	VAR
		Internal : RBTemplateRestApiInternalType; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION FillInInstructionData : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Instruction : RBTemplateABBMoveInstrListType; (*Pointer to instruction in instruction list*)
		MotionData : RBTemplateRobotMotionDataType; (*Motion data from robot*)
		Parameters : RBTemplateTeachingParInstrType; (*Specific instruction parameters*)
	END_VAR
END_FUNCTION

FUNCTION_BLOCK RBTemplateTeaching (*Allows to create procedures by creating instruction list from manually selected points.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables FUB*) (* *) (*#PAR*)
		ErrorReset : BOOL; (*Allows to reset an error. Edge sensitive.*) (* *) (*#PAR*)
		ControllerIpAdr : STRING[15]; (*Robot IP address*) (* *) (*#PAR*)
		ControllerIpAdrSim : STRING[15]; (*Robot IP address*) (* *) (*#PAR*)
		Parameters : RBTemplateTeachingParType; (*FUB and Commands parameters*) (* *) (*#PAR*)
		Commands : RBTemplateTeachingCmdType; (* *) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*TRUE if communication is running and FUB is ready for Commands*) (* *) (*#PAR*)
		Error : BOOL; (*An error has occured*) (* *) (*#PAR*)
		StatusID : DINT; (*StatusID generated from error code*) (* *) (*#PAR*)
		MotionData : RBTemplateRobotMotionDataType; (*Selected robot motion data*) (* *) (*#CMD*)
		Info : RBTemplateTeachingInfoType; (* *) (* *) (*#CMD*)
	END_VAR
	VAR
		Internal : RBTemplateTeachingInternalType; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION RBTemplateRemoveModExt : USINT (*Checks module file name given by pointer. If name contains .mod extension it is removed*) (* *) (*#OMIT*)
	VAR_INPUT
		FileName : UDINT; (*Pointer to a module file name string*)
	END_VAR
END_FUNCTION

FUNCTION_BLOCK RBTemplateToolDec (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Enable : BOOL; (*Enables FB*) (* *) (*#CMD#;*)
		Device : UDINT; (*Name of file device*) (* *) (*#PAR#*)
		FileName : STRING[50]; (*Name of file, which contains tools data*) (* *) (*#PAR#*)
		ToolName : STRING[50]; (*Name of tool which parameters want to read*) (* *) (*#PAR#*)
		ReadToolNames : BOOL; (*Command to read tools name*) (* *) (*#CMD#;*)
		ReadToolParameters : BOOL; (*Command to read parameters of tool with name in ToolName*) (* *) (*#CMD#;*)
		ErrorReset : BOOL; (*Resets function block errors*) (* *) (*#CMD#;*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#CMD#;*)
		Busy : BOOL; (*Function block is busy*) (* *) (*#CMD#;*)
		Error : BOOL; (*Indicates error*) (* *) (*#CMD#;*)
		StatusID : RBTemplateToolDecStatusIDEnum; (*Error/Status information*) (* *) (*#CMD#;*)
		NumberOfTools : INT; (*Number of tools*) (* *) (*#PAR#*)
		ToolNames : ARRAY[0..rbTEMPLATE_MAX_TOOLS_MIN_1] OF STRING[50]; (*Array filled with tool names*) (* *) (*#PAR#*)
		ToolData : RBTemplateABBTooldataType; (*Tool data*) (* *) (*#PAR#*)
	END_VAR
	VAR
		Internal : RBTemplateToolDecInternalType;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RBTemplateCommTcp (*Communication with ABB Robot over TCP/IP sockets.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (* *) (* *) (*#PAR*)
		ErrorReset : BOOL; (* *) (* *) (*#PAR*)
		Parameters : RBTemplateCommTcpParType; (* *) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#PAR*)
		Error : BOOL; (*Indicates error*) (* *) (*#PAR*)
		StatusID : DINT; (*Error/Status information*) (* *) (*#PAR*)
		Info : RBTemplateCommTcpInfoType; (* *) (* *) (*#CMD*)
	END_VAR
	VAR
		Internal : RBTemplateCommTcpInternalType;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION RBTemplateRobotCtrlGetCount : USINT (*Get robot count from RobotInfo*) (* *) (*#OMIT*)
	VAR_INPUT
		SystemInfo : RBTemplateSystemInfoType;
	END_VAR
END_FUNCTION

FUNCTION RBTemplateRobotCtrlRobotError : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		inst : RBTemplateRobotCtrl;
		ErrorCode : DINT;
	END_VAR
END_FUNCTION

FUNCTION RBTemplateRobotCtrlRobotEvent : USINT (*Insert event to logbook (robot)*) (* *) (*#OMIT*)
	VAR_INPUT
		Logbook : RBTemplateLogbook;
		RobotIndex : USINT;
		Sev : MTDiagSeverityEnum;
		Code : DINT;
		pMsg1 : UDINT;
		pMsg2 : UDINT;
	END_VAR
END_FUNCTION

FUNCTION RBTemplateRobotCtrlAddEvent : USINT (*Insert event to logbook (controller, general)*) (* *) (*#OMIT*)
	VAR_INPUT
		Logbook : RBTemplateLogbook;
		Sev : MTDiagSeverityEnum;
		Code : DINT;
		pMsg : UDINT;
	END_VAR
END_FUNCTION

FUNCTION RBTemplateRobotCtrlSetError : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		inst : RBTemplateRobotCtrl;
		ErrorCode : DINT;
		InternalCode : DINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK RBTemplateRobotCtrl
	VAR_INPUT
		Enable : BOOL; (*Enables the FBK.*) (* *) (*#PAR*)
		ErrorReset : BOOL; (*Reset all function block error*) (* *) (*#PAR*)
		ControllerIpAdr : STRING[15]; (*Robot IP address*) (* *) (*#PAR*)
		ControllerIpAdrSim : STRING[15]; (*Robot IP address*) (* *) (*#PAR*)
		ControllerInput : RBTemplateRobotCtrlInputCmdType; (*Controller commands*) (* *) (*#CMD*)
		RobotInput : ARRAY[0..3] OF RBTemplateRobotCtrlInputRobType; (*Robot control*) (* *) (*#CMD*)
		UserDataToController : RBTemplateUserDataType; (*User data from PLC to robot*) (* *) (*#CMD*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Ready to control*) (* *) (*#PAR*)
		Error : BOOL; (* *) (* *) (*#PAR*)
		StatusID : DINT; (*Error/Status information*) (* *) (*#PAR*)
		ControllerStatus : RBTemplateRobotCtrlStatusType; (* *) (* *) (*#CMD*)
		RobotStatus : ARRAY[0..3] OF RBTemplateRobotCtrlRobInfoType; (* *) (* *) (*#CMD*)
		UserDataFromController : RBTemplateUserDataType; (*User data from robot to PLC*) (* *) (*#CMD*)
		MotionData : ARRAY[0..3] OF RBTemplateRobotMotionDataType; (* *) (* *) (*#CMD*)
		Info : RBTemplateRobotCtrlInfoType; (*Structure with information*) (* *) (*#CMD*)
	END_VAR
	VAR
		Internal : RBTemplateRobotCtrlIntType; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RBTemplateTargetEditor
	VAR_INPUT
		Enable : BOOL; (*Enables FB*) (* *) (*#CMD#;*)
		Device : UDINT; (*Name of file device*) (* *) (*#PAR#*)
		FileName : STRING[50]; (*Name of file, which contains targets data*) (* *) (*#PAR#*)
		TargetName : STRING[50]; (*Name of target which parameters want to edit*) (* *) (*#PAR#*)
		TargetTypeRob : BOOL; (*Target is robtarget*) (* *) (*#PAR#*)
		TargetTypeJoint : BOOL; (*Target is jointtarget*) (* *) (*#PAR#*)
		LoadParameters : BOOL; (*Command to load parameters of selected target*) (* *) (*#CMD#;*)
		SetParameters : BOOL; (*Command to set new parameters of selected target*) (* *) (*#CMD#;*)
		ErrorReset : BOOL; (*Resets function block errors*) (* *) (*#CMD#;*)
		RobTargetData : RBTemplateABBRobtargetType; (*Robtarget data structure*) (* *) (*#PAR#*)
		JointTargetData : RBTemplateABBJointtargetType; (*Jointtarget data structure*) (* *) (*#PAR#*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#CMD#;*)
		Busy : BOOL; (*Function block is busy*) (* *) (*#CMD#;*)
		Error : BOOL; (*Indicates error*) (* *) (*#CMD#;*)
		StatusID : RBTemplateTargetEditStatusIDEnum; (*Error/Status information*) (* *) (*#CMD#;*)
	END_VAR
	VAR
		Internal : RBTemplateTargetEditInternalType;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION RBTemplateCmpVersion : BOOL (*Checks if PLC and robot RT versions match*) (* *) (*#OMIT*)
	VAR_INPUT
		pVersionPLC : REFERENCE TO BYTE; (*Pointer to a string with PLC RT version*)
		pVersionRobot : REFERENCE TO BYTE; (*Pointer to a string with robot RT version*)
	END_VAR
END_FUNCTION

FUNCTION setZoneInstr : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Zone : RBTemplateABBZonedataType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setStoppointInstr : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Stoppoint : RBTemplateABBStoppointdataType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setToolInst : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Tool : RBTemplateABBTooldataType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setPoseInst : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Pose : RBTemplateABBPoseType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setWorkObjectInst : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		WorkObject : RBTemplateABBWobjdataType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setREALstr : UDINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		ftNum : REAL;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setINTstr : UDINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		iNum : DINT;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setBOOLstr : UDINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		bNum : BOOL;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setToPointInstr : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		ToPoint : RBTemplateABBRobtargetType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION CheckQs : USINT (*Returns sum of q1...q4. Used to check if given instruction was learned*) (* *) (*#OMIT*)
	VAR_INPUT
		Rotation : RBTemplateABBOrientType;
	END_VAR
END_FUNCTION

FUNCTION settLoaddataInst : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Loaddata : RBTemplateABBLoaddataType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setPredefStoppointInstr : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Stoppoint : RBTemplateABBPredefStoppointEnum;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setSpeedInstr : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		Speed : RBTemplateABBSpeeddataType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setTargetExtaxInstr : UDINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		RobTargetExtax : RBTemplateABBExtjointType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setTargetConfInstr : UDINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		RobTargetConf : RBTemplateABBRobconfType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setOrientInstr : UDINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		RobTargetOrient : RBTemplateABBOrientType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setTransInstr : UDINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		RobTargetTrans : RBTemplateABBPosType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION setMoveInstr : UDINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		MoveInstruction : RBTemplateABBMoveInstrListType;
		destAdr : UDINT; (*pointer to destination string*)
	END_VAR
END_FUNCTION

FUNCTION_BLOCK RBTemplateModGen (*FBK parse InstructionList into defined ABB Rapid *.mod file*)
	VAR_INPUT
		Enable : BOOL; (*Enables the function block*) (* *) (*#PAR*)
		ErrorReset : BOOL; (*Resets function block errors*) (* *) (*#PAR*)
		pPath : REFERENCE TO UDINT; (*Pointer to the path*) (* *) (*#PAR*)
		pPathSim : REFERENCE TO UDINT; (*Pointer to simulation path*) (* *) (*#PAR*)
		pFileName : REFERENCE TO UDINT; (*Pointer to generated file name*) (* *) (*#PAR*)
		pMoveInstructionList : REFERENCE TO RBTemplateABBMoveInstrListType; (*Pointer to MoveInstructionList Array*) (* *) (*#PAR*)
		MoveInstructionList_len : UDINT; (*Size of MoveInstructionList Array*) (* *) (*#PAR*)
		Save : BOOL; (*Saves the current MoveInstructionList to a defined file name, in ABB Rapid *.Mod format*) (* *) (*#PAR*)
		AutoResetCmd : BOOL; (*Turn on autoreset commands*) (* *) (*#PAR*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#PAR*)
		Busy : BOOL; (*Function block is busy*) (* *) (*#PAR*)
		Error : BOOL; (*Indicates error*) (* *) (*#PAR*)
		StatusID : DINT; (*Error/Status information*) (* *) (*#PAR*)
		ARSimUsed : BOOL; (*AR simulation is used*)
		CommandDone : BOOL; (*Command successfully executed by function block*)
		Info : RBTemplateModGenInfoType; (*Structure with information*) (* *) (*#CMD*)
	END_VAR
	VAR
		Internal : RBTemplateModGenInternalType; (*Internal data type*) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION RBTemplateLogbookSetError : USINT (* *) (* *) (*#OMIT*)
	VAR_INPUT
		inst : RBTemplateLogbook;
		ErrorCode : DINT;
		InternalCode : DINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK RBTemplateLogbook (*Allows to create entries in logbook*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*) (*#OMIT*)
	VAR_INPUT
		Execute : BOOL;
		ErrorReset : BOOL;
		EventID : DINT;
		Data : STRING[rbTEMPLATE_LOGBOOK_VAR_DATA_LEN];
	END_VAR
	VAR_OUTPUT
		Error : BOOL;
		Done : BOOL;
		StatusID : DINT; (*StatusID generated from error code*) (* *) (*#PAR*)
		Info : RBTemplateLogbookInfoType; (* *) (* *) (*#CMD*)
	END_VAR
	VAR
		Internal : RBTemplateLogbookInternalType; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK RBTemplateIdentHandler
	VAR_INPUT
		Enable : BOOL;
		ErrorReset : BOOL;
		Save : BOOL;
		Load : BOOL;
		Ident : RBTemplateCommTcpIdentMemoryType;
	END_VAR
	VAR_OUTPUT
		Busy : BOOL;
		Error : BOOL;
		SaveDone : BOOL;
		LoadDone : BOOL;
		StatusID : DINT; (*StatusID generated from error code*) (* *) (*#PAR*)
		Info : RBTemplateIdentHandlerInfoType; (* *) (* *) (*#CMD*)
	END_VAR
	VAR
		Internal : RBTemplateIdentHandlerIntType; (* *) (* *) (*#OMIT*)
	END_VAR
END_FUNCTION_BLOCK
