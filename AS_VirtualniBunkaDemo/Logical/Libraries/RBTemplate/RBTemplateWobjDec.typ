
TYPE
	RBTemplateWobjDecInternalType : 	STRUCT 
		Status : RBTemplateWobjDecStatusType;
		FUB : RBTemplateWobjDecFUBType;
		Data : RBTemplateWobjDecDataType;
		State : USINT;
	END_STRUCT;
	RBTemplateWobjDecDataType : 	STRUCT 
		ReadData : ARRAY[0..799]OF USINT;
		TemporaryRow : STRING[200];
		Row : STRING[200];
		StartStringPosition : INT;
		StringPosition : UDINT;
		HelpString : STRING[20];
		CutRow : STRING[200];
		StringToReal : STRING[20];
		StartString : STRING[10];
		EndString : STRING[10];
		Increment : USINT;
		ReadOffset : UDINT;
		RowNumber : INT;
		WorkObjectsOffset : ARRAY[0..rbTEMPLATE_MAX_WOBJS_MIN_1]OF UDINT;
		WorkObjectIndex : INT;
		EnableOld : BOOL;
		StringLength : INT;
		FirstStringLength : INT;
		FirstQuotationMarksPosition : INT;
		SecondQuotationMarksPosition : INT;
		QuotationMarks : STRING[5];
		QuotationMarksAscii : ARRAY[0..1]OF USINT;
		FileInfo : fiFILE_INFO;
	END_STRUCT;
	RBTemplateWobjDecStatusType : 	STRUCT 
		EndStringFound : BOOL;
		StartStringFound : BOOL;
		UserFrProgFalseFound : BOOL;
		UserFrProgTrueFound : BOOL;
		RobholdFalseFound : BOOL;
		RobholdTrueFound : BOOL;
		CommaFound : BOOL;
		FindOffsetsDone : BOOL;
		FindNamesDone : BOOL;
	END_STRUCT;
	RBTemplateWobjDecFUBType : 	STRUCT 
		FileOpen_0 : FileOpen;
		FileInfo_0 : FileInfo;
		FileRead_0 : FileRead;
		FileClose_0 : FileClose;
	END_STRUCT;
	RBTemplateWobjDecStateEnum : 
		(
		rbtWOBJ_DEC_DISABLED := 0, (*Function block is not enabled*)
		rbtWOBJ_DEC_WAIT_FOR_ENABLED := 10, (*Waiting for function block enable*)
		rbtWOBJ_DEC_WAIT_FOR_CMD := 20, (*Waiting for command*)
		rbtWOBJ_DEC_OPEN_FILE := 30, (*Open file with name in FileName*)
		rbtWOBJ_DEC_GET_FILE_INFO := 35, (*Get info of FileName*)
		rbtWOBJ_DEC_READ_DATA_FOR_NAMES := 40, (*Read data to get wobj names*)
		rbtWOBJ_DEC_FIND_OFFSETS := 50, (*Find offset of wobjs*)
		rbtWOBJ_DEC_FIND_NAMES := 60, (*Find name of wobjs*)
		rbtWOBJ_DEC_CHECK_NAME := 70, (*Look if WorkObjectName exists*)
		rbtWOBJ_DEC_READ_DATA_FOR_PARAMS := 80, (*Read data to get wobj parameters*)
		rbtWOBJ_DEC_SEPARATE_ROW := 90, (*Separate row from readed data*)
		rbtWOBJ_DEC_FIND_WOBJ_NAME := 100, (*Find wobj name*)
		rbtWOBJ_DEC_FIND_ROBHOLD := 110, (*Find robot hold status*)
		rbtWOBJ_DEC_FIND_USR_FRAM_PROG := 120, (*Find user frame programmed*)
		rbtWOBJ_DEC_FIND_USR_FRAM_MECH := 130, (*Find user frame mechanical unit*)
		rbtWOBJ_DEC_FIND_PARAMS := 140, (*Find parameters of wobj*)
		rbtWOBJ_DEC_FIND_VALUE := 150, (*Find value between StartString and EndString*)
		rbtWOBJ_DEC_CLOSE_FILE := 160, (*Close file*)
		rbtWOBJ_DEC_ERROR := 255 (*Error step*)
		);
	RBTemplateWobjDecStatusIDEnum : 
		( (*Type of information / Description / Suggestion*)
		rbtWOBJ_DEC_FUB_OK := 0, (*State / OK*)
		rbtWOBJ_DEC_FUB_BUSY := 65535, (*State / Busy*)
		rbtWOBJ_DEC_ERR_NO_WOBJS := 45050, (*Error WobjDec lib / No wobjs found / Check if the file has any wobjs*)
		rbtWOBJ_DEC_ERR_BAD_WOBJ_SYNTAX := 45051, (*Error WobjDec lib / Some wobj has bad syntax / Check wobjs syntax (probably bad syntax of wobj with index Internal.Data.RowNumber, indexing is gradual and starts with zero)*)
		rbtWOBJ_DEC_ERR_BAD_WOBJ_NAME := 45052, (*Error WobjDec lib / Entered wobj name not found / Check correctness of entered wobj name*)
		rbtWOBJ_DEC_ERR_TOO_MANY_WOBJS := 45053, (*Error WobjDec lib / Too many wobjs in file / Increase the number of rbTEMPLATE_MAX_WOBJS constant or delete some wobjs from file*)
		rbtWOBJ_DEC_ERR_ROW_END_NOT_FND := 45054, (*Error WobjDec lib / Some row doesnt have semicolon / Check the semicolons at the end of the rows*)
		rbtWOBJ_DEC_ERR_BAD_WOBJ_INDEX := 45055, (*Error WobjDec lib / Tool index is higher then the number of wobjs / Run the ReadWorkObjectNames command and try again*)
		rbtWOBJ_DEC_ERR_ROBH_TR_AND_FALS := 45056, (*Error WobjDec lib / Robot hold is true and also false / Check robot hold syntax*)
		rbtWOBJ_DEC_ERR_ROBH_NOT_FOUND := 45057, (*Error WobjDec lib / Robot hold not found / Check robot hold syntax*)
		rbtWOBJ_DEC_ERR_UFP_TR_AND_FALS := 45058, (*Error WobjDec lib / User frame programmed is true and also false / Check user frame programmed syntax*)
		rbtWOBJ_DEC_ERR_UFP_NOT_FOUND := 45059, (*Error WobjDec lib / User frame programmed not found / Check user frame programmed syntax*)
		rbtWOBJ_DEC_ERR_UFMU_NOT_FOUND := 45060, (*Error WobjDec lib / User frame mechanical unit not found (both quotation marks not found) / Check user frame mechanical unit syntax (must be in quotation marks)*)
		rbtWOBJ_DEC_ERR_BAD_PARAMS_SNTX := 45061 (*Error WobjDec lib / Bad parameters syntax / Check syntax of parameters (except WorkObjectName and Robhold )*)
		);
END_TYPE
