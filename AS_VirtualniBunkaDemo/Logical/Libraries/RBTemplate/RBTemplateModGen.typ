
TYPE
	RBTemplateModGenStateEnum : 
		( (*File device link basic state enumeration*)
		rbTEMPLATE_MOD_GEN_NOT_ENABLED := 0, (*Function block is not enabled*)
		rbTEMPLATE_MOD_GEN_WAIT_CMD := 100, (*Function block is waiting for commands*)
		rbTEMPLATE_MOD_GEN_CHK_INSTR_LST := 110, (*Check instruction list validness*)
		rbTEMPLATE_MOD_GEN_GEN_INSTR_LST := 120, (*Check instruction list validness*)
		rbTEMPLATE_MOD_GEN_ERROR_RESET := 1000 (*Function block error reset*)
		);
	RBTemplateModGenFileGenState : 
		(
		rbTEMPLATE_MOD_GEN_OPEN_DEVICE := 0,
		rbTEMPLATE_MOD_GEN_GET_INFO_FILE := 10,
		rbTEMPLATE_MOD_GEN_DELETE_FILE := 30,
		rbTEMPLATE_MOD_GEN_CREATE_FILE := 40,
		rbTEMPLATE_MOD_GEN_CREATE_HEADER := 50,
		rbTEMPLATE_MOD_GEN_WRITE_HEADER := 60,
		rbTEMPLATE_MOD_GEN_CREATE_INSTR := 70,
		rbTEMPLATE_MOD_GEN_WRITE_INSTR := 80,
		rbTEMPLATE_MOD_GEN_WRITE_CLOSE := 90,
		rbTEMPLATE_MOD_GEN_CLOSE_FILE := 100
		);
	RBTemplateModGenErrorEnum : 
		(
		rbTEMPLATE_MODGEN_ERR_INVFILE := 45300, (*Invalid pointer to file name*)
		rbTEMPLATE_MODGEN_ERR_INVLIST := 45301, (*Invalid pointer to instruction list*)
		rbTEMPLATE_MODGEN_ERR_LSTLEN := 45302, (*Zero list length*)
		rbTEMPLATE_MODGEN_ERR_FILE := 45303 (*FDLink or File error*)
		);
	RBTemplateModGenInfoType : 	STRUCT  (*File device link info type*)
		Diag : MTDiagType; (*Diagnostics structure*)
	END_STRUCT;
	RBTemplateModGenInternalType : 	STRUCT  (*Communication TCP client internal type*)
		State : RBTemplateModGenStateEnum; (*State of function block*)
		ArrSize : UDINT;
		FDLinkBasic_0 : FDLinkBasic;
		PathSim : STRING[255];
		Path : STRING[255];
		GenFileState : RBTemplateModGenFileGenState;
		FileOffset : UDINT;
		FileIdent : UDINT;
		FileCreate_0 : FileCreate;
		FileInfo_0 : FileInfo;
		pInfo : fiFILE_INFO;
		FileClose_0 : FileClose;
		FileWrite_0 : FileWrite;
		FileDelete_0 : FileDelete;
		ActInstNum : UDINT;
		ActInstructionStr : STRING[20000];
	END_STRUCT;
END_TYPE
