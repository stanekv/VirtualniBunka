(*Cmd, MM, PLC -> Robot*)

TYPE
	RBTemplateRelToolType : 	STRUCT 
		X : REAL;
		Y : REAL;
		Z : REAL;
		Rx : REAL;
		Ry : REAL;
		Rz : REAL;
	END_STRUCT;
	RBTemplateControllerCmdType : 	STRUCT  (*System commands for controller*)
		MotorsOn : BOOL; (*Prikaz pro zapnuti motoru*)
		MotorsOff : BOOL; (*Prikaz pro vypnuti motoru*)
		Start : BOOL; (*Program Start - spusteni programu z aktualni pozice - spusteni PLC programu*)
		StartAtMain : BOOL; (*Program Start At Main - spusteni programu od zacatku*)
		Stop : BOOL;
		StopAtEndInstr : BOOL; (*Zastaveni robota po dokonceni aktualne provadene instrukce*)
		ResetEmergencyStop : BOOL; (*Reset EmStop - reset EmStop okruhu po nouzovem zastaveni*)
		ResetExecutionError : BOOL; (*Reset chyby - reset chyby na robotu*)
		SystemRestart : BOOL; (*Provedeni tepleho restartu systemu*)
		OpModeReqAuto : BOOL; (*Remote request of automaticmode*)
		OpModeReqManual : BOOL; (*Remoterequest of manual reduced speed mode*)
		AckAutoMode : BOOL; (*Acknowledgement of automatic mode*)
		ProfiSafeOpAck : BOOL; (*Acknowledge a change in the PROFIsafe communication.*)
		Fill1 : BOOL;
		Fill2 : BOOL;
		Fill3 : BOOL;
	END_STRUCT;
	RBTemplateRobotCmdType : 	STRUCT  (*Commands for (each) robot*)
		ItemWeight : REAL; (*Weight of item held by robot*)
		SpeedOverride : USINT; (*Speed override for all moves, value is multiplied by value set in FP*)
		LimitSpeed : BOOL; (*Omezi rychlost robota*)
		RelTool : BOOL; (*Use RelTool *)
		ModuleLoad : BOOL; (*Load program modul*)
		ProcStart : BOOL;
		Fill1 : BOOL;
		Fill2 : BOOL;
		Fill3 : BOOL;
		RelToolCords : RBTemplateRelToolType;
		ModuleName : STRING[rbtROBOT_CTRL_MODULE_STRING_LEN]; (*Modul name (35 Bytes + 1 end byte)*)
		ProcName : STRING[rbtROBOT_CTRL_PROC_STRING_LEN]; (*Procedure name (31 Bytes + 1 end byte)*)
		Fill4 : BOOL;
		Fill5 : BOOL;
	END_STRUCT;
	RBTemplateSystemCmdType : 	STRUCT 
		Controller : RBTemplateControllerCmdType;
		Robot : ARRAY[0..3]OF RBTemplateRobotCmdType;
	END_STRUCT;
END_TYPE

(*Status, MM, Robot -> PLC*)

TYPE
	RBTemplateControllerStatusType : 	STRUCT  (*Controller status*)
		LicenceApproved : BOOL; (*License of robot's RBTemplate is valid*)
		OperatorModeAutoChange : BOOL; (*Automatic mode needs to be acknowledged*)
		AutomatOn : BOOL; (*Controller is in automatic mode*)
		MotorsOn : BOOL; (*Motors are enabled*)
		MotorsOff : BOOL; (*Motors are disabled*)
		ExecutingMotionTask : BOOL; (*Any of the motion tasks is running*)
		EmStop : BOOL; (*Emergency stop triggered*)
		ExeError : BOOL; (*Execution error has occured*)
		RunChainOk : BOOL; (*All runchains are closed*)
		ESChainOk : BOOL; (*Emergency stop*)
		ASChainOk : BOOL; (*Auto stop*)
		GSChainOk : BOOL; (*General stop*)
		SSChainOk : BOOL; (*Superior stop*)
		SafeMoveViolated : BOOL; (*Safe Move violation*)
		WorldZone : ARRAY[0..19]OF BOOL; (*World zones, TRUE if robot is in zone*)
	END_STRUCT;
	RBTemplateRobotStatusType : 	STRUCT  (*Status from (each) robot*)
		LimitSpeed : BOOL; (*The speed limitation is set up with RAPID instructions SpeedLimAxis and SpeedLimCheckPoint*)
		MechUnitNotMove : BOOL; (*Robot neni v pohybu*)
		MotSupTriggered : BOOL; (*Detekce kolize robota - system vyhodnotil kolizi robota*)
		LoadModuleDone : BOOL; (*Procedure in act module was not loaded*)
		ProcExecuting : BOOL;
		RapidError : BOOL; (*Rapid executuion error*)
		RapidErrorNumber : UINT; (*Rapid execution error number*)
		SpeedOverride : USINT; (*Speed override set in FP*)
		Fill1 : BOOL;
	END_STRUCT;
	RBTemplateSystemStatusType : 	STRUCT 
		Controller : RBTemplateControllerStatusType;
		Robot : ARRAY[0..3]OF RBTemplateRobotStatusType;
	END_STRUCT;
END_TYPE

(*Request, MM, PLC -> Robot*)

TYPE
	RBTemplateSystemReqDataType :UDINT;
END_TYPE

(*Motion, MM, Robot -> PLC*)

TYPE
	RBTemplateRobotRobtargetType : 	STRUCT 
		X : REAL;
		Y : REAL;
		Z : REAL;
		Q1 : REAL;
		Q2 : REAL;
		Q3 : REAL;
		Q4 : REAL;
		Cf1 : SINT;
		Cf4 : SINT;
		Cf6 : SINT;
		Cfx : SINT;
	END_STRUCT;
	RBTemplateRobotMotionDataType : 	STRUCT 
		Robtarget : RBTemplateRobotRobtargetType;
		Jointtarget : RBTemplateABBRobjointType;
		ExternalAxis : RBTemplateABBExtjointType;
		ActTCPSpeed : REAL; (*[mm/s]*)
		RefTCPSpeed : REAL; (*[mm/s]*)
		ToolName : STRING[32];
		WobjName : STRING[32];
	END_STRUCT;
END_TYPE

(*Info, MM, Robot <-> PLC*)

TYPE
	RBTemplateSystemInfoType : 	STRUCT 
		SerialNumber : STRING[31]; (*Serial number of first robot*)
		SwVersion : STRING[31];
		SwVersionName : STRING[31];
		ControllerID : STRING[31];
		WAN_IPAddress : STRING[31];
		SystemLanguage : STRING[3];
		SystemName : STRING[31];
		RoboTemplateVersion : STRING[11]; (*Version of RBTemplate in robot, format XX.YY.ZZ, XX and YY must match version of RBTemplate in PLC*)
		RobotType : ARRAY[0..3]OF STRING[31]; (*Array is also used to determine number of connected robots*)
	END_STRUCT;
END_TYPE

(*User, MM, Robot <-> PLC*)

TYPE
	RBTemplateUserDataType : 	STRUCT 
		Bool : ARRAY[0..15]OF BOOL; (*User bool signals*)
		Dint : ARRAY[0..7]OF DINT; (*User double integer signals*)
		Real : ARRAY[0..7]OF REAL; (*User float signals*)
	END_STRUCT;
END_TYPE

(*Handshake, MM, Robot -> PLC*)

TYPE
	RBTemplateHandshakeDataType :UDINT;
END_TYPE
