
TYPE
	RBTemplateIdentHandlerErrorEnum : 
		(
		rbtIDENT_HANDLER_ERR_ := 0
		);
	RBTemplateIdentHandlerStateEnum : 
		(
		rbtIDENT_HANDLER_STATE_ENABLE,
		rbtIDENT_HANDLER_STATE_CHK_FILE,
		rbtIDENT_HANDLER_STATE_CREATE,
		rbtIDENT_HANDLER_STATE_WAIT,
		rbtIDENT_HANDLER_STATE_LOAD,
		rbtIDENT_HANDLER_STATE_SAVE,
		rbtIDENT_HANDLER_STATE_ERROR
		);
	RBTemplateIdentHandlerInfoType : 	STRUCT 
		Diag : MTDiagType;
	END_STRUCT;
	RBTemplateIdentHandlerIntType : 	STRUCT 
		DatObjInfo_0 : DatObjInfo;
		DatObjCreate_0 : DatObjCreate;
		DatObjRead_0 : DatObjRead;
		DatObjWrite_0 : DatObjWrite;
		DatObjIdent : UDINT;
		OldErrorReset : BOOL;
		OldLoad : BOOL;
		OldSave : BOOL;
		State : RBTemplateIdentHandlerStateEnum;
	END_STRUCT;
END_TYPE
