
TYPE
	RBTemplateABBMoveInstrListType : 	STRUCT 
		Name : STRING[32]; (*Point (instruction) name*)
		Type : RBTemplateABBMoveInstrEnum; (*Type of move instruction - MoveJ, MoveL*)
		Concurrent : USINT; (*Optional - Subsequent instructions are executed while the robot is moving. The argument is usually not used but can be used to avoid unwanted stops caused by overloaded CPU when using fly-by points. This is useful when the programmed points are very close together at high speeds. The argument is also useful when, for example, communicating with external equipment and synchronization between the external equipment and robot movement is not required.*)
		ToPoint : RBTemplateABBRobtargetType; (*The destination point of the robot and external axes. It is defined as a named position or stored directly in the instruction (marked with an * in the instruction).*)
		SynchronizationId : RBTemplateABBIdentnoType; (*Optional - The argument [ \ID ] is mandatory in the MultiMove systems, if the movement is synchronized or coordinated synchronized. This argument is not allowed in any other case. The specified id number must be the same in all the cooperating program tasks. By using the id number the movements are not mixed up at the runtime.*)
		Speed : RBTemplateABBSpeeddataType; (*The speed data that applies to movements. Speed data defines the velocity for the tool center point, the tool reorientation, and external axes.*)
		PredefinedSpeed : RBTemplateABBPredefSpeedEnum; (*Optional - This argument is used to select predefined speed (v5, v10, v100 etc.).*)
		Time : REAL; (*Optional - This argument is used to specify the total time in seconds during which the robot moves. It is then substituted for the corresponding speed data.*)
		Velocity : REAL; (*Optional - This argument is used to specify the velocity of the TCP in mm/s directly in the instruction. It is then substituted for the corresponding velocity specified in the speed data.*)
		PredefinedZone : RBTemplateABBPredefZoneEnum; (*Optional - This argument is used to select predefined zone (z0, z5, fine etc.).*)
		Zone : RBTemplateABBZonedataType; (*Optional - Zone data for the movement. Zone data describes the size of the generated corner path.*)
		InPosition : RBTemplateABBStoppointdataType; (*Optional - This argument is used to specify the convergence criteria for the position of the robot�s TCP in the stop point. The stop point data substitutes the zone specified in the Zone parameter.*)
		PredefinedStoppoint : RBTemplateABBPredefStoppointEnum; (*Optional - This argument is used to specify the convergence criteria for the position of the robot�s TCP in the stop point. The stop point data substitutes the zone specified in the Zone parameter.*)
		Tool : RBTemplateABBTooldataType; (*The tool in use when the robot moves. The tool center point is the point moved to the specified destination position.*)
		WorkObject : RBTemplateABBWobjdataType; (*Optional - The work object (coordinate system) to which the robot position in the instruction is related. This argument can be omitted and if so then the position is related to the world coordinate system. If, on the other hand, a stationary tool or coordinated external axes are used then this argument must be specified to perform a linear movement relative to the work object.*)
		Correction : USINT; (*Correction data written to a corrections entry by the instruction CorrWrite will be added to the path and destination position if this argument is present. The RobotWare option Path Offset is required when using this argument.*)
		TotalLoad : RBTemplateABBLoaddataType; (*Optional - The \TLoad argument describes the total load used in the movement. The total load is the tool load together with the payload that the tool is carrying. If the \TLoad argument is used, then the loaddata in the current tooldata is not considered. If the \TLoad argument is set to load0, then the \TLoad argument is not considered and the loaddata in the current tooldata is used instead. To be able to use the \TLoad argument it is necessary to set the value of the system parameter ModalPayLoadMode to 0. If ModalPayLoadMode is set to 0, it is no longer possible to use the instruction GripLoad. The total load can be identified with the service routine LoadIdentify. If the system parameter ModalPayLoadMode is set to 0, the operator has the possibility to copy the loaddata from the tool to an existing or new loaddata persistent variable when running the service routine. It is possible to test run the program without any payload by using a digital input signal connected to the system input SimMode (Simulated Mode). If the digital input signal is set to 1, the loaddata in the optional argument \TLoad is not considered, and the loaddata in the current tooldata is used instead.*)
	END_STRUCT;
END_TYPE

(*Inpos*)

TYPE
	RBTemplateABBPredefStoppointEnum : 
		(
		abbSTOPPOINT_NONE := 0, (*none*)
		abbSTOPPOINT_INPOS20 := 10, (*inpos20*)
		abbSTOPPOINT_INPOS50 := 11, (*inpos50*)
		abbSTOPPOINT_INPOS100 := 12, (*inpos100*)
		abbSTOPPOINT_STOPTIME0_5 := 20, (*stoptime0_5*)
		abbSTOPPOINT_STOPTIME1_0 := 21, (*stoptime1_0*)
		abbSTOPPOINT_STOPTIME1_5 := 22, (*stoptime1_5*)
		abbSTOPPOINT_FLLWTIME0_5 := 30, (*fllwtime0_5*)
		abbSTOPPOINT_FLLWTIME1_0 := 31, (*fllwtime1_0*)
		abbSTOPPOINT_FLLWTIME1_5 := 32 (*fllwtime1_5*)
		);
	RBTemplateABBInposDataType : 	STRUCT 
		position : REAL; (*The position condition (the radius) for the TCP in percent of a normal fine stop point.*)
		speed : REAL; (*The speed condition for the TCP in percent of a normal fine stop point.*)
		mintime : REAL; (*The minimum wait time in seconds before in position. Used to make the robot wait at least the specified time in the point. Maximum value is 20.0 seconds.*)
		maxtime : REAL; (*The maximum wait time in seconds for convergence criteria to be satisfied. Used to assure that the robot does not get stuck in the point if the speed and position conditions are set too tight. Maximum value is 20.0 seconds.*)
	END_STRUCT;
END_TYPE

(*Identno*)

TYPE
	RBTemplateABBPoseType : 	STRUCT 
		Trans : RBTemplateABBPosType; (*The displacement in position (x, y, and z) of the coordinate system.*)
		Rot : RBTemplateABBOrientType; (*The rotation of the coordinate system.*)
	END_STRUCT;
END_TYPE

(*InPosition*)

TYPE
	RBTemplateABBStoppointdataType : 	STRUCT 
		type : RBTemplateABBStoppointTypeEnum; (*type of stop point*)
		progsynch : BOOL; (*Synchronization with RAPID program execution. TRUE: The movement is synchronized with RAPID execution. The program does not start to execute the next instruction until the stop point has been reached. FALSE: The movement is not synchronized with RAPID execution. The program starts the execution of the next instruction before the stop point has been reached.*)
		inposPosition : RBTemplateABBInposDataType;
		stoptime : REAL; (*The time in seconds, the TCP stands still in position before starting the next movement. Valid range 0 - 20 s, resolution 0.001 s.*)
		followtime : REAL; (*The time in seconds the TCP follows the conveyor. Valid range 0 - 20 s, resolution 0.001 s.*)
		signal : STRING[16]; (*Reserved for future use.*)
		relation : REAL; (*Reserved for future use.*)
		checkvalue : REAL; (*Reserved for future use.*)
	END_STRUCT;
END_TYPE

(*Speed*)

TYPE
	RBTemplateABBPredefSpeedEnum : 
		(
		abbSPEED_NONE := 0,
		abbSPEED_V5 := 5,
		abbSPEED_V10 := 10,
		abbSPEED_V20 := 20,
		abbSPEED_V30 := 30,
		abbSPEED_V40 := 40,
		abbSPEED_V50 := 50,
		abbSPEED_V60 := 60,
		abbSPEED_V70 := 70,
		abbSPEED_V80 := 80,
		abbSPEED_V100 := 100,
		abbSPEED_V150 := 150,
		abbSPEED_V200 := 200,
		abbSPEED_V300 := 300,
		abbSPEED_V400 := 400,
		abbSPEED_V500 := 500,
		abbSPEED_V600 := 600,
		abbSPEED_V800 := 800,
		abbSPEED_V1000 := 1000,
		abbSPEED_V1500 := 1500,
		abbSPEED_V2000 := 2000,
		abbSPEED_V2500 := 2500,
		abbSPEED_V3000 := 3000,
		abbSPEED_V4000 := 4000,
		abbSPEED_V5000 := 5000,
		abbSPEED_V6000 := 6000,
		abbSPEED_V7000 := 7000
		);
END_TYPE

(*Zone*)

TYPE
	RBTemplateABBPredefZoneEnum : 
		(
		abbZONE_Z0 := 0,
		abbZONE_Z1 := 1,
		abbZONE_Z5 := 5,
		abbZONE_Z10 := 10,
		abbZONE_Z15 := 15,
		abbZONE_Z20 := 20,
		abbZONE_Z30 := 30,
		abbZONE_Z40 := 40,
		abbZONE_Z50 := 50,
		abbZONE_Z60 := 60,
		abbZONE_Z80 := 80,
		abbZONE_Z100 := 100,
		abbZONE_Z150 := 150,
		abbZONE_Z200 := 200,
		abbZONE_FINE := 998,
		abbZONE_NONE := 999
		);
END_TYPE

(*MoveInstruction*)

TYPE
	RBTemplateABBMoveInstrEnum : 
		(
		abbMOVE_J := 0,
		abbMOVE_L := 1
		);
	RBTemplateABBStoppointTypeEnum : 
		( (*Data type stoppoint is an alias data type for num. It is used to choose the type of stop point and which data elements to use in the stoppointdata*)
		abbSTOPPOINT_TYPE_NONE := 0,
		abbSTOPPOINT_TYPE_INPOS := 1, (*The movement terminates as an in-position type of stop point. Enables the inpos element in stoppointdata. The zone data in the instruction is not used, use fine or z0.*)
		abbSTOPPOINT_TYPE_STOPTIME := 2, (*The movement terminates as a stop-time type of stop point. Enables the stoptime element in stoppointdata. The zone data in the instruction is not used, use fine or z0.*)
		abbSTOPPOINT_TYPE_FOLLOWTIME := 3 (*The movement terminates as a conveyor follow-time type of fine point. The zone data in the instruction is used when the robot leaves the conveyor. Enables the followtime element in stoppointdata.*)
		);
	RBTemplateABBIdentnoType : 	STRUCT  (*dentno (Identity Number) is used to control synchronizing of two or more coordinated synchronized movements with each other. The data type identno can only be used in a MultiMove system with option Coordinated Robots and only in program tasks defined as Motion Task.*)
		syncident : USINT;
	END_STRUCT;
	RBTemplateABBWobjdataType : 	STRUCT  (*If work objects are defined in a positioning instruction, the position will be based on the coordinates of the work object. The advantages of this are as follows: If position data is entered manually, such as in off-line programming, the values can often be taken from a drawing. Programs can be reused quickly following changes in the robot installation. If, for example, the fixture is moved, only the user coordinate system has to be redefined. Variations in how the work object is attached can be compensated for. For this, however, some sort of sensor will be required to position the work object. If a stationary tool or coordinated external axes are used, the work object must be defined, since the path and velocity would then be related to the work object instead of the TCP. Work object data can also be used for jogging: The robot can be jogged in the directions of the work object. The current position displayed is based on the coordinate system of the work object.*)
		Robhold : BOOL; (*Defines whether or not the robot in the actual program task is holding the work object: TRUE: The robot is holding the work object, i.e. using a stationary tool. FALSE: The robot is not holding the work object, i.e. the robot is holding the tool.*)
		UserFrameProgrammed : BOOL; (*Defines whether or not a fixed user coordinate system is used: TRUE: Fixed user coordinate system. FALSE: Movable user coordinate system, i.e. coordinated external axes are used. Also to be used in a MultiMove system in semicoordinated or synchronized coordinated mode.*)
		UserFrameMechanicalUnit : STRING[80]; (*The mechanical unit with which the robot movements are coordinated. Only specified in the case of movable user coordinate systems (ufprog is FALSE). Specify the mechanical unit name defined in system parameters, e.g. orbit_a.*)
		UserFrame : RBTemplateABBPoseType; (*The user coordinate system, i.e. the position of the current work surface or fixture (see figure below): The position of the origin of the coordinate system (x, y and z) in mm. The rotation of the coordinate system, expressed as a quaternion (q1, q2, q3, q4). If the robot is holding the tool, the user coordinate system is defined in the world coordinate system (in the wrist coordinate system if a stationary tool is used). For movable user frame (ufprog is FALSE), the user frame is continuously defined by the system.*)
		ObjectFrame : RBTemplateABBPoseType; (*The object coordinate system, i.e. the position of the current work object (see figure below): The position of the origin of the coordinate system (x, y and z) in mm. The rotation of the coordinate system, expressed as a quaternion (q1, q2, q3, q4).*)
		WorkObjectName : STRING[50];
	END_STRUCT;
	RBTemplateABBSpeeddataType : 	STRUCT  (*Is used to specify the velocity at which both the robot and the external axes move.*)
		v_tcp : REAL; (*The velocity of the tool center point (TCP) in mm/s. If a stationary tool or coordinated external axes are used, the velocity is specified relative to the work object.*)
		v_ori : REAL; (*The reorientation velocity of the TCP expressed in degrees/s. If a stationary tool or coordinated external axes are used, the velocity is specified relative to the work object.*)
		v_leax : REAL; (*The velocity of linear external axes in mm/s.*)
		v_reax : REAL; (*The velocity of rotating external axes in degrees/s.*)
	END_STRUCT;
END_TYPE

(*Target*)

TYPE
	RBTemplateABBRobtargetType : 	STRUCT  (*Position data is used to define the position in the move instructions to which the robot and additional axes are to move.*)
		Trans : RBTemplateABBPosType;
		Rot : RBTemplateABBOrientType;
		Robconf : RBTemplateABBRobconfType;
		Extax : RBTemplateABBExtjointType;
	END_STRUCT;
	RBTemplateABBJointtargetType : 	STRUCT  (*Jointtarget defines each individual axis position, for both the robot and the external axes.*)
		Robax : RBTemplateABBRobjointType;
		Extax : RBTemplateABBExtjointType;
	END_STRUCT;
END_TYPE

(*Parts of target type*)

TYPE
	RBTemplateABBPosType : 	STRUCT  (*The position (x, y, and z) of the tool center point expressed in mm.*)
		X : REAL;
		Y : REAL;
		Z : REAL;
	END_STRUCT;
	RBTemplateABBOrientType : 	STRUCT  (*The orientation of the tool, expressed in the form of a quaternion (q1, q2, q3, and q4).*)
		Q1 : REAL;
		Q2 : REAL;
		Q3 : REAL;
		Q4 : REAL;
	END_STRUCT;
	RBTemplateABBRobconfType : 	STRUCT  (*The axis configuration of the robot (cf1, cf4, cf6, and cfx). This is defined in the form of the current quarter revolution of axis 1, axis 4, and axis 6. The first positive quarter revolution 0 to 90� is defined as 0. The meaning of the component cfx is dependent on robot type.*)
		Cf1 : SINT;
		Cf4 : SINT;
		Cf6 : SINT;
		Cfx : SINT;
	END_STRUCT;
	RBTemplateABBExtjointType : 	STRUCT  (*The position of the additional axes.*)
		Eaxa : REAL;
		Eaxb : REAL;
		Eaxc : REAL;
		Eaxd : REAL;
		Eaxe : REAL;
		Eaxf : REAL;
	END_STRUCT;
	RBTemplateABBRobjointType : 	STRUCT  (*Axis positions of the robot axes in degrees.*)
		Rax1 : REAL;
		Rax2 : REAL;
		Rax3 : REAL;
		Rax4 : REAL;
		Rax5 : REAL;
		Rax6 : REAL;
	END_STRUCT;
END_TYPE

(*Tool*)

TYPE
	RBTemplateABBLoaddataType : 	STRUCT  (*loaddata is used to describe loads attached to the mechanical interface of the robot (the robot�s mounting flange). Load data usually defines the payload or grip load (set up by the instruction GripLoad or MechUnitLoad for positioners) of the robot, that is, the load held in the robot gripper. loaddata is also used as part of tooldata to describe the tool load.*)
		Mass : REAL; (*The mass (weight) of the load in kg.*)
		Cog : RBTemplateABBPosType; (*The center of gravity of the payload expressed in mm in the tool coordinate system if the robot is holding the tool. If a stationary tool is used then the center of gravity for the payload held by the gripper is expressed in the object frame of the work object coordinate system moved by the robot.*)
		Aom : RBTemplateABBOrientType; (*The orientation of the axes of moment. These are the principal axes of the payload moment of inertia with origin in cog. If the robot is holding the tool, the axes of moment are expressed in the tool coordinate system. The figure shows the center of gravity and inertial axes of the payload.*)
		Ix : REAL; (*The moment of inertia of the load around the x-axis of moment expressed in kgm2. Correct definition of the moments of inertia will allow optimal utilization of the path planner and axes control. This may be of special importance when handling large sheets of metal, and so on. All moments of inertia ix, iy, and iz equal to 0 kgm2 imply a point mass. Normally, the moments of inertia must only be defined when the distance from the mounting flange to the center of gravity is less than the maximal dimension of the load (see the following figure).*)
		Iy : REAL; (*The moment of inertia of the load around the y-axis, expressed in kgm2. For more information, see ix.*)
		Iz : REAL; (*The moment of inertia of the load around the z-axis, expressed in kgm2. For more information, see ix.*)
	END_STRUCT;
	RBTemplateABBTooldataType : 	STRUCT  (*tooldata is used to describe the characteristics of a tool, for example, a welding gun or a gripper. These characteristics are position and orientation of the tool center point (TCP) and the physical characteristics of the tool load. If the tool is fixed in space (a stationary tool), the tool data firstly defines position and orientation of this very tool in space, TCP. Then it describes the load of the gripper moved by the robot.*)
		Robhold : BOOL; (*Defines whether or not the robot is holding the tool: TRUE: The robot is holding the tool. FALSE: The robot is not holding the tool, that is, a stationary tool.*)
		Tframe : RBTemplateABBPoseType; (*The tool coordinate system, that is: The position of the TCP (x, y and z) in mm, expressed in the wrist coordinate system (tool0) (see figure below). The orientation of the tool coordinate system, expressed in the wrist coordinate system (see figure below).*)
		Tload : RBTemplateABBLoaddataType; (*Robot held tool: The load of the tool, that is: The mass (weight) of the tool in kg. The center of gravity of the tool load (x, y and z) in mm, expressed in the wrist coordinate system The orientation of the principal inertial axes of moment of the tool expressed in the wrist coordinate system The moments of inertia around inertial axes of moment in kgm2. If all inertial components are defined as being 0 kgm2, the tool is handled as a point mass.  Stationary tool: The load of the gripper holding the work object: The mass (weight) of the moved gripper in kg The center of gravity of moved gripper (x, y and z) in mm, expressed in the wrist coordinate system The orientation of the principal inertial axes of moment of the moved gripper expressed in the wrist coordinate system The moments of inertia around inertial axes of moment in kgm2. If all inertial components are defined as being 0 kgm2, the gripper is handled as a point mass. *)
		Tname : STRING[50];
	END_STRUCT;
	RBTemplateABBZonedataType : 	STRUCT  (*zonedata is used to specify how a position is to be terminated, i.e. how close to the programmed position the axes must be before moving towards the next position.*)
		finep : BOOL; (*Defines whether the movement is to terminate as a stop point (fine point) or as a fly-by point. TRUE: The movement terminates as a stop point, and the program execution will not continue until robot reach the stop point. The remaining components in the zone data are not used. FALSE: The movement terminates as a fly-by point, and the program execution continues when the prefetch conditions have been met (see system parameter Prefetch Time).*)
		pzone_tcp : REAL; (*The size (the radius) of the TCP zone in mm.*)
		pzone_ori : REAL; (*The zone size (the radius) for the tool reorientation. The size is defined as the distance of the TCP from the programmed point in mm. The size must be larger than the corresponding value for pzone_tcp. If a lower value is specified, the size is automatically increased to make it the same as pzone_tcp.*)
		pzone_eax : REAL; (*The zone size (the radius) for external axes. The size is defined as the distance of the TCP from the programmed point in mm. The size must be larger than the corresponding value for pzone_tcp. If a lower value is specified, the size is automatically increased to make it the same as pzone_tcp.*)
		zone_ori : REAL; (*The zone size for the tool reorientation in degrees. If the robot is holding the work object, this means an angle of rotation for the work object.*)
		zone_leax : REAL; (*The zone size for linear external axes in mm.*)
		zone_reax : REAL; (*The zone size for rotating external axes in degrees.*)
	END_STRUCT;
END_TYPE
