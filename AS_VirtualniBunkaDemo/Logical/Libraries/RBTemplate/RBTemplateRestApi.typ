(*Enumeration*)

TYPE
	RBTemplateRestApiStateEnum : 
		( (*FUB states*) (* *) (*#OMIT*)
		rbTEMPLATE_RESTAPI_NOT_ENABLED := 0, (*Wait for FUB enable*)
		rbTEMPLATE_RESTAPI_CONNECT := 10, (*Authentication*)
		rbTEMPLATE_RESTAPI_WAIT := 20, (*Wait for command*)
		rbTEMPLATE_RESTAPI_UPL_FILE_P := 30, (*Prepare values for file upload*)
		rbTEMPLATE_RESTAPI_UPL_FILE := 31, (*Upload a new file*)
		rbTEMPLATE_RESTAPI_UPL_FILE_W := 32, (*Wait until uploading is finished*)
		rbTEMPLATE_RESTAPI_READFILE := 40,
		rbTEMPLATE_RESTAPI_RMSHIP_REQ_S := 50, (*Send request for RAPID mastership*)
		rbTEMPLATE_RESTAPI_RMSHIP_REQ_W := 51, (*Wait for response to RAPID mastership request*)
		rbTEMPLATE_RESTAPI_FILE_READ_S := 52, (*Request content of file specified in parameters*)
		rbTEMPLATE_RESTAPI_FILE_READ_W := 53, (*Wait for response to file read request*)
		rbTEMPLATE_RESTAPI_DOWNLOAD_P := 60, (*Prepare values for file download*)
		rbTEMPLATE_RESTAPI_DOWNLOAD_S := 61, (*Send request download specified file*)
		rbTEMPLATE_RESTAPI_DOWNLOAD_W := 62, (*Wait for response to download request*)
		rbTEMPLATE_RESTAPI_DOWNLOAD_F := 63, (*Save read content to file*)
		rbTEMPLATE_RESTAPI_UPL_DATA_P := 70, (*Prepare values for data upload*)
		rbTEMPLATE_RESTAPI_UPL_DATA := 71, (*Upload data*)
		rbTEMPLATE_RESTAPI_UPL_DATA_W := 72, (*Wait for upload data to finish*)
		rbTEMPLATE_RESTAPI_DISABLE := 90,
		rbTEMPLATE_RESTAPI_ERROR := 100 (*Error handling*)
		);
	RBTemplateRestApiErrorEnum : 
		( (*Error codes, 45321 - 45349*)
		rbTEMPLATE_RESTAPI_ERR_INV_PAR := 45321, (*Invalid input parameters.*)
		rbTEMPLATE_RESTAPI_ERR_INT_BUF := 45322, (*Some of the internal buffers is too small.*)
		rbTEMPLATE_RESTAPI_ERR_HASH_BUF := 45323, (*Buffer for hash input string is too small.*)
		rbTEMPLATE_RESTAPI_ERR_CONN := 45324, (*Connection failed.*)
		rbTEMPLATE_RESTAPI_ERR_AUTH := 45325, (*Authentication failed.*)
		rbTEMPLATE_RESTAPI_ERR_HEAD_PARS := 45326, (*Parsing header unsuccessful, key(s) not found.*)
		rbTEMPLATE_RESTAPI_ERR_UPLOAD := 45327, (*Upload failed.*)
		rbTEMPLATE_RESTAPI_ERR_UPL_ENC := 45328, (*Error encoding url before file upload*)
		rbTEMPLATE_RESTAPI_ERR_UPL_EMPTY := 45329, (*File content is empty, it is not possible to upload empty file.*)
		rbTEMPLATE_RESTAPI_ERR_UPL_FNAME := 45330, (*Invalid file name or path.*)
		rbTEMPLATE_RESTAPI_ERR_COOKIES := 45331, (*Cookies could not be saved.*)
		rbTEMPLATE_RESTAPI_ERR_URI_BUF := 45332, (*Request URI buffer is not long enough.*)
		rbTEMPLATE_RESTAPI_ERR_REQ_D_BUF := 45333, (*Request data buffer is not long enough.*)
		rbTEMPLATE_RESTAPI_ERR_UPL_SRCF := 45334, (*Error opening / reading upload source file. It is not specified or does not exist.*)
		rbTEMPLATE_RESTAPI_ERR_READFILE := 45335, (*Read file error. Check if specified file exists in robot controller and if data buffer is long enough.*)
		rbTEMPLATE_RESTAPI_ERR_FILE_NAME := 45336, (*Invalid file name or path.*)
		rbTEMPLATE_RESTAPI_ERR_DWL_DSTF := 45337, (*Error saving file to specified destination. Destination is not specified or does not exist. Check pDstPath, pDstPathSim and pDstDevice.*)
		rbTEMPLATE_RESTAPI_ERR_DATA_BUF := 45338, (*Invalid (not specified) parameter pDataBuffer.*)
		rbTEMPLATE_RESTAPI_ERR_DOWNLOAD := 45339, (*Error reading file from controller*)
		rbTEMPLATE_RESTAPI_ERR_SMALL_BUF := 45340, (*Supplied buffer pDataBuffer is too small. Increase buffer and try again.*)
		rbTEMPLATE_RESTAPI_ERR_NOT_FOUND := 45341, (*File to download does not exist in controller.*)
		rbTEMPLATE_RESTAPI_ERR_PATH_LONG := 45342 (*Specified SrcPath, SrcPathSim, DstPath or DstPathSim is too long.*)
		);
	RBTemplateRestApiSaveFileEnum : 
		( (*FUB download file states*) (* *) (*#OMIT*)
		rbTEMPLATE_RESTAPI_SF_FDLINK := 0, (*DevLink*)
		rbTEMPLATE_RESTAPI_SF_CREATE := 10, (*FCreate*)
		rbTEMPLATE_RESTAPI_SF_DELETE := 20, (*FOpen*)
		rbTEMPLATE_RESTAPI_SF_WRITE := 30, (*FRead*)
		rbTEMPLATE_RESTAPI_SF_CLOSE := 40, (*FClose*)
		rbTEMPLATE_RESTAPI_SF_UNLINK := 50 (*DevUnlink*)
		);
	RBTemplateRestApiReadFileEnum : 
		( (*FUB readfile states*) (* *) (*#OMIT*)
		rbTEMPLATE_RESTAPI_RF_FDLINK := 0, (*FDLink*)
		rbTEMPLATE_RESTAPI_RF_OPEN := 10, (*FOpen*)
		rbTEMPLATE_RESTAPI_RF_READ := 20, (*FRead*)
		rbTEMPLATE_RESTAPI_RF_CLOSE := 30, (*FClose*)
		rbTEMPLATE_RESTAPI_RF_UNLINK := 40 (*DevUnlink*)
		);
	RBTemplateRestApiConnectEnum : 
		( (*FUB connect states*) (* *) (*#OMIT*)
		rbTEMPLATE_RESTAPI_CON_INIT := 0, (*Send "blank" request*)
		rbTEMPLATE_RESTAPI_CON_PARSE := 10, (*Parse variables from response header*)
		rbTEMPLATE_RESTAPI_CON_HASH_A1 := 20, (*Create MD5 hash of A1*)
		rbTEMPLATE_RESTAPI_CON_HASH_A1_W := 21,
		rbTEMPLATE_RESTAPI_CON_HASH_A2 := 30, (*Create MD5 hash of A2*)
		rbTEMPLATE_RESTAPI_CON_HASH_A2_W := 31,
		rbTEMPLATE_RESTAPI_CON_PREPARE := 40, (*Prepare request raw header*)
		rbTEMPLATE_RESTAPI_CON_HASH_R := 50, (*Create MD5 hash of response*)
		rbTEMPLATE_RESTAPI_CON_HASH_R_W := 51,
		rbTEMPLATE_RESTAPI_CON_SEND := 60, (*Send request with response*)
		rbTEMPLATE_RESTAPI_CON_COOKIES := 70, (*Set cookies from response header*)
		rbTEMPLATE_RESTAPI_CON_DONE := 80 (*Authentication successful*)
		);
END_TYPE

(*Structure*)

TYPE
	RBTemplateRestApiCmdType : 	STRUCT 
		UploadFile : BOOL; (*Uploads pSrcFile to robot pFileName. If the file exists, it is overwritten*)
		UploadData : BOOL; (*Write content of pDataBuffer to file specified in pFileName*)
		DownloadFile : BOOL; (*Download pFileName to DstPath/DstFile*)
		ReadFile : BOOL; (*Read content of file specified at pFileName*)
	END_STRUCT;
	RBTemplateRestApiParType : 	STRUCT 
		pRobotIpAdr : UDINT; (*Robot IP address*)
		pRobotIpAdrSim : UDINT; (*Simulation robot IP address*)
		pUsername : UDINT; (*Robot RestAPI username*)
		pPassword : UDINT; (*Robot RestAPI password*)
		pSrcPath : UDINT; (*Path to source file (upload)*)
		pSrcPathSim : UDINT; (*Simulation path to source file (upload)*)
		pSrcDevice : UDINT; (*Name of device with source folder*)
		pSrcFile : UDINT; (*Source file (upload) *)
		LinkSrcPath : BOOL; (*Switches between pSrcDevice and pSrcPath*)
		pFileName : UDINT; (*File name (including path) in robot controller. Root directory is $home*)
		pDataBuffer : UDINT; (*Pointer to data buffer, used to save content of read file*)
		DataBufferLen : UDINT; (*Data buffer length*)
		pDstPath : UDINT; (*Path to destination file (download)*)
		pDstPathSim : UDINT; (*Simulation path to destination file (download)*)
		pDstDevice : UDINT; (*Name of device with source folder*)
		LinkDstPath : BOOL; (*Switches between pDstDevice and pDstPath*)
		pDstFile : UDINT; (*New name of downloaded file. When blank, pFileName is used*)
	END_STRUCT;
	RBTemplateRestApiInfoType : 	STRUCT 
		Diag : MTDiagType;
	END_STRUCT;
	RBTemplateRestApiIntCmdType : 	STRUCT  (*Internal commands*) (* *) (*#OMIT*)
		Connect : BOOL;
	END_STRUCT;
	RBTemplateRestApiIntStatusType : 	STRUCT  (*Internal status*) (* *) (*#OMIT*)
		Connected : BOOL; (*Communication is active (TRUE)*)
		CommandEdge : RBTemplateRestApiCmdType; (*Holds values of input Commands from previous cycle*)
		ErrorState : RBTemplateRestApiConnectEnum; (*State in which last error has occured*)
		ErrorResetOld : BOOL; (*Holds ErrorReset input value from previous cycle*)
	END_STRUCT;
	RBTemplateRestApiIntDataType : 	STRUCT  (*Internal data*) (* *) (*#OMIT*)
		RequestUri : STRING[rbTEMPLATE_RESTAPI_REQ_URI_BUF]; (*Request uri buffer*)
		RequestData : STRING[rbTEMPLATE_RESTAPI_REQ_DATA_BUF]; (*Request data in raw format*)
		RequestHeader : httpRequestHeader_t; (*Request header buffer*)
		RequestRawHeader : STRING[rbTEMPLATE_RESTAPI_REQ_RAW_BUF]; (*Request header in raw format*)
		ResponseData : STRING[rbTEMPLATE_RESTAPI_RES_DATA_BUF]; (*Response data in raw format*)
		ResponseHeader : httpResponseHeader_t; (*Response header buffer*)
		ResponseRawHeader : STRING[rbTEMPLATE_RESTAPI_RES_RAW_BUF]; (*Response header in raw format*)
		ParseKey : STRING[rbTEMPLATE_RESTAPI_PARSE_KEY_BUF]; (*Key to parse from response header*)
		ParsedValue : STRING[rbTEMPLATE_RESTAPI_PARSE_VAL_BUF]; (*General buffer for value parsed from response header*)
		DigestRealm : STRING[rbTEMPLATE_RESTAPI_REALM_BUF]; (*Buffer for realm string*)
		DigestNonce : STRING[rbTEMPLATE_RESTAPI_NONCE_BUF]; (*Buffer for nonce string*)
		DigestCnonce : STRING[rbTEMPLATE_RESTAPI_CNONCE_BUF] := '123456'; (*Buffer for cnonce string*)
		DigestOpaque : STRING[rbTEMPLATE_RESTAPI_OPAQUE_BUF]; (*Buffer for opaque string*)
		DigestQop : STRING[rbTEMPLATE_RESTAPI_QOP_BUF]; (*Buffer for qop string*)
		DigestNonceCnt : UDINT; (*Nonce count*)
		DigestNonceCntStr : STRING[rbTEMPLATE_RESTAPI_NC_BUF]; (*Buffer for nonce count string, HEX*)
		StringToHash : STRING[rbTEMPLATE_RESTAPI_HASH_IN_BUF]; (*String to hash to MD5*)
		DigestA1 : STRING[rbTEMPLATE_RESTAPI_DIGEST_A_BUF]; (*Buffer for MD5 hashed A1*)
		EncodeUrlBuffer : STRING[rbTEMPLATE_RESTAPI_ENC_URL_BUF];
		DigestA2 : STRING[rbTEMPLATE_RESTAPI_DIGEST_A_BUF]; (*Buffer for MD5 hashed A2*)
		ReadFileState : RBTemplateRestApiReadFileEnum;
		SaveFileState : RBTemplateRestApiSaveFileEnum;
		ConnectState : RBTemplateRestApiConnectEnum; (*Connect state*)
		StateAfterConnect : RBTemplateRestApiConnectEnum;
		FileName : STRING[36]; (*File name buffer*)
		pDstFile : {REDUND_UNREPLICABLE} UDINT; (*Pointer to destination file name, value from parameters - pFileName or pSrcFile (if pFileName is empty)*)
		DevLinkParam : STRING[72]; (*Parameter string buffer for DevLink*)
		DeviceHandle : UDINT; (*DevLink and DevUnlink device handle *)
	END_STRUCT;
	RBTemplateRestApiIntFubType : 	STRUCT  (* *) (* *) (*#OMIT*)
		TON_0 : TON;
		Client_0 : httpClient;
		httpEncodeUrl_0 : httpEncodeUrl;
		MTHashMd5_0 : MTHashMd5;
		DevLink_0 : DevLink;
		DevUnlink_0 : DevUnlink;
		FileDelete_0 : FileDelete;
		FileCreate_0 : FileCreate;
		FileOpen_0 : FileOpen;
		FileRead_0 : FileRead;
		FileWrite_0 : FileWrite;
		FileClose_0 : FileClose;
	END_STRUCT;
	RBTemplateRestApiInternalType : 	STRUCT  (* *) (* *) (*#OMIT*)
		State : RBTemplateRestApiStateEnum; (*Next (current) state*)
		Commands : RBTemplateRestApiIntCmdType;
		Status : RBTemplateRestApiIntStatusType;
		Data : RBTemplateRestApiIntDataType;
		FUB : RBTemplateRestApiIntFubType;
	END_STRUCT;
END_TYPE
