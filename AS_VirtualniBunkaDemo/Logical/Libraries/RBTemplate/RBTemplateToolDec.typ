
TYPE
	RBTemplateToolDecInternalType : 	STRUCT 
		Status : ToolDecInternalStatusType;
		FUB : ToolDecInternalFUBType;
		Data : ToolDecInternalDataType;
		State : USINT;
	END_STRUCT;
	ToolDecInternalDataType : 	STRUCT 
		ReadData : ARRAY[0..299]OF USINT;
		TemporaryRow : STRING[200];
		Row : STRING[200];
		StartStringPosition : INT;
		StringPosition : UDINT;
		CutRow : STRING[200];
		StringToReal : STRING[20];
		StartString : STRING[10];
		EndString : STRING[10];
		Increment : USINT;
		ReadOffset : UDINT;
		RowNumber : INT;
		ToolsOffset : ARRAY[0..rbTEMPLATE_MAX_TOOLS_MIN_1]OF UDINT;
		ToolIndex : INT;
		EnableOld : BOOL;
		StringLength : INT;
		FirstStringLength : INT;
		FileInfo : fiFILE_INFO;
	END_STRUCT;
	ToolDecInternalStatusType : 	STRUCT 
		EndStringFound : BOOL;
		StartStringFound : BOOL;
		RobholdFalseFound : BOOL;
		RobholdTrueFound : BOOL;
		FindOffsetsDone : BOOL;
		FindNamesDone : BOOL;
	END_STRUCT;
	ToolDecInternalFUBType : 	STRUCT 
		FileOpen_0 : FileOpen;
		FileInfo_0 : FileInfo;
		FileRead_0 : FileRead;
		FileClose_0 : FileClose;
	END_STRUCT;
	RBTemplateToolDecStateEnum : 
		(
		rbtTOOL_DEC_DISABLED := 0, (*Function block is not enabled*)
		rbtTOOL_DEC_WAIT_FOR_ENABLED := 10, (*Waiting for function block enable*)
		rbtTOOL_DEC_WAIT_FOR_CMD := 20, (*Waiting for command*)
		rbtTOOL_DEC_OPEN_FILE := 30, (*Open file with name in FileName*)
		rbtTOOL_DEC_GET_FILE_INFO := 35, (*Get info of FileName*)
		rbtTOOL_DEC_READ_DATA_FOR_NAMES := 40, (*Read data to get tool names*)
		rbtTOOL_DEC_FIND_OFFSETS := 50, (*Find offset of tools*)
		rbtTOOL_DEC_FIND_NAMES := 60, (*Find name of tools*)
		rbtTOOL_DEC_CHECK_NAME := 70, (*Look if ToolName exists*)
		rbtTOOL_DEC_READ_DATA_FOR_PARAMS := 80, (*Read data to get tool parameters*)
		rbtTOOL_DEC_SEPARATE_ROW := 90, (*Separate row from readed data*)
		rbtTOOL_DEC_FIND_TOOL_NAME := 100, (*Find tool name*)
		rbtTOOL_DEC_FIND_ROBHOLD := 110, (*Find robot hold status*)
		rbtTOOL_DEC_FIND_PARAMS := 120, (*Find parameters of tool*)
		rbtTOOL_DEC_FIND_VALUE := 130, (*Find value between StartString and EndString*)
		rbtTOOL_DEC_CLOSE_FILE := 140, (*Close file*)
		rbtTOOL_DEC_ERROR := 255 (*Error step*)
		);
	RBTemplateToolDecStatusIDEnum : 
		( (*Type of information / Description / Suggestion*)
		rbtTOOL_DEC_FUB_OK := 0, (*State / OK*)
		rbtTOOL_DEC_FUB_BUSY := 65535, (*State / Busy*)
		rbtTOOL_DEC_ERR_NO_TOOLS := 45000, (*Error ToolDec lib / No tools found / Check if the file has any tools*)
		rbtTOOL_DEC_ERR_BAD_TOOL_SYNTAX := 45001, (*Error ToolDec lib / Some tool has bad syntax / Check tools syntax (probably bad syntax of tool with index Internal.Data.RowNumber, indexing is gradual and starts with zero)*)
		rbtTOOL_DEC_ERR_BAD_TOOL_NAME := 45002, (*Error ToolDec lib / Entered tool name not found / Check correctness of entered tool name*)
		rbtTOOL_DEC_ERR_TOO_MANY_TOOLS := 45003, (*Error ToolDec lib / Too many tools in file / Increase the number of rbTEMPLATE_MAX_TOOLS constant or delete some tools from file*)
		rbtTOOL_DEC_ERR_ROW_END_NOT_FND := 45004, (*Error ToolDec lib / Some row doesnt have semicolon / Check the semicolons at the end of the rows*)
		rbtTOOL_DEC_ERR_BAD_TOOL_INDEX := 45005, (*Error ToolDec lib / Tool index is higher then the number of tools / Run the ReadToolNames command and try again*)
		rbtTOOL_DEC_ERR_ROBH_TR_AND_FALS := 45006, (*Error ToolDec lib / Robot hold is true and also false / Check robot hold syntax*)
		rbtTOOL_DEC_ERR_ROBH_NOT_FOUND := 45007, (*Error ToolDec lib / Robot hold not found / Check robot hold syntax*)
		rbtTOOL_DEC_ERR_BAD_PARAMS_SNTX := 45008 (*Error ToolDec lib / Bad parameters syntax / Check syntax of parameters (except ToolName and Robhold )*)
		);
END_TYPE
