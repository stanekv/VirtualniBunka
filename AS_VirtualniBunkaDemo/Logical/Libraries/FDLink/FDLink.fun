(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: FDLink
 * File: FDLink.fun
 * Author: markvartv
 * Created: April 15, 2015
 ********************************************************************
 * Functions and function blocks of library FDLink
 ********************************************************************)

FUNCTION_BLOCK FDLinkBasic (*File Device Link Basic*) (*$GROUP=User*)
	VAR_INPUT
		pCSComLink : REFERENCE TO CSComLinkType; (*Custom Solution link*) (* *) (*#PAR#OPT#;*)
		Enable : BOOL; (*Enables/Disables function block*) (* *) (*#PAR#;*)
		ErrorReset : BOOL; (*Reset all function block error*) (* *) (*#PAR#;*)
		AutoResetCmd : BOOL; (*Turn on autoreset commands*) (* *) (*#PAR#OPT#;*)
		Configuration : REFERENCE TO FDLinkBasicConfigurationType; (*Pointer to the configuration*) (* *) (*#PAR#;*)
		Path : REFERENCE TO STRING[255]; (*Pointer to the path*) (* *) (*#CMD#;*)
		PathSim : REFERENCE TO STRING[255]; (*Pointer to the simulation path*) (* *) (*#CMD#;*)
		SplitType : FDLinkBasicSplitTypeEnum; (*Define how  the path will be split. By default the entire path doesn't include the file name*) (* *) (*#CMD#;*)
		SelectUSB : USINT; (*Select connected USB device(from the 1st connected USB device to nth connected USB device)*) (* *) (*#CMD#;*)
		LinkPath : BOOL; (*Link device and create path*) (* *) (*#CMD#;*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#PAR#;*)
		Busy : BOOL; (*Function block is busy*) (* *) (*#PAR#;*)
		Error : BOOL; (*Indicates error*) (* *) (*#PAR#;*)
		StatusID : DINT; (*Error/Status information*) (* *) (*#PAR#;*)
		ARSimUsed : BOOL; (*AR simulation is used*) (* *) (*#CMD#;*)
		LinkPathDone : BOOL; (*Link device is done*) (* *) (*#CMD#;*)
		Info : FDLinkInfoType; (*Structure with information*) (* *) (*#CMD#;*)
	END_VAR
	VAR
		Internal : FDLinkBasicInternalType; (*Internal data type*) (* *) (*#CMD#;*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FDLinkBasicCopy (*File Device Link Basic Copy*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pCSComLink : REFERENCE TO CSComLinkType; (*Custom Solution link*) (* *) (*#PAR#OPT#;*)
		Enable : BOOL; (*Enables/Disables function block*) (* *) (*#PAR#;*)
		ErrorReset : BOOL; (*Reset all function block error*) (* *) (*#PAR#;*)
		AutoResetCmd : BOOL; (*Turn on autoreset commands*) (* *) (*#PAR#OPT#;*)
		Configuration : REFERENCE TO FDLinkBasicCopyConfigurationType; (*Pointer to the configuration*) (* *) (*#PAR#;*)
		SrcPath : REFERENCE TO STRING[255]; (*Pointer to the source path*) (* *) (*#CMD#;*)
		SrcPathSim : REFERENCE TO STRING[255]; (*Pointer to the source simulation path*) (* *) (*#CMD#;*)
		DestPath : REFERENCE TO STRING[255]; (*Pointer to the destination path*) (* *) (*#CMD#;*)
		DestPathSim : REFERENCE TO STRING[255]; (*Pointer to the destination simulation path*) (* *) (*#CMD#;*)
		CopyType : FDLinkBasicCopyTypeEnum; (*Defined what will be copied, if file or directory. By default only file will be copied.*) (* *) (*#CMD#;*)
		SelectUSB : USINT; (*Select connected USB device(from the 1st connected USB device to nth connected USB device)*) (* *) (*#CMD#;*)
		FileRename : REFERENCE TO STRING[255]; (*File rename*) (* *) (*#CMD#;*)
		ForceFileRename : BOOL; (*Force file rename*) (* *) (*#CMD#;*)
		ForceFileOverwrite : BOOL; (*Force file overwrite*) (* *) (*#CMD#;*)
		ForceDirOverwrite : BOOL; (*Force directory overwrite*) (* *) (*#CMD#;*)
		CopyToDest : BOOL; (*Copy to destination path*) (* *) (*#CMD#;*)
		CopyToSrc : BOOL; (*Copy to source path*) (* *) (*#CMD#;*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#PAR#;*)
		Busy : BOOL; (*Function block is busy*) (* *) (*#PAR#;*)
		Error : BOOL; (*Indicates error*) (* *) (*#PAR#;*)
		StatusID : DINT; (*Error/Status information*) (* *) (*#PAR#;*)
		ARSimUsed : BOOL; (*AR simulation is used*) (* *) (*#CMD#;*)
		CopyToDestDone : BOOL; (*Copy to destination is done*) (* *) (*#CMD#;*)
		CopyToSrcDone : BOOL; (*Copy to sourse is done*) (* *) (*#CMD#;*)
		Info : FDLinkBasicCopyInfoType; (*Structure with information*) (* *) (*#CMD#;*)
	END_VAR
	VAR
		Internal : FDLInkBasicCopyInternalType; (*Internal data type*) (* *) (*#CMD#;*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION FDLinkPathSplit : DINT (*File Device Link Path Split*) (*$GROUP=User*)
	VAR_INPUT
		SplitType : UINT; (*Split path type*) (* *) (*#PAR#;*)
		Path : STRING[255]; (*Pointer to the path*) (* *) (*#PAR#;*)
	END_VAR
	VAR_IN_OUT
		SplitPathPart : FDLinkBasicSplitPathPartsType; (*Split path parts*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkCreateParam : DINT (*File Device Link Create Param*) (*$GROUP=User*)
	VAR_INPUT
		Parameters : REFERENCE TO FDLinkCreateParamType; (*Parameters for create param string for device link*) (* *) (*#PAR#;*)
	END_VAR
	VAR_IN_OUT
		CreatedParam : FDLinkCreateParamFoldedType; (*Created param string for device link*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkPathType : DINT (*File Device Link Path Type*) (*$GROUP=User*)
	VAR_INPUT
		Path : STRING[255]; (*Pointer to the path*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkChangePathType : DINT (*File Device Link Change Path Type*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Path : STRING[255]; (*Pointer to the path*) (* *) (*#PAR#;*)
		ChangePathType : FDLinkCreateParamDeviceTypeEnum; (*Changer path type*) (* *) (*#PAR#;*)
		LocalDeviceLetter : STRING[1]; (*Pointer to the local device letter*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkArchiveAddExt : DINT (*File Device Link Archive Add Extension*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		ArchiveFile : STRING[255]; (*Archive File string*) (* *) (*#PAR#;*)
		ArchiveTypeExt : FDLinkBasicCopyExtensionTypeEnum; (*Archive File extension type*) (* *) (*#PAR#;*)
		AddType : FDLinkBasicExtensionAddTypeEnum; (*Add type*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkFileAddExt : DINT (*File Device Link File Add Extension*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		File : STRING[255]; (*File name*) (* *) (*#PAR#;*)
		Extension : STRING[7]; (*File extension*) (* *) (*#PAR#;*)
		AddType : FDLinkBasicExtensionAddTypeEnum; (*Add type*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkFileRemoveExt : DINT (*File Device Link File Remove Extension*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		File : STRING[255]; (*File name*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkFindPathIndx : DINT (*File Device Link Find Path Index*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		FDInfoBuffer : FDLinkInfoBufferType; (*Pointer to file devices info buffer*) (* *) (*#PAR#;*)
		Path : STRING[255]; (*Path*) (* *) (*#PAR#;*)
		SimPath : STRING[255]; (*Simulation Path*) (* *) (*#PAR#;*)
		Index : REFERENCE TO USINT; (*Pointer to index*) (* *) (*#PAR#;*)
		DeviceType : REFERENCE TO FDLinkCreateParamDeviceTypeEnum; (*Pointer to device type type*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkCreatePath : DINT (*File Device Link Create Path*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Path : STRING[255]; (*Path*) (* *) (*#PAR#;*)
		DeviceName : STRING[50]; (*Device name*) (* *) (*#PAR#;*)
		PathPart : STRING[255]; (*Path without device name*) (* *) (*#PAR#;*)
		Level : REFERENCE TO UINT; (*Pointer to the level*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkPathLevel : DINT (*File Device Link Path Level*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Path : STRING[255]; (*Path*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkUSBNum : DINT (*File Device Link USB Number*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Path : STRING[255]; (*Path*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkFindPath : DINT (*File Device Link Find Path*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		FDInfoBuffer : FDLinkInfoBufferType; (*Pointer to file devices info buffer*) (* *) (*#PAR#;*)
		Path : STRING[255]; (*Path*) (* *) (*#PAR#;*)
		Device : STRING[50]; (*Device*) (* *) (*#PAR#;*)
		Index : REFERENCE TO USINT; (*Pointer to index*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkFindPathBoth : DINT (*File Device Link Find Path Both*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		FDInfoBuffer : FDLinkInfoBufferType; (*Pointer to file devices info buffer*) (* *) (*#PAR#;*)
		Path : STRING[255]; (*Path*) (* *) (*#PAR#;*)
		SimPath : STRING[255]; (*Simulation Path*) (* *) (*#PAR#;*)
		Device : STRING[50]; (*Device*) (* *) (*#PAR#;*)
		Index : REFERENCE TO USINT; (*Pointer to index*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkConfigPath : DINT (*File Device Link Config Path*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		FDConfigBuffer : FDLinkConfigBufferType; (*Pointer to file devices config buffer*) (* *) (*#PAR#;*)
		Path : STRING[255]; (*Path*) (* *) (*#PAR#;*)
		SimPath : STRING[255]; (*Simulation Path*) (* *) (*#PAR#;*)
		Index : REFERENCE TO USINT; (*Pointer to index*)
	END_VAR
END_FUNCTION

FUNCTION FDLinkConfigPathAll : DINT (*File Device Link Config Path All*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		FDConfigBuffer : FDLinkConfigBufferType; (*Pointer to file devices config buffer*) (* *) (*#PAR#;*)
		ConfigFileDevice : FDLinkConfigFileDevType; (*Config file device*) (* *) (*#PAR#;*)
		Index : REFERENCE TO USINT; (*Pointer to index*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION_BLOCK FDLinkDirHandle (*File Device Link Directory Handle*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pCSComLink : REFERENCE TO CSComLinkType; (*Custom Solution link*) (* *) (*#PAR#OPT#;*)
		Enable : BOOL; (*Enables/Disables function block*) (* *) (*#PAR#;*)
		ErrorReset : BOOL; (*Reset all function block error*) (* *) (*#PAR#;*)
		AutoResetCmd : BOOL; (*Turn on autoreset commands*) (* *) (*#PAR#OPT#;*)
		pConfiguration : REFERENCE TO FDLinkDirHandleConfigurationType; (*Pointer to configuration*) (* *) (*#PAR#;*)
		pDevice : REFERENCE TO STRING[50]; (*Pointer to device*) (* *) (*#CMD#;*)
		pPath : REFERENCE TO STRING[255]; (*Pointer to path*) (* *) (*#CMD#;*)
		Delete : BOOL; (*Delete*) (* *) (*#CMD#;*)
		DeleteAll : BOOL; (*Delete all*) (* *) (*#CMD#;*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#PAR#;*)
		Busy : BOOL; (*Function block is busy*) (* *) (*#PAR#;*)
		Error : BOOL; (*Indicates error*) (* *) (*#PAR#;*)
		StatusID : DINT; (*Error/Status information*) (* *) (*#PAR#;*)
		DeleteDone : BOOL; (*Delete done*) (* *) (*#CMD#;*)
		DeleteAllDone : BOOL; (*Delete all done*) (* *) (*#CMD#;*)
		Info : FDLinkDirHandleInfoType; (*Structure with information*) (* *) (*#CMD#;*)
	END_VAR
	VAR
		Internal : FDLinkDirHandleInternalType; (*Internal data type*) (* *) (*#CMD#;*)
	END_VAR
END_FUNCTION_BLOCK
