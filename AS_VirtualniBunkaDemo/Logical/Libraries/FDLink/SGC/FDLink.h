/* Automation Studio generated header file */
/* Do not edit ! */
/* FDLink 5.01.0 */

#ifndef _FDLINK_
#define _FDLINK_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _FDLink_VERSION
#define _FDLink_VERSION 5.01.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG3
		#include "AsBrStr.h"
		#include "AsIODiag.h"
		#include "AsUSB.h"
		#include "AsZip.h"
		#include "FileIO.h"
		#include "standard.h"
		#include "CSCom.h"
#endif
#ifdef _SG4
		#include "AsBrStr.h"
		#include "AsIODiag.h"
		#include "AsUSB.h"
		#include "AsZip.h"
		#include "FileIO.h"
		#include "standard.h"
		#include "CSCom.h"
#endif
#ifdef _SGC
		#include "AsBrStr.h"
		#include "AsIODiag.h"
		#include "AsUSB.h"
		#include "AsZip.h"
		#include "FileIO.h"
		#include "standard.h"
		#include "CSCom.h"
#endif


/* Constants */
#ifdef _REPLACE_CONST
 #define fdLINK_NUMBER_NESTED_DIRECTORY 10U
 #define fdLINK_PATH_STRING_LENGHT 255U
 #define fdLINK_PATH_DEVICE_LENGHT 50U
 #define fdLINK_EXTENTION_STRING_LENGHT 7U
 #define fdLINK_DIRECTORY_SEPARATOR 92U
 #define fdLINK_DEVICE_IDENTIFICATOR 58U
 #define fdLINK_DEVICE_USB_IDENTIFICATOR 47U
 #define fdLINK_EXTENSION_IDENTIFICATOR 46U
 #define fdLINK_NETWORK_CON_TIMEOUT 120U
 #define fdLINK_DIR_READ_FILE_NUM 50U
 #define fdLINK_DIR_FILE_NUM 10U
 #define fdLINK_CS_COM_MEM_VALID_TIME_OUT 45.0f
 #define fdLINK_DIR_READ_ERR_NUM 10U
 #define fdLINK_FILE_DEVICE_BUF_NUM 20U
 #define fdLINK_FILE_DEVICE_NUM_BUF_INDX 19U
 #define fdLINK_FILE_DEV_CFG_BUF_PV_NAME "FileMan:FileMan.FDConfigBuffer"
 #define fdLINK_FILE_DEV_INFO_BUF_PV_NAME "FileMan:FileMan.FDInfoBuffer"
#else
 _GLOBAL_CONST unsigned char fdLINK_NUMBER_NESTED_DIRECTORY;
 _GLOBAL_CONST unsigned char fdLINK_PATH_STRING_LENGHT;
 _GLOBAL_CONST unsigned char fdLINK_PATH_DEVICE_LENGHT;
 _GLOBAL_CONST unsigned char fdLINK_EXTENTION_STRING_LENGHT;
 _GLOBAL_CONST unsigned char fdLINK_DIRECTORY_SEPARATOR;
 _GLOBAL_CONST unsigned char fdLINK_DEVICE_IDENTIFICATOR;
 _GLOBAL_CONST unsigned char fdLINK_DEVICE_USB_IDENTIFICATOR;
 _GLOBAL_CONST unsigned char fdLINK_EXTENSION_IDENTIFICATOR;
 _GLOBAL_CONST unsigned char fdLINK_NETWORK_CON_TIMEOUT;
 _GLOBAL_CONST unsigned short fdLINK_DIR_READ_FILE_NUM;
 _GLOBAL_CONST unsigned short fdLINK_DIR_FILE_NUM;
 _GLOBAL_CONST float fdLINK_CS_COM_MEM_VALID_TIME_OUT;
 _GLOBAL_CONST unsigned long fdLINK_DIR_READ_ERR_NUM;
 _GLOBAL_CONST unsigned char fdLINK_FILE_DEVICE_BUF_NUM;
 _GLOBAL_CONST unsigned char fdLINK_FILE_DEVICE_NUM_BUF_INDX;
 _GLOBAL_CONST plcstring fdLINK_FILE_DEV_CFG_BUF_PV_NAME[51];
 _GLOBAL_CONST plcstring fdLINK_FILE_DEV_INFO_BUF_PV_NAME[51];
#endif




/* Datatypes and datatypes of function blocks */
typedef enum FDLinkSeveritiesEnum
{	fdLINK_SEV_SUCCESS = 0,
	fdLINK_SEV_INFORMATIONAL = 1,
	fdLINK_SEV_WARNING = 2,
	fdLINK_SEV_ERROR = 3
} FDLinkSeveritiesEnum;

typedef enum FDLinkFacilitiesEnum
{	fdLINK_FAC_BASIC = 270
} FDLinkFacilitiesEnum;

typedef enum FDLinkBasicSplitTypeEnum
{	fdLINK_BASIC_PATH_WITHOUT_FILE = 0,
	fdLINK_BASIC_PATH_WITH_FILE = 1
} FDLinkBasicSplitTypeEnum;

typedef enum FDLinkBasicUsbSelectModeEnum
{	fdLINK_FIND_FIRST_FREE_CON_DEV = 0,
	fdLINK_FIND_LAST_FREE_CON_DEV = 1
} FDLinkBasicUsbSelectModeEnum;

typedef enum FDLinkBasicStateEnum
{	fdLINK_BASIC_NOT_ENABLED = 0,
	fdLINK_BASIC_W_SIM_DIAG = 5,
	fdLINK_BASIC_COLLECT_INFO_CPU = 10,
	fdLINK_BASIC_GET_INFO_CPU = 11,
	fdLINK_BASIC_DISPOSE_INFO_CPU = 12,
	fdLINK_BASIC_WAIT_CMD = 100,
	fdLINK_BASIC_CNTRL_AR_SIM_USED = 110,
	fdLINK_BASIC_SPLIT_PATH = 150,
	fdLINK_BASIC_DETERMINE_ROOT_TYPE = 155,
	fdLINK_BASIC_USB_NODE_ID_LIST = 160,
	fdLINK_BASIC_USB_READ_DATA = 165,
	fdLINK_BASIC_USB_READ_DESCRIPTOR = 170,
	fdLINK_BASIC_SELECT_USB_DEVICE = 175,
	fdLINK_BASIC_USB_DEVICE_READY = 180,
	fdLINK_BASIC_SPLIT_SIM_PATH = 190,
	fdLINK_BASIC_CREATE_PARAM_ROOT = 200,
	fdLINK_BASIC_FILL_DEV_NAME_ROOT = 205,
	fdLINK_BASIC_CNTRL_DEVICE_HANDLE = 210,
	fdLINK_BASIC_UNLINK_DEVICE_ROOT = 215,
	fdLINK_BASIC_LINK_DEVICE_ROOT = 220,
	fdLINK_BASIC_SAVE_DEV_HNDL_ROOT = 221,
	fdLINK_BASIC_GET_DIR_INFO = 225,
	fdLINK_BASIC_DIR_CREATE = 230,
	fdLINK_BASIC_CHECK_LINK_DEV_PATH = 235,
	fdLINK_BASIC_CREATE_PARAM_PATH = 240,
	fdLINK_BASIC_CNTRL_AR_SIM_USED_2 = 245,
	fdLINK_BASIC_DETERMINE_PATH_TYPE = 250,
	fdLINK_BASIC_FILL_DEV_NAME_PATH = 255,
	fdLINK_BASIC_LINK_DEVICE_PATH = 260,
	fdLINK_BASIC_SAVE_DEV_HNDL_PATH = 261,
	fdLINK_BASIC_CNTRL_DEV_NOT_ENBL = 950,
	fdLINK_BASIC_UNLINK_DEV_NOT_ENBL = 951,
	fdLINK_BASIC_ERROR_RESET = 1000
} FDLinkBasicStateEnum;

typedef enum FDLinkCreateParamDeviceTypeEnum
{	fdLINK_CREATE_PARAM_DEVICE_LOCAL = 0,
	fdLINK_CREATE_PARAM_DEVICE_FTP = 1,
	fdLINK_CREATE_PARAM_DEVICE_NFS = 2,
	fdLINK_CREATE_PARAM_DEVICE_CIFS = 3,
	fdLINK_CREATE_PARAM_DEVICE_USB = 4
} FDLinkCreateParamDeviceTypeEnum;

typedef enum FDLinkBasicCopyTypeEnum
{	fdLINK_BASIC_COPY_FILE = 0,
	fdLINK_BASIC_COPY_DIRECTORY = 1
} FDLinkBasicCopyTypeEnum;

typedef enum FDLinkBasicCopyDirModeEnum
{	fdLINK_BASIC_COPY_DIR_RECURSIVE = 1,
	fdLINK_BASIC_COPY_DIR_OVERWRITE = 2
} FDLinkBasicCopyDirModeEnum;

typedef enum FDLinkBasicCopyFileModeEnum
{	fdLINK_BASIC_COPY_FILE_KEEP_OLD = 0,
	fdLINK_BASIC_COPY_FILE_OVERWRITE = 2
} FDLinkBasicCopyFileModeEnum;

typedef enum FDLinkBasicCopyArchiveModeEnum
{	fdLINK_BASIC_COPY_ARCHIVE = 0,
	fdLINK_BASIC_COPY_EXTRACT = 1
} FDLinkBasicCopyArchiveModeEnum;

typedef enum FDLinkBasicCopyCompLevelEnum
{	fdLINK_BASIC_COPY_COMP_LEVEL_0 = 0,
	fdLINK_BASIC_COPY_COMP_LEVEL_1 = 1,
	fdLINK_BASIC_COPY_COMP_LEVEL_2 = 2,
	fdLINK_BASIC_COPY_COMP_LEVEL_3 = 3,
	fdLINK_BASIC_COPY_COMP_LEVEL_4 = 4,
	fdLINK_BASIC_COPY_COMP_LEVEL_5 = 5,
	fdLINK_BASIC_COPY_COMP_LEVEL_6 = 6,
	fdLINK_BASIC_COPY_COMP_LEVEL_7 = 7,
	fdLINK_BASIC_COPY_COMP_LEVEL_8 = 8,
	fdLINK_BASIC_COPY_COMP_LEVEL_9 = 9
} FDLinkBasicCopyCompLevelEnum;

typedef enum FDLinkBasicCopyExtensionTypeEnum
{	fdLINK_BASIC_COPY_EXT_TAR = 0,
	fdLINK_BASIC_COPY_EXT_TAR_GZ = 1,
	fdLINK_BASIC_COPY_EXT_GZ = 2
} FDLinkBasicCopyExtensionTypeEnum;

typedef enum FDLinkBasicExtensionAddTypeEnum
{	fdLINK_BASIC_KEEP_OLD_EXT = 0,
	fdLINK_BASIC_OVERWRITE_EXT = 1
} FDLinkBasicExtensionAddTypeEnum;

typedef enum FDLinkBasicCopyExtractModeEnum
{	fdLINK_BASIC_EXTRACT_DEVICE = 0,
	fdLINK_BASIC_EXTRACT_FOLDER = 1
} FDLinkBasicCopyExtractModeEnum;

typedef enum FDLinkBasicCopyStateEnum
{	fdLINK_BASIC_CPY_NOT_ENABLED = 0,
	fdLINK_BASIC_CPY_ENABLE_SRC = 10,
	fdLINK_BASIC_CPY_ENABLE_DEST = 11,
	fdLINK_BASIC_CPY_WAIT_CMD = 100,
	fdLINK_BASIC_CPY_LINK_SRC_PATH = 150,
	fdLINK_BASIC_CPY_DET_SRC_PATH = 155,
	fdLINK_BASIC_CPY_CNTRL_DEST_PATH = 160,
	fdLINK_BASIC_CPY_CNTRL_DEST_P_S = 161,
	fdLINK_BASIC_CPY_LINK_DEST_PATH = 162,
	fdLINK_BASIC_CPY_DET_DEST_PATH = 165,
	fdLINK_BASIC_CPY_FILL_DEV_NAME = 170,
	fdLINK_BASIC_CPY_TYPE_CNTRL = 175,
	fdLINK_BASIC_CPY_FILE = 180,
	fdLINK_BASIC_CPY_DIR = 185,
	fdLINK_BASIC_CPY_ARCHIVE_ADD_EXT = 190,
	fdLINK_BASIC_CPY_ARCHIVE_DEST = 191,
	fdLINK_BASIC_CPY_ARCHIVE_SRC = 192,
	fdLINK_BASIC_CPY_FILL_DEV_NAME_A = 193,
	fdLINK_BASIC_CPY_ARCHIVE_ADD_E_S = 194,
	fdLINK_BASIC_CPY_ARCHIVE = 195,
	fdLINK_BASIC_CPY_ARCHIVE_COPY = 196,
	fdLINK_BASIC_CPY_ARCHIVE_DELETE = 197,
	fdLINK_BASIC_CPY_EXTRACT_ADD_EXT = 210,
	fdLINK_BASIC_CPY_EXTRACT_RMV_EXT = 211,
	fdLINK_BASIC_CPY_CNTRL_EXTR_MODE = 212,
	fdLINK_BASIC_CPY_EXTRACT_DEST = 213,
	fdLINK_BASIC_CPY_EXTRACT_SRC = 214,
	fdLINK_BASIC_CPY_FILL_DEV_NAME_E = 215,
	fdLINK_BASIC_CPY_EXTRACT_ADD_E_S = 216,
	fdLINK_BASIC_CPY_EXTRACT_COPY = 217,
	fdLINK_BASIC_CPY_EXTRACT = 218,
	fdLINK_BASIC_CPY_EXTRACT_DELETE = 219,
	fdLINK_BASIC_CPY_DISABLE_SRC = 950,
	fdLINK_BASIC_CPY_DISABLE_DEST = 951,
	fdLINK_BASIC_CPY_ERROR_RESET = 1000
} FDLinkBasicCopyStateEnum;

typedef enum FDLinkDirHandleStateEnum
{	fdLINK_DIR_NOT_ENABLED = 0,
	fdLINK_DIR_WAIT_CMD = 100,
	fdLINK_DIR_READ_INIT = 110,
	fdLINK_DIR_READ = 111,
	fdLINK_DIR_MEM_INFO = 112,
	fdLINK_DIR_SORT_ARRAY = 115,
	fdLINK_DIR_FILE_DELETE = 120,
	fdLINK_DIR_ERROR_RESET = 1000
} FDLinkDirHandleStateEnum;

typedef enum FDLinkErrorEnum
{	fdLINK_NO_ERROR = 0,
	fdLINK_ERR_INVALID_PATH = -519163422,
	fdLINK_ERR_INVALID_ARG_SPLIT_FCE = -519163421,
	fdLINK_ERR_INVALID_DEVICE = -519163420,
	fdLINK_ERR_DIRECTORY_NAME_LONG = -519163419,
	fdLINK_ERR_TOO_MANY_NESTED_DIR = -519163418,
	fdLINK_WRN_DEVICE_NOT_FOUND_PATH = -1592905241,
	fdLINK_ERR_DIRECTORY_PATH_LONG = -519163416,
	fdLINK_ERR_DIRECTORY_CREATE_LONG = -519163415,
	fdLINK_ERR_INVALID_ARG_PARAM_FCE = -519163414,
	fdLINK_ERR_INVALID_CREATE_PARAM = -519163413,
	fdLINK_ERR_DEVICE_NOT_FOUND_PATH = -519163412,
	fdLINK_ERR_LINK_DEVICE_ROOT = -519163411,
	fdLINK_ERR_GET_DIR_INFO = -519163410,
	fdLINK_ERR_GET_DIR_CREATE = -519163409,
	fdLINK_ERR_PATH_DEVICE_LONG = -519163408,
	fdLINK_ERR_LINK_DEVICE_PATH = -519163407,
	fdLINK_ERR_COLLECT_INFO_CPU = -519163406,
	fdLINK_ERR_GET_INFO_CPU = -519163405,
	fdLINK_ERR_INVALID_SIM_PATH = -519163404,
	fdLINK_ERR_DEV_NOT_FOUND_SIMPATH = -519163403,
	fdLINK_ERR_NOT_ALLOW_DEVICE_SIM = -519163402,
	fdLINK_ERR_INVALID_ARG_TYPE_FCE = -519163401,
	fdLINK_ERR_UNLINK_DEVICE_ROOT = -519163400,
	fdLINK_ERR_LINK_DEV_ROOT_TIMEOUT = -519163399,
	fdLINK_ERR_LINK_DEV_PATH_TIMEOUT = -519163398,
	fdLINK_ERR_UNLINK_DEV_PATH_T_OUT = -519163397,
	fdLINK_ERR_GET_DIR_INFO_TIMEOUT = -519163396,
	fdLINK_ERR_CREATE_DIR_TIMEOUT = -519163395,
	fdLINK_ERR_USB_NODE_ID_LIST = -519163394,
	fdLINK_ERR_USB_READ_DATA = -519163393,
	fdLINK_ERR_USB_READ_DESCRIPTOR = -519163392,
	fdLINK_ERR_USB_DEVICE_READY = -519163391,
	fdLINK_ERR_USB_NOT_CONNECTED = -519163390,
	fdLINK_ERR_USB_NOT_CON_ON_POS = -519163389,
	fdLINK_ERR_USB_GUARD_CON_ON_POS = -519163388,
	fdLINK_ERR_UNLINK_DEV_NOT_ENABLE = -519163387,
	fdLINK_ERR_USB_DEVICE_NOT_READY = -519163386,
	fdLINK_ERR_COPY_PATH_TOO_LONG = -519163385,
	fdLINK_ERR_COPY_FILE = -519163384,
	fdLINK_ERR_COPY_FILE_TIMEOUT = -519163383,
	fdLINK_ERR_COPY_DIR = -519163382,
	fdLINK_ERR_COPY_DIR_TIMEOUT = -519163381,
	fdLINK_ERR_INVALID_LOCAL_DEV_LET = -519163380,
	fdLINK_ERR_ARCHIVE = -519163379,
	fdLINK_ERR_ARCHIVE_TIMEOUT = -519163378,
	fdLINK_ERR_EXTRACT = -519163377,
	fdLINK_ERR_EXTRACT_TIMEOUT = -519163376,
	fdLINK_ERR_INVALID_ARG_EXT_FCE = -519163375,
	fdLINK_ERR_COPY_CRT_DEST_S_PATH = -519163374,
	fdLINK_ERR_INVALID_ARCH_SRC_PATH = -519163373,
	fdLINK_ERR_DEL_FILE = -519163372,
	fdLINK_ERR_DEL_FILE_TIMEOUT = -519163371,
	fdLINK_ERR_DISPOSE_INFO_CPU = -519163370,
	fdLINK_ERR_INVALID_ARG_FPI_FCE = -519163369,
	fdLINK_ERR_FPI_FCE_INDEX_N_FOUND = -519163368,
	fdLINK_ERR_INVALID_ARG_CP_FCE = -519163367,
	fdLINK_ERR_INVALID_ARG_FP_FCE = -519163366,
	fdLINK_ERR_DIR_READ = -519163365,
	fdLINK_ERR_DEV_MEM_INFO = -519163364,
	fdLINK_ERR_INVALID_ARG_CFGP_FCE = -519163363,
	fdLINK_ERR_CP_FCE_INDEX_N_FOUND = -519163362,
	fdLINK_ERR_INVALID_ARG_CFGPA_FCE = -519163361,
	fdLINK_ERR_CPA_FCE_INDEX_N_FOUND = -519163360
} FDLinkErrorEnum;

typedef struct FDLinkStatusIDType
{	enum FDLinkErrorEnum ID;
	enum FDLinkSeveritiesEnum Severity;
	unsigned short Code;
} FDLinkStatusIDType;

typedef struct FDLinkInternalIDType
{	signed long ID;
	enum FDLinkSeveritiesEnum Severity;
	signed long Facility;
	unsigned short Code;
} FDLinkInternalIDType;

typedef struct FDLinkDiagType
{	struct FDLinkStatusIDType StatusID;
	struct FDLinkInternalIDType Internal;
} FDLinkDiagType;

typedef struct FDLinkBasicDeviceLocalNamesType
{	plcstring Root[51];
	plcstring Path[51];
} FDLinkBasicDeviceLocalNamesType;

typedef struct FDLinkBasicDeviceLocalType
{	struct FDLinkBasicDeviceLocalNamesType Names;
} FDLinkBasicDeviceLocalType;

typedef struct FDLinkBasicDeviceNetworkType
{	plcstring Name[51];
} FDLinkBasicDeviceNetworkType;

typedef struct FDLinkBasicDeviceUSBType
{	plcstring Name[51];
	unsigned long ConnectNum;
	unsigned char LinkedNum;
} FDLinkBasicDeviceUSBType;

typedef struct FDLinkBasicDeviceType
{	struct FDLinkBasicDeviceLocalType Local;
	struct FDLinkBasicDeviceNetworkType FTP;
	struct FDLinkBasicDeviceNetworkType NFS;
	struct FDLinkBasicDeviceNetworkType CIFS;
	struct FDLinkBasicDeviceUSBType USB;
} FDLinkBasicDeviceType;

typedef struct FDLinkBasicPathLocalNetworkType
{	plcstring Name[256];
} FDLinkBasicPathLocalNetworkType;

typedef struct FDLinkBasicPathType
{	struct FDLinkBasicPathLocalNetworkType Local;
	struct FDLinkBasicPathLocalNetworkType FTP;
	struct FDLinkBasicPathLocalNetworkType NFS;
	struct FDLinkBasicPathLocalNetworkType CIFS;
	struct FDLinkBasicPathLocalNetworkType USB;
} FDLinkBasicPathType;

typedef struct FDLinkBasicFileLocalNetworkType
{	plcstring Name[256];
} FDLinkBasicFileLocalNetworkType;

typedef struct FDLinkBasicFileType
{	struct FDLinkBasicFileLocalNetworkType Local;
	struct FDLinkBasicFileLocalNetworkType FTP;
	struct FDLinkBasicFileLocalNetworkType NFS;
	struct FDLinkBasicFileLocalNetworkType CIFS;
	struct FDLinkBasicFileLocalNetworkType USB;
} FDLinkBasicFileType;

typedef struct FDLinkInfoType
{	struct FDLinkBasicDeviceType Devices;
	struct FDLinkBasicPathType Paths;
	struct FDLinkBasicFileType Files;
	struct FDLinkDiagType Diag;
} FDLinkInfoType;

typedef struct FDLinkBasicSplitPathPartsType
{	plcstring Device[51];
	plcstring DirectoryName[11][256];
	plcstring DirectoryPath[11][256];
	plcstring DirectoryCreate[11][256];
	plcstring FileName[256];
	unsigned short LastDirNameIndx;
	plcbit DirNameArrayEmpty;
} FDLinkBasicSplitPathPartsType;

typedef struct FDLinkBasicIPAdrType
{	plcstring StringDetermined[21];
	unsigned char ArrayDetermined[4];
} FDLinkBasicIPAdrType;

typedef struct FDLinkBasicNetworkConnectionType
{	struct FDLinkBasicIPAdrType ServerIPAddress;
	plcstring ServerName[51];
	plcstring Domain[51];
	plcstring User[21];
	plcstring Password[21];
	plcstring Share[51];
	signed short NFSUserID;
} FDLinkBasicNetworkConnectionType;

typedef struct FDLinkCreateParamType
{	plcstring Device[51];
	plcstring UsbInterface[129];
	enum FDLinkCreateParamDeviceTypeEnum DeviceType;
	struct FDLinkBasicNetworkConnectionType NetworkConfig;
} FDLinkCreateParamType;

typedef struct FDLinkCreateParamFoldedType
{	plcstring DevLinkParam[256];
	plcstring DeviceName[51];
	enum FDLinkCreateParamDeviceTypeEnum DeviceTypeUsed;
} FDLinkCreateParamFoldedType;

typedef struct FDLinkBasicDeviceHandleType
{	unsigned long LocalRoot;
	unsigned long LocalPath;
	unsigned long FTP;
	unsigned long NFS;
	unsigned long CIFS;
	unsigned long USB;
} FDLinkBasicDeviceHandleType;

typedef struct FDLinkBasicUsbBuffersType
{	unsigned long NodeID[5];
	struct usbNode_typ Data[5];
	struct usbDeviceDescr_typ DeviceDescriptor[5];
} FDLinkBasicUsbBuffersType;

typedef struct FDLinkBasicInternalType
{	enum FDLinkBasicStateEnum State;
	enum FDLinkBasicStateEnum LastErrorState;
	enum FDLinkBasicStateEnum LastStates[36];
	unsigned char EdgePos[4];
	unsigned long CntError;
	unsigned long CntErrorReset;
	unsigned long CntSplitPath;
	unsigned long CntLinkPath;
	plcbit AutoResetCmdOld;
	struct FDLinkBasicSplitPathPartsType SplitPathParts;
	struct FDLinkCreateParamType Parameters;
	struct FDLinkCreateParamFoldedType CreatedParam;
	struct DevLink DevLink_0;
	struct DevUnlink DevUnlink_0;
	struct DirInfo DirInfo_0;
	struct DirCreate DirCreate_0;
	unsigned long* pDeviceName;
	struct FDLinkBasicDeviceHandleType DeviceHandle;
	struct FDLinkBasicDeviceHandleType* pDeviceHandle;
	unsigned long Handle;
	unsigned short IndxI;
	unsigned short IndxJ;
	struct DiagCreateInfo DiagCreateInfo_0;
	struct DiagGetStrInfo DiagGetStrInfo_0;
	struct DiagDisposeInfo DiagDisposeInfo_0;
	plcstring DiagGetStrInfoBuffer[81];
	plcbit ARSimUsed;
	enum FDLinkCreateParamDeviceTypeEnum PathType;
	struct TON TON_0;
	struct TON TON_1;
	struct UsbNodeListGet UsbNodeListGet_0;
	struct UsbNodeGet UsbNodeGet_0;
	struct UsbDescriptorGet UsbDescriptorGet_0;
	struct UsbMsDeviceReady UsbMsDeviceReady_0;
	struct FDLinkBasicUsbBuffersType UsbBuffers;
	plcbit CSComMemAdrValid;
	struct CSComShareMemType* pCSComMemory;
} FDLinkBasicInternalType;

typedef struct FDLinkBasicConfigurationType
{	struct FDLinkBasicNetworkConnectionType NetworkConnection;
	enum FDLinkBasicUsbSelectModeEnum SelectUSBMode;
	unsigned char SelectUSB;
	unsigned short NetworkTimeout;
} FDLinkBasicConfigurationType;

typedef struct FDLinkBasicCopyConfigurationType
{	struct FDLinkBasicConfigurationType FDLinkConfig;
	enum FDLinkCreateParamDeviceTypeEnum ChangePathType;
	plcstring LocalDeviceLetter[2];
	enum FDLinkBasicCopyDirModeEnum DirectoryCopyMode;
	enum FDLinkBasicCopyFileModeEnum FileCopyMode;
	plcbit ArchiveTurnOn;
	enum FDLinkBasicCopyArchiveModeEnum ArchiveMode;
	enum FDLinkBasicCopyCompLevelEnum ArchiveCompressionLevel;
	enum FDLinkBasicCopyExtensionTypeEnum ArchiveExtensionType;
	enum FDLinkBasicCopyExtractModeEnum ArchiveExtractMode;
} FDLinkBasicCopyConfigurationType;

typedef struct FDLinkBasicCopyPathsDirFileType
{	struct FDLinkBasicFileLocalNetworkType FTP;
	struct FDLinkBasicFileLocalNetworkType NFS;
	struct FDLinkBasicFileLocalNetworkType CIFS;
	struct FDLinkBasicFileLocalNetworkType USB;
} FDLinkBasicCopyPathsDirFileType;

typedef struct FDLinkBasicCopyInfoType
{	struct FDLinkBasicDeviceType SrcDevices;
	struct FDLinkBasicPathType SrcPaths;
	struct FDLinkBasicFileType SrcFiles;
	struct FDLinkBasicCopyPathsDirFileType SrcPathsDirFile;
	struct FDLinkBasicDeviceType DestDevices;
	struct FDLinkBasicPathType DestPaths;
	struct FDLinkBasicFileType DestFiles;
	struct FDLinkBasicCopyPathsDirFileType DestPathsDirFile;
	struct FDLinkDiagType Diag;
} FDLinkBasicCopyInfoType;

typedef struct FDLinkBasic
{
	/* VAR_INPUT (analog) */
	struct CSComLinkType* pCSComLink;
	struct FDLinkBasicConfigurationType* Configuration;
	plcstring (*Path);
	plcstring (*PathSim);
	enum FDLinkBasicSplitTypeEnum SplitType;
	unsigned char SelectUSB;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct FDLinkInfoType Info;
	/* VAR (analog) */
	struct FDLinkBasicInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	plcbit AutoResetCmd;
	plcbit LinkPath;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Error;
	plcbit ARSimUsed;
	plcbit LinkPathDone;
} FDLinkBasic_typ;

typedef struct FDLInkBasicCopyInternalType
{	enum FDLinkBasicCopyStateEnum State;
	enum FDLinkBasicCopyStateEnum LastErrorState;
	enum FDLinkBasicCopyStateEnum LastStates[36];
	unsigned char EdgePos[4];
	unsigned long CntError;
	unsigned long CntErrorReset;
	unsigned long CntCopyToDest;
	unsigned long CntCopyToSrc;
	plcbit AutoResetCmdOld;
	plcbit CopyToDestAct;
	plcbit CopyToSrcAct;
	struct FDLinkBasic FDLinkBasic_0;
	struct FDLinkBasic FDLinkBasic_1;
	enum FDLinkCreateParamDeviceTypeEnum SrcPathType;
	enum FDLinkCreateParamDeviceTypeEnum DestPathType;
	enum FDLinkCreateParamDeviceTypeEnum ArchiveSrcPathType;
	enum FDLinkCreateParamDeviceTypeEnum ArchiveDestPathType;
	unsigned long* pSrcDeviceName;
	unsigned long* pDestDeviceName;
	unsigned long* pSrcFileDirName;
	unsigned long* pDestFileDirName;
	struct FileCopy FileCopy_0;
	struct DirCopy DirCopy_0;
	struct TON TON_0;
	plcbit PathDirFileStrOverflow;
	plcstring DestPath[256];
	plcstring DestPathSim[256];
	struct zipArchive ZipArchive_0;
	struct zipExtract ZipExtract_0;
	plcstring ArchiveOptionStr[81];
	plcbit ArchiveMoveToDest;
	struct FileDelete FileDelete_0;
	unsigned long* pArchiveSrcDeviceName;
	unsigned long* pArchiveDestDeviceName;
	unsigned long* pArchiveSrcFileDirName;
	unsigned long* pArchiveDestFileDirName;
	plcbit CSComMemAdrValid;
	struct CSComShareMemType* pCSComMemory;
} FDLInkBasicCopyInternalType;

typedef struct FDLinkBasicFileDeviceInfoType
{	struct FDLinkInfoType Info;
	plcbit ARSimUsed;
	plcstring SetPath[256];
	plcbit LinkDone;
	plcbit LinkError;
	plcbit LinkUsed;
} FDLinkBasicFileDeviceInfoType;

typedef struct FDLinkDirHandleConfigurationType
{	unsigned short FileNum;
	plcstring FileExtension[8];
} FDLinkDirHandleConfigurationType;

typedef struct FDLinkDirHandleInfoType
{	plcbit FileNumFull;
	unsigned long FileNumber;
	float FileLength;
	float DevFreeMemory;
	struct FDLinkDiagType Diag;
} FDLinkDirHandleInfoType;

typedef struct FDLinkDirHandleInternalType
{	enum FDLinkDirHandleStateEnum State;
	enum FDLinkDirHandleStateEnum LastErrorState;
	enum FDLinkDirHandleStateEnum LastStates[11];
	unsigned char EdgePos[5];
	unsigned char EdgeNeg[3];
	unsigned long CntError;
	unsigned long CntErrorReset;
	unsigned long CntDelete;
	unsigned long CntDeleteAll;
	plcbit AutoResetCmdOld;
	plcbit CSComMemAdrValid;
	struct CSComShareMemType* pCSComMemory;
	struct TON TON_0;
	struct DirRead DirRead_0;
	struct FileDelete FileDelete_0;
	struct DevMemInfo DevMemInfo_0;
	unsigned long DirEntry;
	unsigned long ItemIndx;
	unsigned long LastDelItemIndx;
	struct fiDIR_READ_DATA DirReadData;
	struct fiDIR_READ_DATA DirData[51];
	unsigned long DirReadErrCnt;
	plcstring FileName[256];
	plcstring FileNameExt[81];
	plcstring HelpString[81];
	unsigned long LengthString;
	unsigned long Status;
	plcbit ActCmdDeleteAll;
	plcbit ControlExtAct;
	plcbit SaveFile;
} FDLinkDirHandleInternalType;

typedef struct FDLinkConfigFileDevType
{	struct FDLinkBasicConfigurationType Connection;
	struct FDLinkDirHandleConfigurationType Directory;
	plcstring Path[256];
	plcstring SimPath[256];
	enum FDLinkBasicSplitTypeEnum SplitType;
	enum FDLinkBasicSplitTypeEnum SimSplitType;
	unsigned char SelectUSB;
	plcbit MappAvailable;
	plcbit ConfigUsed;
} FDLinkConfigFileDevType;

typedef struct FDLinkConfigBufferType
{	struct FDLinkConfigFileDevType FileDevice[20];
	plcbit AddCncDevice;
} FDLinkConfigBufferType;

typedef struct FDLinkInfoBufferType
{	struct FDLinkBasicFileDeviceInfoType FileDevice[20];
	struct FDLinkDirHandleInfoType Directory[20];
} FDLinkInfoBufferType;

typedef struct FDLinkBasicCopy
{
	/* VAR_INPUT (analog) */
	struct CSComLinkType* pCSComLink;
	struct FDLinkBasicCopyConfigurationType* Configuration;
	plcstring (*SrcPath);
	plcstring (*SrcPathSim);
	plcstring (*DestPath);
	plcstring (*DestPathSim);
	enum FDLinkBasicCopyTypeEnum CopyType;
	unsigned char SelectUSB;
	plcstring (*FileRename);
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct FDLinkBasicCopyInfoType Info;
	/* VAR (analog) */
	struct FDLInkBasicCopyInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	plcbit AutoResetCmd;
	plcbit ForceFileRename;
	plcbit ForceFileOverwrite;
	plcbit ForceDirOverwrite;
	plcbit CopyToDest;
	plcbit CopyToSrc;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Error;
	plcbit ARSimUsed;
	plcbit CopyToDestDone;
	plcbit CopyToSrcDone;
} FDLinkBasicCopy_typ;

typedef struct FDLinkDirHandle
{
	/* VAR_INPUT (analog) */
	struct CSComLinkType* pCSComLink;
	struct FDLinkDirHandleConfigurationType* pConfiguration;
	plcstring (*pDevice);
	plcstring (*pPath);
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct FDLinkDirHandleInfoType Info;
	/* VAR (analog) */
	struct FDLinkDirHandleInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	plcbit AutoResetCmd;
	plcbit Delete;
	plcbit DeleteAll;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Error;
	plcbit DeleteDone;
	plcbit DeleteAllDone;
} FDLinkDirHandle_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void FDLinkBasic(struct FDLinkBasic* inst);
_BUR_PUBLIC void FDLinkBasicCopy(struct FDLinkBasicCopy* inst);
_BUR_PUBLIC void FDLinkDirHandle(struct FDLinkDirHandle* inst);
_BUR_PUBLIC signed long FDLinkPathSplit(unsigned short SplitType, plcstring* Path, struct FDLinkBasicSplitPathPartsType* SplitPathPart);
_BUR_PUBLIC signed long FDLinkCreateParam(struct FDLinkCreateParamType** Parameters, struct FDLinkCreateParamFoldedType* CreatedParam);
_BUR_PUBLIC signed long FDLinkPathType(plcstring* Path);
_BUR_PUBLIC signed long FDLinkChangePathType(plcstring* Path, enum FDLinkCreateParamDeviceTypeEnum ChangePathType, plcstring* LocalDeviceLetter);
_BUR_PUBLIC signed long FDLinkArchiveAddExt(plcstring* ArchiveFile, enum FDLinkBasicCopyExtensionTypeEnum ArchiveTypeExt, enum FDLinkBasicExtensionAddTypeEnum AddType);
_BUR_PUBLIC signed long FDLinkFileAddExt(plcstring* File, plcstring* Extension, enum FDLinkBasicExtensionAddTypeEnum AddType);
_BUR_PUBLIC signed long FDLinkFileRemoveExt(plcstring* File);
_BUR_PUBLIC signed long FDLinkFindPathIndx(struct FDLinkInfoBufferType* FDInfoBuffer, plcstring* Path, plcstring* SimPath, unsigned char* Index, enum FDLinkCreateParamDeviceTypeEnum* DeviceType);
_BUR_PUBLIC signed long FDLinkCreatePath(plcstring* Path, plcstring* DeviceName, plcstring* PathPart, unsigned short* Level);
_BUR_PUBLIC signed long FDLinkPathLevel(plcstring* Path);
_BUR_PUBLIC signed long FDLinkUSBNum(plcstring* Path);
_BUR_PUBLIC signed long FDLinkFindPath(struct FDLinkInfoBufferType* FDInfoBuffer, plcstring* Path, plcstring* Device, unsigned char* Index);
_BUR_PUBLIC signed long FDLinkFindPathBoth(struct FDLinkInfoBufferType* FDInfoBuffer, plcstring* Path, plcstring* SimPath, plcstring* Device, unsigned char* Index);
_BUR_PUBLIC signed long FDLinkConfigPath(struct FDLinkConfigBufferType* FDConfigBuffer, plcstring* Path, plcstring* SimPath, unsigned char* Index);
_BUR_PUBLIC signed long FDLinkConfigPathAll(struct FDLinkConfigBufferType* FDConfigBuffer, struct FDLinkConfigFileDevType* ConfigFileDevice, unsigned char* Index);


#ifdef __cplusplus
};
#endif
#endif /* _FDLINK_ */

