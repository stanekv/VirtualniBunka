/* Automation Studio generated header file */
/* Do not edit ! */
/* CSCom 5.01.0 */

#ifndef _CSCOM_
#define _CSCOM_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _CSCom_VERSION
#define _CSCom_VERSION 5.01.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG3
		#include "astime.h"
		#include "standard.h"
		#include "sys_lib.h"
		#include "DataObj.h"
#endif
#ifdef _SG4
		#include "astime.h"
		#include "standard.h"
		#include "sys_lib.h"
		#include "DataObj.h"
#endif
#ifdef _SGC
		#include "astime.h"
		#include "standard.h"
		#include "sys_lib.h"
		#include "DataObj.h"
#endif


/* Constants */
#ifdef _REPLACE_CONST
 #define csCOM_DATA_OBJECT_NAME "CSComLink"
 #define csCOM_NUMBER_LINK_PV_ADR 25U
 #define csCOM_NUMBER_LINK_PV_ADR_INDX 24U
 #define csCOM_PV_NAME_STRING_LENGTH 32U
 #define csCOM_UDINT_MAX_VALUE 4294967295U
 #define csCOM_UDINT_MIN_VALUE 0U
 #define csCOM_MOD_LINK_HEAD_CHECK_VALUE 22865U
 #define csCOM_MOD_LINK_FOOT_CHECK_VALUE 80973U
 #define csCOM_HANDLE_TIME_OUT_CNT 500U
 #define csCOM_DAT_OBJ_CREATE_TIME_OUT 30.0f
#else
 _GLOBAL_CONST plcstring csCOM_DATA_OBJECT_NAME[11];
 _GLOBAL_CONST unsigned char csCOM_NUMBER_LINK_PV_ADR;
 _GLOBAL_CONST unsigned char csCOM_NUMBER_LINK_PV_ADR_INDX;
 _GLOBAL_CONST unsigned char csCOM_PV_NAME_STRING_LENGTH;
 _GLOBAL_CONST unsigned long csCOM_UDINT_MAX_VALUE;
 _GLOBAL_CONST unsigned long csCOM_UDINT_MIN_VALUE;
 _GLOBAL_CONST unsigned long csCOM_MOD_LINK_HEAD_CHECK_VALUE;
 _GLOBAL_CONST unsigned long csCOM_MOD_LINK_FOOT_CHECK_VALUE;
 _GLOBAL_CONST unsigned long csCOM_HANDLE_TIME_OUT_CNT;
 _GLOBAL_CONST float csCOM_DAT_OBJ_CREATE_TIME_OUT;
#endif




/* Datatypes and datatypes of function blocks */
typedef enum CSComSeveritiesEnum
{	csCOM_SEV_SUCCESS = 0,
	csCOM_SEV_INFORMATIONAL = 1,
	csCOM_SEV_WARNING = 2,
	csCOM_SEV_ERROR = 3
} CSComSeveritiesEnum;

typedef enum CSComBasicStateEnum
{	csCOM_BASIC_NOT_ENABLED = 0,
	csCOM_BASIC_CNTRL_P_CS_COM_LINK = 10,
	csCOM_BASIC_CNTRL_V_CS_COM_LINK = 11,
	csCOM_BASIC_DATOBJ_INFO_LINK = 20,
	csCOM_BASIC_CS_COM_LINK_PV_ADR_C = 35,
	csCOM_BASIC_CNTRL_CS_COM_LINK_PV = 40,
	csCOM_BASIC_CS_COM_LINK_PV_ADR = 45,
	csCOM_BASIC_ENABLED = 100,
	csCOM_BASIC_CNTRL_MODULE_LINK = 110,
	csCOM_BASIC_ERROR_RESET = 1000
} CSComBasicStateEnum;

typedef enum CSComAdvancedStateEnum
{	csCOM_ADVANCE_NOT_ENABLED = 0,
	csCOM_ADVANCE_DELETE_DATOBJ_LINK = 25,
	csCOM_ADVANCE_CREATE_DATOBJ_LINK = 30,
	csCOM_ADVANCE_CS_COM_LINK_CHECK = 45,
	csCOM_ADVANCE_ENABLED = 100,
	csCOM_ADVANCE_COPY_MODULE_LINK = 110,
	csCOM_ADVANCE_ERROR_RESET = 1000
} CSComAdvancedStateEnum;

typedef enum CSComErrorEnum
{	csCOM_NO_ERROR = 0,
	csCOM_ERR_CS_COM_LINK_NULL = -523753612,
	csCOM_ERR_PV_NAME_INVALID = -523753611,
	csCOM_ERR_CS_COM_LINK_INVALID = -523753610,
	csCOM_ERR_MEM_DAT_OBJECT_INFO = -523753609,
	csCOM_ERR_MEM_DAT_OBJECT_DELETE = -523753608,
	csCOM_ERR_MEM_DAT_OBJECT_CREATE = -523753607,
	csCOM_ERR_INVALID_ARG_LC_FCE = -523753606,
	csCOM_ERR_CS_COM_LINK_CHECK = -523753605,
	csCOM_ERR_NO_FREE_LINK_PV_ADR = -523753604,
	csCOM_ERR_NULL_P_MODULE_LINK = -523753603,
	csCOM_ERR_MODULE_LINK_CORRUPTED = -523753602,
	csCOM_ERR_CS_COM_LINK_MEM_IN_USE = -523753601,
	csCOM_ERR_CS_COM_MEM_CORRUPTED = -523753600,
	csCOM_ERR_CS_COM_LINK_CHANGED = -523753599,
	csCOM_ERR_INVALID_ARG_LMC_FCE = -523753598,
	csCOM_ERR_MEM_DAT_OBJ_DELETE_T = -523753597,
	csCOM_ERR_MEM_DAT_OBJ_CREATE_T = -523753596
} CSComErrorEnum;

typedef enum CSComFacilitiesEnum
{	csCOM_CS_COM_FAC_BASIC = 200,
	csCOM_TCP_COM_FAC = 250,
	csCOM_FD_LINK_FAC_BASIC = 270,
	csCOM_MC_EXT_FAC_BASIC = 280,
	csCOM_HW_DIAG_FAC_BASIC = 290,
	csCOM_DG_DATA_FAC_BASIC = 300,
	csCOM_TN_PNEU_FAC_BASIC = 320
} CSComFacilitiesEnum;

typedef struct CSComLinkType
{	unsigned long InternalLink[2];
} CSComLinkType;

typedef struct CSComStatusIDType
{	enum CSComErrorEnum ID;
	enum CSComSeveritiesEnum Severity;
	unsigned short Code;
} CSComStatusIDType;

typedef struct CSComStatusIDGenType
{	signed long ID;
	enum CSComSeveritiesEnum Severity;
	unsigned short Code;
} CSComStatusIDGenType;

typedef struct CSComInternalIDType
{	signed long ID;
	enum CSComSeveritiesEnum Severity;
	signed long Facility;
	unsigned short Code;
} CSComInternalIDType;

typedef struct CSComDiagnosticType
{	struct CSComStatusIDType StatusID;
	struct CSComInternalIDType Internal;
} CSComDiagnosticType;

typedef struct CSComLinkPvInfoType
{	unsigned long Address;
	plcstring Name[33];
} CSComLinkPvInfoType;

typedef struct CSComLinkModuleType
{	unsigned long HeaderCheck;
	unsigned long LinkMemAdr;
	struct CSComLinkPvInfoType LinkPv[25];
	plcbit NewLinkPvAdr;
	unsigned long FooterCheck;
} CSComLinkModuleType;

typedef struct CSComInfoCPUDiagSimType
{	plcbit PCAnyUsed;
	plcbit ARSimUsed;
	plcbit Done;
	plcbit Error;
	plcbit Busy;
} CSComInfoCPUDiagSimType;

typedef struct CSComInfoCPUType
{	struct CSComInfoCPUDiagSimType DiagSim;
} CSComInfoCPUType;

typedef struct CSComInfoDGDataType
{	unsigned long LogBufferAddress;
	plcbit InitDone;
	plcbit InitCmd;
} CSComInfoDGDataType;

typedef struct CSComInfoHWDiagType
{	unsigned long HwDiagDataAddress;
	plcbit InitDone;
} CSComInfoHWDiagType;

typedef struct CSComShareMemType
{	unsigned long HeaderCheck;
	struct CSComLinkModuleType LinkModule;
	struct CSComInfoCPUType InfoCPU;
	struct CSComInfoDGDataType DGData;
	struct CSComInfoHWDiagType HWDiag;
	unsigned long FooterCheck;
} CSComShareMemType;

typedef struct CSComInfoType
{	plcbit ModuleLinkCreate;
	float InitTime;
	struct CSComShareMemType* pCSComMemory;
	plcbit CSComMemAdrValid;
	struct CSComDiagnosticType Diag;
} CSComInfoType;

typedef struct CSComAdvanceInfoType
{	plcbit ModuleLinkCreate;
	plcbit ModuleLinkOverWrite;
	float InitTime;
	unsigned short NumConPvLink;
	struct CSComDiagnosticType Diag;
} CSComAdvanceInfoType;

typedef struct CSComBasicInternalType
{	enum CSComBasicStateEnum State;
	enum CSComBasicStateEnum LastErrorState;
	enum CSComBasicStateEnum LastStates[11];
	unsigned char EdgePos[5];
	unsigned long CntError;
	unsigned long CntErrorReset;
	plcbit AutoResetCmdOld;
	struct DatObjInfo DatObjInfo_0;
	struct DatObjCreate DatObjCreate_0;
	struct DatObjDelete DatObjDelete_0;
	unsigned long ModuleLinkIdent;
	unsigned long ModuleLinkData;
	unsigned long ModuleLinkLen;
	unsigned long StartTime;
	unsigned long EndTime;
	plcbit InitTimeActive;
	plcstring PvName[33];
	struct CSComLinkModuleType* pModuleLinkData;
	unsigned short IndexI;
	plcbit FreeLinkPvAdrFound;
	unsigned short FreeLinkPvAdrIndex;
	plcbit SameLinkPvAdrFound;
	plcbit CntrlModuleLink;
	unsigned long ShareMemAdrOld;
	struct TON TON_0;
} CSComBasicInternalType;

typedef struct CSComAdvancedInternalType
{	enum CSComAdvancedStateEnum State;
	enum CSComAdvancedStateEnum LastErrorState;
	enum CSComAdvancedStateEnum LastStates[11];
	unsigned char EdgePos[5];
	unsigned long CntError;
	unsigned long CntErrorReset;
	plcbit AutoResetCmdOld;
	struct DatObjInfo DatObjInfo_0;
	struct DatObjCreate DatObjCreate_0;
	struct DatObjDelete DatObjDelete_0;
	unsigned long ModuleLinkIdent;
	unsigned long ModuleLinkData;
	unsigned long ModuleLinkLen;
	unsigned long StartTime;
	unsigned long EndTime;
	plcbit InitTimeActive;
	struct CSComLinkModuleType* pModuleLinkData;
	plcbit CopyModuleLink;
	struct CSComShareMemType CSComMemory;
	unsigned short IndexI;
	unsigned short IndexJ;
	struct CSComLinkType* pCSComLink;
	unsigned short ConPvLinkCnt;
	plcbit FreeLinkPvAdrFound;
	unsigned short FreeLinkPvAdrIndex;
	plcbit SameLinkPvAdrFound;
	unsigned long HandleTimeOutCnt;
} CSComAdvancedInternalType;

typedef struct CSComMonDiagInternalLevel0Type
{	struct CSComInternalIDType FirstLevel;
	struct CSComInternalIDType NextLevels[1];
} CSComMonDiagInternalLevel0Type;

typedef struct CSComMonDiagPartsLevel0Type
{	struct CSComStatusIDGenType StatusID;
	struct CSComMonDiagInternalLevel0Type Internal;
} CSComMonDiagPartsLevel0Type;

typedef struct CSComMonDiagInternalLevel1Type
{	struct CSComInternalIDType FirstLevel;
	struct CSComInternalIDType NextLevels[2];
} CSComMonDiagInternalLevel1Type;

typedef struct CSComMonDiagPartsLevel1Type
{	struct CSComStatusIDGenType StatusID;
	struct CSComMonDiagInternalLevel1Type Internal;
} CSComMonDiagPartsLevel1Type;

typedef struct CSComMonDiagInternalLevel2Type
{	struct CSComInternalIDType FirstLevel;
	struct CSComInternalIDType NextLevels[3];
} CSComMonDiagInternalLevel2Type;

typedef struct CSComMonDiagPartsLevel2Type
{	struct CSComStatusIDGenType StatusID;
	struct CSComMonDiagInternalLevel2Type Internal;
} CSComMonDiagPartsLevel2Type;

typedef struct CSComMonDiagInternalLevel3Type
{	struct CSComInternalIDType FirstLevel;
	struct CSComInternalIDType NextLevels[4];
} CSComMonDiagInternalLevel3Type;

typedef struct CSComMonDiagPartsLevel3Type
{	struct CSComStatusIDGenType StatusID;
	struct CSComMonDiagInternalLevel3Type Internal;
} CSComMonDiagPartsLevel3Type;

typedef struct CSComMonDiagInternalLevel4Type
{	struct CSComInternalIDType FirstLevel;
	struct CSComInternalIDType NextLevels[5];
} CSComMonDiagInternalLevel4Type;

typedef struct CSComMonDiagPartsLevel4Type
{	struct CSComStatusIDGenType StatusID;
	struct CSComMonDiagInternalLevel4Type Internal;
} CSComMonDiagPartsLevel4Type;

typedef struct CSComMonDiagInternalLevel5Type
{	struct CSComInternalIDType FirstLevel;
	struct CSComInternalIDType NextLevels[6];
} CSComMonDiagInternalLevel5Type;

typedef struct CSComMonDiagPartsLevel5Type
{	struct CSComStatusIDGenType StatusID;
	struct CSComMonDiagInternalLevel5Type Internal;
} CSComMonDiagPartsLevel5Type;

typedef struct CSComBasic
{
	/* VAR_INPUT (analog) */
	struct CSComLinkType* pCSComLink;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct CSComInfoType Info;
	/* VAR (analog) */
	struct CSComBasicInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit AutoResetCmd;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Error;
} CSComBasic_typ;

typedef struct CSComAdvanced
{
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct CSComAdvanceInfoType Info;
	/* VAR (analog) */
	struct CSComAdvancedInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit AutoResetCmd;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Busy;
	plcbit Error;
	plcbit LinkActive;
} CSComAdvanced_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void CSComBasic(struct CSComBasic* inst);
_BUR_PUBLIC void CSComAdvanced(struct CSComAdvanced* inst);
_BUR_PUBLIC signed long CSComLinkCheck(unsigned long CSComLinkAdr, plcstring* PvName);
_BUR_PUBLIC signed long CSComLinkMemCheck(struct CSComLinkType* CSComLink, plcbit* pCSComMemAdrValid, unsigned long* pCSComMemAdrOld);


#ifdef __cplusplus
};
#endif
#endif /* _CSCOM_ */

