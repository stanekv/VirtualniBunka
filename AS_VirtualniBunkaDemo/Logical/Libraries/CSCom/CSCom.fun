
FUNCTION_BLOCK CSComBasic (*Custom Solution Communication Basic*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pCSComLink : REFERENCE TO CSComLinkType; (*Custom Solution link*) (* *) (*#PAR#;*)
		Enable : BOOL; (*Enables/Disables function block*) (* *) (*#PAR#;*)
		AutoResetCmd : BOOL; (*Turn on autoreset commands*) (* *) (*#PAR#OPT#;*)
		ErrorReset : BOOL; (*Reset all function block error*) (* *) (*#PAR#;*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#PAR#;*)
		Busy : BOOL; (*Function block is busy*) (* *) (*#PAR#;*)
		Error : BOOL; (*Indicates error*) (* *) (*#PAR#;*)
		StatusID : DINT; (*Error/Status information*) (* *) (*#PAR#;*)
		Info : CSComInfoType; (*Structure with information*) (* *) (*#CMD#;*)
	END_VAR
	VAR
		Internal : CSComBasicInternalType; (*Internal data type*) (* *) (*#CMD#;*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK CSComAdvanced (*Custom Solution Communication Advanced*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables/Disables function block*) (* *) (*#PAR#;*)
		AutoResetCmd : BOOL; (*Turn on autoreset commands*) (* *) (*#PAR#OPT#;*)
		ErrorReset : BOOL; (*Reset all function block error*) (* *) (*#PAR#;*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Function block is active*) (* *) (*#PAR#;*)
		Busy : BOOL; (*Function block is busy*) (* *) (*#PAR#;*)
		Error : BOOL; (*Indicates error*) (* *) (*#PAR#;*)
		StatusID : DINT; (*Error/Status information*) (* *) (*#PAR#;*)
		LinkActive : BOOL; (*Link to share memroy is active*) (* *) (*#CMD#;*)
		Info : CSComAdvanceInfoType; (*Structure with information*) (* *) (*#CMD#;*)
	END_VAR
	VAR
		Internal : CSComAdvancedInternalType; (*Internal data type*) (* *) (*#CMD#;*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION CSComLinkCheck : DINT (*Custom solution communication link check*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		CSComLinkAdr : UDINT; (*CSCom link address*) (* *) (*#PAR#;*)
		PvName : STRING[32]; (*PV name of the CSCom link*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION

FUNCTION CSComLinkMemCheck : DINT (*Custom Solution Communication Link Memory Check*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		CSComLink : CSComLinkType; (*Custom Solution link*) (* *) (*#PAR#;*)
		pCSComMemAdrValid : REFERENCE TO BOOL; (*CSCom share memory address is valid*) (* *) (*#PAR#;*)
		pCSComMemAdrOld : REFERENCE TO UDINT; (*CSCom share memory address old value*) (* *) (*#PAR#;*)
	END_VAR
END_FUNCTION
