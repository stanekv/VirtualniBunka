
TYPE
	CSComFacilitiesEnum : 
		( (*Custom solution communication facilities enumeration*)
		csCOM_CS_COM_FAC_BASIC := 200, (*Custom solution facilities basic facility*)
		csCOM_TCP_COM_FAC := 250, (*TCP communication facility*)
		csCOM_FD_LINK_FAC_BASIC := 270, (*File device link basic facility*)
		csCOM_MC_EXT_FAC_BASIC := 280, (*Motion control extension facilities basic facility*)
		csCOM_HW_DIAG_FAC_BASIC := 290, (*Hardware diagnostic facilities basic facility*)
		csCOM_DG_DATA_FAC_BASIC := 300, (*Diagnostic data facilities basic facility*)
		csCOM_TN_PNEU_FAC_BASIC := 320 (*Technology pneumatic basic facility*)
		);
END_TYPE
