
TYPE
	CSComErrorEnum : 
		( (*Error numbers of library CSCom*)
		csCOM_NO_ERROR := 0, (*No error*)
		csCOM_ERR_CS_COM_LINK_NULL := -523753612, (*Custom solution communication link is null pointer. [Error: 10100]*)
		csCOM_ERR_PV_NAME_INVALID := -523753611, (*PV name is invalid. [Error: 10101]*)
		csCOM_ERR_CS_COM_LINK_INVALID := -523753610, (*Custom solution communication link is invalid. [Error: 10102]*)
		csCOM_ERR_MEM_DAT_OBJECT_INFO := -523753609, (*Function block DatObjInfo has error. [Error: 10103]*)
		csCOM_ERR_MEM_DAT_OBJECT_DELETE := -523753608, (*Function block DatObjDelete has error. [Error: 10104]*)
		csCOM_ERR_MEM_DAT_OBJECT_CREATE := -523753607, (*Function block DatObjCreate has error. [Error: 10105]*)
		csCOM_ERR_INVALID_ARG_LC_FCE := -523753606, (*Arguments of link check are not valid (null pointers). [Error: 10106]*)
		csCOM_ERR_CS_COM_LINK_CHECK := -523753605, (*Function CSComLinkCheck has error. [Error: 10107]*)
		csCOM_ERR_NO_FREE_LINK_PV_ADR := -523753604, (*No free link PV address, increase the number of address of custom solution communication link PV. [Error: 10108]*)
		csCOM_ERR_NULL_P_MODULE_LINK := -523753603, (*Null pointer to the module link. [Error: 10109]*)
		csCOM_ERR_MODULE_LINK_CORRUPTED := -523753602, (*Module link is corrupted, header and footer check do not match. [Error: 10110]*)
		csCOM_ERR_CS_COM_LINK_MEM_IN_USE := -523753601, (*In the module link the share memory link address is already in use. [Error: 10111]*)
		csCOM_ERR_CS_COM_MEM_CORRUPTED := -523753600, (*CSCom share memory corrupted, header and footer chekc do not match. [Error: 10112]*)
		csCOM_ERR_CS_COM_LINK_CHANGED := -523753599, (*Custom solution communication link is modified. [Error: 10113]*)
		csCOM_ERR_INVALID_ARG_LMC_FCE := -523753598, (*Arguments of link memory check are not valid (null pointers). [Error: 10114]*)
		csCOM_ERR_MEM_DAT_OBJ_DELETE_T := -523753597, (*Time out delete data object. [Error: 10115]*)
		csCOM_ERR_MEM_DAT_OBJ_CREATE_T := -523753596 (*Time out create data object. [Error: 10116]*)
		);
END_TYPE
