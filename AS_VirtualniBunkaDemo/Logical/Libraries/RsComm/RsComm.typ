
TYPE
	RsCommErrorEnum : 
		(
		rsCOMM_ERR_CLOSE
		);
	RSCommBufferType : 	STRUCT 
		Digital : ARRAY[0..63]OF BOOL;
		Analog : ARRAY[0..15]OF REAL;
		Group : ARRAY[0..15]OF DINT;
	END_STRUCT;
END_TYPE
