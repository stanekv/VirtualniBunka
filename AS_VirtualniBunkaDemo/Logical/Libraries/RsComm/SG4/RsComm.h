/* Automation Studio generated header file */
/* Do not edit ! */
/* RsComm 1.00.4 */

#ifndef _RSCOMM_
#define _RSCOMM_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _RsComm_VERSION
#define _RsComm_VERSION 1.00.4
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG3
		#include "AsUDP.h"
#endif
#ifdef _SG4
		#include "AsUDP.h"
#endif
#ifdef _SGC
		#include "AsUDP.h"
#endif


/* Constants */
#ifdef _REPLACE_CONST
 #define rsCOMM_FACILITY 0U
#else
 _GLOBAL_CONST unsigned short rsCOMM_FACILITY;
#endif




/* Datatypes and datatypes of function blocks */
typedef enum RsCommErrorEnum
{	rsCOMM_ERR_CLOSE
} RsCommErrorEnum;

typedef struct RSCommBufferType
{	plcbit Digital[64];
	float Analog[16];
	signed long Group[16];
} RSCommBufferType;

typedef struct RsComm
{
	/* VAR_INPUT (analog) */
	plcstring ClientAdr[16];
	unsigned short PortLocal;
	unsigned short PortRemote;
	struct RSCommBufferType InputBuffer;
	struct RSCommBufferType OutputBuffer;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
} RsComm_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void RsComm(struct RsComm* inst);


#ifdef __cplusplus
};
#endif
#endif /* _RSCOMM_ */

