
FUNCTION_BLOCK RsComm (*RsComm *) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL;
		ErrorReset : BOOL;
		ClientAdr : STRING[15] := '127.0.0.1'; (*Pointer to client IP address.*)
		PortLocal : UINT := 11000; (*Port of the server. Function block waits for incoming datagrams on this port.*)
		PortRemote : UINT := 11001; (*Port of the client (receiver). Data is sent to this port.*)
		InputBuffer : RSCommBufferType; (*PLC Inputs*)
		OutputBuffer : RSCommBufferType; (*PLC Outputs*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL;
		Error : BOOL;
		StatusID : DINT;
	END_VAR
END_FUNCTION_BLOCK
